# CHD4615 - Family and Child Public Policy
## Andrew Benesh 

## Description

CHD4615, Family and Child Public Policy, is a family policy course for advanced undergraduates studying Family and Child Sciences (FCS).
This course provides an overview of theory and implementation of public policies relating to children and families at the State and Federal levels.

As an aid to student learning, course materials are hosted online at [benesh.info/CHD4615](http://www.benesh.info/CHD4615).

## Contributors

The contents of this repository were developed by Andrew Benesh.

