% Document settings
\documentclass[11pt]{article}
\usepackage[margin=0.6in]{geometry}
\usepackage[pdftex]{graphicx}
\usepackage{multirow}
\usepackage{setspace}
\usepackage{hyperref}
\hypersetup{colorlinks = true, linkcolor = blue, urlcolor = blue, citecolor = blue}
\usepackage{apacite}
\usepackage{bibentry}
\bibliographystyle{apacite}

\setlength\parindent{0pt}

\begin{document}

% Course information
\begin{tabular}{ l l }
  \multirow{3}{*}{\includegraphics[height=1.25in,width=1.25in]{logo.png}} 
  & \LARGE Family and Child Public Policy \\\\
  & \LARGE CHD 4615-0001 Spring 2016\\\\
\end{tabular}
\vspace{10mm}

% Professor information
\begin{tabular}{ l l l l }
\\
  & \large Instructor \& TA: & Andrew Benesh, MFT & Leah Pompey\\
  & \large Email: & \href{mailto:asb12f@my.fsu.edu}{asb12f@my.fsu.edu} & \href{mailto:lap14e@my.fsu.edu}{lap14e@my.fsu.edu} \\
  & \large Classroom: & UPL 0101 & \\
  & \large Class Times: & Tuesday \& Thursday 2:00 -- 3:10PM  & \\
  & \large Office: & Sandels 228 & Sandels 211  \\
  & \large Office Hours: & Tuesday \& Thursday 12:30 -- 1:30PM & Tuesday 11:30AM -- 1:30PM\\
  & \large & or by appointment & or by appointment \\
  & \large External Course Website: & \href{http://www.benesh.info/CHD4615}{benesh.info/CHD4615} \\
\end{tabular}
\vspace{3mm}

% Course details
\section*{Course Description}
This course will provide an overview of theory and implementation of public policies relating to children and families at the state and federal levels.
Students will explore ways that families contribute to and are affected by social problems, and how families can be involved in policy solutions. 
Students will learn about roles professionals can play in building and interacting with family policy.

\subsection*{Course Objectives} 
\begin{enumerate}
\setlength\itemsep{-0.6em}
\item Critically examine theoretical orientations for conceptualizing family policy and for connecting research and policy making
\item Demonstrate an understanding of how policy is influenced by demographic changes, values, attitudes, and perceptions of the well-being of children and families
\item Apply a family perspective to policy analysis by assessing current policy issues in terms of their sensitivity to and support for diverse contemporary families
\item Describe the roles professionals can play in building family policies that support families across the life cycle
\item Discuss how these roles can be implemented using either an educational or advocacy approach
\item Apply critical thinking skills in developing and expressing logical arguments to policymakers, practitioners, and the public
\item Develop and refine skills for collaborating with policymakers and professionals with diverse political viewpoints
\end{enumerate} 

\subsection*{Textbooks} 
\textbf{In order to do well in this course, you MUST read.} 
Due to the scope of the course, it is \textbf{impossible} for me to cover every point detailed in the chapters. 
Students are responsible for all assigned course material, including material that is \textit{not explicitly} discussed in class.
Therefore, it is essential for students to read all assigned material prior to the class period for in which the topic will be discussed.

Students are encouraged to be aggressive in reading (highlighting, underlining, taking notes, etc)! 
Skimming the material and last minute cramming are insufficient for this course. 
Many exam questions will come directly from the reading and not addressed during lecture, so students must be familiar with the text!

Additional readings will be provided throughout the course.\\

\begin{tabular}{l l}
\textbf{Title:} & Family Policy Matters, 3rd Edition\\
\textbf{Author:}& Karen Bogenschneider \\
\textbf{Year:} & 2014 \\
\textbf{Publisher:}& Routledge \\
\textbf{ISBN13:} & 978-0-41584448-2 \\
\textbf{Get it Here:} & \href{http://www.amazon.com/Family-Policy-Matters-Policymaking-Professionals/dp/0415844487}{Amazon}; \href{http://www.barnesandnoble.com/w/family-policy-matters-karen-bogenschneider/1117508913?ean=9780415844482}{Barnes \& Noble}; \href{http://www.routledgetextbooks.com/textbooks/9780415844482/}{Routledge}\\
& \\

\end{tabular}

\section*{Process \& Content}

\subsection*{Course Content}
The instructor of this class is responsible for organizing information related to the course content and for developing learning experiences. 
Students should read \textbf{before} class to ensure that they are adequately prepared.

\subsection*{Course Activities}
Class time is primarily devoted to lecture, but class participation is strongly encouraged. 
Throughout the semester, class time may be used to view videos and documentaries, address current issues, or discuss recent news or magazine articles relevant to course materials.
Students are encouraged to locate and bring in relevant articles and news.

\subsection*{Attendance \& Scheduling} 
\subsubsection*{General Attendance Policy}
Although attendance will not be taken daily, students are expected to attend class regularly. 
A large portion of test questions will come from lecture and class discussions, which will cover topics beyond those presented in the book; therefore missing class is likely to have a negative effect on student grades. 
Additionally, throughout the semester in-class quizzes and activities will be used to collect attendance, which is factored into student's final score for the course.

\subsubsection*{First Day Attendance}
Students who are not present the first day of class will be dropped according to university policy. 

%\subsubsection*{Presentation Day Attendance}
%In the latter portion of the course, student groups will be conducting presentations.
%As a matter of respect for your peers, you are expected to actively attend and participate on these dates.
%Students who are absent on presentation days will receive a 1 point deduction per day from their total points in the course.

\subsection*{Announcements \& Communication}
It is important to use appropriate online forms of communication to stay up to date, and students should frequently view the course schedule and attend to deadlines.
There will be announcements in Blackboard \& emails as a way of letting you know about something that either has changed or needs clarification.  It is your responsibility to frequently read announcements and emails for the course (suggested at least THREE TIMES each week).

When communicating online, it is essential to follow outstanding levels of etiquette/civil discourse.  
Typed words have a lasting impact. 
Placing ideas about issues in a positive framework will more likely lead to learning new ideas, collegiality, and enlightening interactions with peers and instructor.  
Be respectful of the thoughts and experiences of your instructor and the class as a whole.

\textbf{If you need to contact the instructor via email, it is important that you use your official school email address, and include CHD4615 in the title of your email. 
Other emails may be sorted as spam, or receive delayed responses.}

\pagebreak
\section*{Course Policies}

\section*{Assignments}
During this course, students will complete four written papers, three tests, and a group project.
To ensure depth of understanding, students must address different policy issues for each assignment.
Unless otherwise specified, all written papers should be submitted to Blackboard by 5PM on the due date specified, submitted in PDF format, and follow the formatting requirements specified in the rubric.
Failure to adhere to these instructions will result in loss of points.
Additional details can be found on the rubric for each assignment.

\paragraph*{A Disagreeable Discussion (20pts)}
Being able to work with people who have differing viewpoints is an essential skills for all policymakers and professionals.
In this assignment, students will practice these skills by interviewing someone who holds a political opinion they strongly disagree with, and preparing a one-page written report.

\paragraph*{My Own Worst Enemy (20pts)}
Just as students must be able to understand and listen to differing viewpoints, they must be able to critically evaluate their own beliefs. 
In this assignment, students will write a two-page report first describing their own position on an issue, then providing the strongest possible argument against it.

\paragraph*{Meet the Press (40pts)}
To be an active participant in public policy, students must keep up-to-date on policy issues being debated or decided in local, state, or federal government. 
To do this, students are expected to follow the news in print, radio, and online media. 
For this assignment, students will write a 2-page summary describing coverage of a current policy between two print media articles, providing a critical assessment of the  articles, policy implications, and appropriate personal reflections.

\paragraph*{Family Impact Analysis (60pts)}
In this assignment, students will conduct a formal family impact analysis of an existing or currently proposed policy.
This paper may be no more than four pages long.

\paragraph*{Advocacy Project (60pts total)}
\subparagraph*{Part 1: Policy Memo (20pts)}
In groups of no more than six, students will identify an existing social problem, analyze it using the three worldviews, and develop a potential policy solution using the Theory of Paradox. 
Students will then advocate for their policy solution by developing a one-page policy memo arguing for their policy solution.

\subparagraph*{Part 2: Advocacy or Education Media (30pts)}
After reflecting on their own professional roles and the nature of the policy issue, students will develop materials to either advocate for their policy solution or provide general education about the social problem. 
For this part of the project, students will develop a media presentation in the form of a youtube video or audio podcast at least 10 minutes length.

\subparagraph*{Part 3: Consumer Response (10pts)}
Each group will be assigned the task of reviewing materials generated by another group, and providing critical feedback on the clarity and effectiveness of the campaign.

%\paragraph*{Views of a Current Family Policy (80pts)}
%For this assignment, students will develop a detailed 5--8 page paper on a policy issue of their choice. 
%Students will discuss how the issue can be approached from concerned, sanguine, and impatient views, and present on their personal views, policy agenda, and values. 
%
%\paragraph*{Presentation of a Current Family Policy (40pts)}
%For this assignment, students will prepare a short presentation and class activity on a current family policy issue.

\paragraph*{Exams (60pts each)}
All exams will consist of multiple choice questions, and will be scantron based. 
Exams are not explicitly cumulative, but will rely on knowledge of policies and theories from throughout the course.
Students are responsible for bringing their own pencils for the exam; scantrons will be provided.
Students who complete the exam in pen will receive a grade of 0.

\paragraph*{Attendance (20pts)}
Throughout the course, students will be asked to complete in-class quizzes, surveys, and in-class activities, some of which will be collected for attendance.
Final attendance points will be determined by the proportion of points students earn on these activities (i.e., if a student earns 85\% of the available points on attendance activities, their attendance score will be: $20 $ x $ 0.85 = 17$). 
Attendance scores will be rounded \textbf{down} to the nearest whole point value.

\subsection*{Extra Credit}
There are two (2) extra credit opportunities available to students in this course.
No other extra credit opportunities will be available.
Students who wish to take advantage of these extra credit opportunities must complete and submit the assignments no later than Friday, March 4, at 5:00PM.

\paragraph*{Letter to a Government Official (3-5pts)}
Students will write a 1-page letter to a government official expressing their view on a current family policy issue of their choice.
Students should use \href{https://owl.english.purdue.edu/owl/resource/653/01/}{modified block style letter formatting} and structure the letter using the four part structure described by \href{https://www.youtube.com/watch?v=rWhLSORCwW0}{Omar Ahmad}.
Students may not use pre-formed letters; all work must be original.
Students who successfully write and send the letter will earn 3 points; if the student receives a response from the government official, they will earn an additional 2 points.
Students should provide 

Students may only receive credit for completing this assignment once; however if a student does not receive a response they may send additional letters until they receive response to earn the additional 2 points.

\paragraph*{Write a Course Wiki Article (5pts)}
Students will write a wiki article for the course wiki on a course topic of the student's choice (note: students must get topics approved prior to work to avoid duplication).
Wiki articles should be at least 500 words long, include a reference at least one external source and the policy or bill, and include one example of how the policy works in real life.

\pagebreak
\section*{Grading}

\paragraph*{Final Grades} Grades will be based on a point system comprised of a total of 400 points as indicated below.  
A plus/minus system will be in effect although there are no grades of A+, F+ or F-.  
No assignments will be accepted after the last day of class.  
There will be no rewrites of assignments in this course.  
\textbf{Rounding allowed for this class already is included for point totals below so do not ask for additional rounding at semester end}.  \\

\textbf {Points Distribution:} \\\\
\hspace*{10mm}
\begin{tabular}{ l | l }
A Disagreeable Conversation & 20pts\\
My Own Worst Enemy & 20pts \\
Meet the Press & 40pts \\
Advocacy Project & 60pts \\
Family Impact Analysis & 60pts \\
Attendance & 20pts \\
%Analysis of Current Family Policy & 80pts\\
%Presentation of Current Family Policy & 40pts \\
%Views of a Current Family Policy & 80pts \\
Exams (3)  & 180pts \\ \hline
Total Points & 400pts\\
\end{tabular} \\\\

\textbf {Letter Grade Distribution:} \\\\
\hspace*{10mm}
\begin{tabular}{ l l l | ll  l }
Percent & Points & Grade & Percent & Points & Grade \\ \hline
\textgreater= 92.00 & (\textgreater= 368)& A & 72 - 77\% &(288-311)& C \\
90 - 91\% &(360-367)& A-  & 70 - 71\% &(280-287)& C- \\
88 - 89\% &(352-359)& B+  & 68- 69\% &(272-279)& D+ \\
82 - 87\% &(328-351)& B  & 62- 67\% &(248-271)& D \\
80 - 81\% &(320-327)& B-  & 60 - 61\% &(240-247)& D- \\
78 - 79\% &(312-319)& C+  & \textless= 59\% & (\textless= 239) & F \\
\end{tabular} \\

\paragraph*{Grading Questions} Exams and assignments will be graded quickly, but grading a large number of assignments takes time.
It is your responsibility to be aware of your grades consistently throughout the course, in order to prevent any surprises at the end of the semester.
If you desire clarification of your score on an exam or assignment, you have 1 week after scores are posted to contact your instructor to make an appointment.
You are encouraged to seek clarification but it is assumed you understand your score if you ask no questions within one week of any test or assignment.  
Students should check for grades posted in Blackboard regularly.

\paragraph*{Missed Exams}
Make-up exams will be given only in cases of ``FSU excused absences''.
It is the student’s responsibility to notify the instructor within 24 hours of the missed exam or assignment and to make it up within one week of the due date. 
If you do not have verifiable documentation for your absence you will not be allowed to make it up. 
Absences are excused only when verifiable:  DOCUMENTATION of illness (from the health center or medical doctor), loss of family member (from obituary), FSU athletics involvement (from coach), or religious holiday presented to the instructor.  
Consideration will also be given to students whose dependent children experience serious illness, with proper medical documentation.  
Please let the instructor know as soon as possible if chronic health problems and/or a personal emergency threaten to interfere with your regular attendance and required work for this class; the instructor will work to facilitate the best resolution in extreme cases.  
Only the days indicated on the documentation will be accepted as verification of an excused absence. 
Activities such as job interviews, weddings, work schedule, etc., are NOT considered excused absences.

\paragraph*{Missed Assignments}
To receive full credit for your assignments, you must turn them in on or before their due date.
Assignments will not be accepted after their due date unless the student provides documentation of an excused absence.
Technological problems are not considered a valid excuse; for this reason it is advisable to submit assignment ahead of due dates so there is sufficient time for troubleshooting if problems arise.

\paragraph*{Exam Tardiness}
Students will be allowed to take any exam they arrive tardy to only if no students have turned in their test or quiz.
Any student who arrives late for a test or quiz after the first test or quiz has been turned in will receive a grade of zero unless they provide documentation of an excusable absence.

\subsection*{Class Participation and Decorum}
All class members have a responsibility to keep current with reading and all assignments. 
This participation is essential for your learning. 
Disrespectful behavior toward other students, the course instructor, or anyone associated with the class is unacceptable and can have consequences according to the FSU honor code.  

\subsection*{University Attendance Policy}
Excused absences include documented illness, deaths in the family and other documented crises, call to active military duty or jury duty, religious holy days, and official University activities.  
These absences will be accommodated in a way that does not arbitrarily penalize students who have a valid excuse.  
Consideration will also be given to students whose dependent children experience serious illness. 

\subsection*{Academic Honor Policy}
The Florida State University Academic Honor Policy outlines the University’s expectations for the integrity of students’ academic work, the procedures for resolving alleged violations of those expectations, and the rights and responsibilities of students and faculty members throughout the process.  
Students are responsible for reading the Academic Honor Policy and for living up to their pledge to “. . . be honest and truthful and . . . [to] strive for personal and institutional integrity at Florida State University.”  (Florida State University Academic Honor Policy, found at \href{http://fds.fsu.edu/Academics/Academic-Honor-Policy}{http://fds.fsu.edu/Academics/Academic-Honor-Policy}.)

\subsection*{Americans with Disabilities Act}
Students with disabilities needing academic accommodation should: (1) register with and provide documentation to the Student Disability Resource Center; and (2) bring a letter to the instructor indicating the need for accommodation and what type.  
This should be done during the first week of class.  This syllabus and other class materials are available in alternative format upon request.
 For more information about services available to FSU students with disabilities, contact the: \\

Student Disability Resource Center \\
874 Traditions Way\\
108 Student Services Building\\
Florida State University\\
Tallahassee, FL 32306-4167\\
(850) 644-9566 (voice)\\
(850) 644-8504 (TDD)\\
\href{mailto:sdrc@admin.fsu.edu}{sdrc@admin.fsu.edu} \\
\href{http://www.disabilitycenter.fsu.edu/}{http://www.disabilitycenter.fsu.edu/}\\

\subsection*{Syllabus Change Policy}
Except for changes that substantially affect implementation of the evaluation (grading) statement, this syllabus is a guide for the course and is subject to change with advance notice.  

\subsection*{Free Tutoring from FSU}
On-campus tutoring and writing assistance is available for many courses at Florida State University.  
For more information, visit the Academic Center for Excellence (ACE) Tutoring Services’ comprehensive list of on-campus tutoring options at \href{http://ace.fsu.edu/tutoring}{http://ace.fsu.edu/tutoring} or contact \href{mailto:tutor@fsu.edu}{tutor@fsu.edu}.  
High-quality tutoring is available by appointment and on a walk-in basis.
 These services are offered by tutors trained to encourage the highest level of individual academic success while upholding personal academic integrity.
 
\section*{Advice from Former Students}
\begin{itemize}
%\item I would tell them to \textbf{NOT} fall behind. Keep up with the readings and lectures. It is not a terribly hard course but it is a lot of material and if you fall behind it can be overwhelming.
\item Read! I read quite a bit from the text and it really helped me understand what was discussed in class.
\item To read ahead and set out specific times to complete tasks
%\item Take notes from the textbook. Email if you are stuck or need clarification. 
%\item You \textbf{HAVE} to put in the hours and work to get a good grade. 
%\item Read material as early as possible so you don't fall behind
%\item Read every page of the book that is assigned. Take extremely detailed notes. Visit the office hours. Listen to each lecture, also take detailed notes.
%\item You should make sure you are able to apply the material that you are learning to real life article and research papers to make sure you understand the material 100\%
\item Begin thinking of concepts in terms of examples as soon as you start reading, because this will help in fully comprehending the course material.
%\item Study beforehand. The information is based off of the textbook and lectures. 
\item COME TO CLASS. Get the education you are paying for and \textbf{appreciate} it!
\item Focus on examples and concepts not definitions and focus on the lectures because the book makes things more confusing than they need to be sometimes. 
%\item Do not slack and wait till the last minute to do the assignments, stay on top of reading and work with a partner from class when doing papers, helps you to make sure your staying on the right track
\item Focus on the exams! Make sure you begin to study for each exam at least a week before. The exams are not easy!
\item Just listen! And use the Jeopardy game while studying and you should probably  just bit the bullet and do the extra credit ASAP.
%\item Keep up with the work. It is a lot of information take in. You must both learn the information and apply it.
%\item Keep up with the readings; do not wait until the last day to start research papers; and practice searching for peer-reviewed articles (it can be frustrating at times when you can't find the exact article you are looking for; so key terms are important to use when searching for articles).
%\item  \textbf{GO TO THE OFFICE HOURS}. Come prepared with questions and demand answers that will help you.
%\item Don't get behind in the course. Definitely spend time studying before the exams, and don't wait until the last minute.
%\item My advice to students who will take this course in the future is to not procrastinate your papers and stay on top of your work. The papers take time and revision so don't do them the day of.
\item  Also, ask for questions and help if you need it. Mr. Benesh will answer all of your questions and really help you to understand the topic better!
\item Stay on top of the readings because there will be questions on the test out of the book.
\item Don't just memorize, synthesize!
\item Study as you go! There are a lot of policies and important topics to remember. To be sure to keep it all in mind, review after every other class.
%\item Study, study, study! Read the textbook. Put in the time and you will excel.
%\item I advise students to study ahead of time and do not allow the readings to build up because it is terrible trying to study for an exam the night before
\item I would advise them to use their resources well and ask for help when needed, especially in regards to feedback for papers.
\item If you ever have a question, don't hesitate to email!
\item Study ahead. Don't try to cram. You have to really know the material to do well on the test. Have to study to know the info rather than study to recognize it on a MC test.
\subsection*{My Advice}
As my past students have said, keeping up with the readings and assignments is critical to your success in this course.
Focus on being able to apply the ideas we talk about in class.
Never be afraid to ask questions -- in class, office hours, or by email -- and if the answer I give you isn't clear or helpful, \textbf{don't be afraid to ask again} until you feel like you have a good understanding of the concepts and material! 

\textbf{Take advantage of feedback}.
Due to the class size, I only provide basic feedback on graded assignments. 
However, I'm always willing to provide more detailed feedback on completed assignments upon request. 
I'm also always willing to provide feedback on drafts of assignments before you turn them in, provided you submit the drafts to me in a timely manner (at least 1 week prior to the due date). 
\end{itemize} 

\newpage
\begin{table}[h!]
\centering
\caption{Tentative Schedule}
\begin{tabular}{ | c | c | c | }
\hline
\textbf{Week} & \textbf{Dates} & \textbf{Content} \\
\hline
Week 1 & 1/8 & \begin{minipage}{.6\textwidth}
\begin{itemize} \itemsep-0.4em
	\vspace{1mm}
	\item Topics: Course Introduction
	\item Reading: Chapter 1
	\vspace{1mm}
\end{itemize}
\end{minipage} \\
\hline
Week 2 & 1/12 and 1/14 & \begin{minipage}{.6\textwidth}
\begin{itemize} \itemsep-0.4em
	\vspace{1mm}
	\item Topic: Why Family Policy is Important, Defining Family \newline Policy
	\item Reading: Chapters 2 \& 3, Ebaugh \& Curry (2000)
	\vspace{1mm}
\end{itemize}
\end{minipage} \\
\hline
Week 3 & 1/19 and 1/21 & \begin{minipage}{.6\textwidth}
\begin{itemize} \itemsep-0.4em
	\vspace{1mm}
	\item Topics: Defining Family Policy, Individual vs Family Policy 
	\item Reading: Chapters 3 \& 4
	\item Due: A Disagreeable Discussion 1/21 5PM
	\vspace{1mm}
\end{itemize}
\end{minipage} \\
\hline
Week 4 & 1/26 and 1/28 & \begin{minipage}{.6\textwidth}
\begin{itemize} \itemsep-0.4em
	\vspace{1mm}
	\item Topic: Global Perspectives \& US Perspectives \& Family \newline Policy Legitimacy
	\item Reading: Chapters 5 \& 6; Zhang \& Goza (2006), Zapata, Contreras, \& Kruger (2010)
	\vspace{1mm}
\end{itemize}
\end{minipage} \\
\hline
Week 5 & 2/2 and 2/4 &\begin{minipage}{.6\textwidth}
\begin{itemize} \itemsep-0.4em
	\vspace{1mm}
	\item Topics: Review
	\item \textbf{Exam 1: Thursday, 2/4}
	\vspace{1mm}
\end{itemize}
\end{minipage} \\
\hline
Week 6 & 2/9 and 2/11 & \begin{minipage}{.6\textwidth}
\begin{itemize} \itemsep-0.4em
	\vspace{1mm}
	\item Topic: Family Policy Legitimacy, The Family Impact Lens
	\item Reading: Chapter 7 \& 8
	\item Due: My Own Worst Enemy 2/11 5PM
	\vspace{1mm}
\end{itemize}
\end{minipage} \\
\hline
Week 7 & 2/16 and 2/18 & \begin{minipage}{.6\textwidth}
\begin{itemize} \itemsep-0.4em
	\vspace{1mm}
	\item Topic: The Theory of Paradox
	\item Reading: Chapter 9
	\vspace{1mm}
\end{itemize}
\end{minipage} \\
\hline
Week 8 & 2/23 and 2/25 &\begin{minipage}{.6\textwidth}
\begin{itemize} \itemsep-0.4em
	\vspace{1mm}
	\item Topic: Roots of Family Policy
	\item Reading: Chapter 10
	\item Due: Meet the Press 2/25 5PM
	\vspace{1mm}
\end{itemize}
\end{minipage} \\
\hline
Week 9 & 3/1 and 3/3 & \begin{minipage}{.6\textwidth}
\begin{itemize} \itemsep-0.4em
	\vspace{1mm}
	\item Topics: Policy Makers and the Policy Process
	\item Reading: Chapter 11
	\item Extra Credit Due 3/4 5PM
	\vspace{1mm}
\end{itemize}
\end{minipage} \\
\hline
Spring Break! & 3/8 and 3/10 & \begin{minipage}{.6\textwidth}
\begin{itemize} \itemsep-0.4em
	\vspace{1mm}
	\item Topic: Relaxation!
	\vspace{1mm}
\end{itemize}
\end{minipage} \\
\hline
Week 10 & 3/15 and 3/17 & \begin{minipage}{.6\textwidth}
\begin{itemize} \itemsep-0.4em
	\vspace{1mm}
	\item Assignment: Go Vote on 3/15
	\item Topic: Advocacy or Education
	\item Reading: Chapter 15; Lee \& Hsieh (2013)
	\vspace{1mm}
\end{itemize}
\end{minipage} \\
\hline
Week 11 & 3/22 and 3/24 & \begin{minipage}{.6\textwidth}
\begin{itemize} \itemsep-0.4em
	\vspace{1mm}
	\item Topic: Review	
	\item \textbf{Exam 2: Thursday, 3/24}
	\vspace{1mm}
\end{itemize}
\end{minipage} \\
\hline
Week 12 & 3/29 and 3/31 & \begin{minipage}{.6\textwidth}
\begin{itemize} \itemsep-0.4em
	\vspace{1mm}
	\item Topic: Evidence-Based Family Policy
	\item Reading: Chapter 13	
	\vspace{1mm}
\end{itemize}
\end{minipage} \\
\hline
Week 13 & 4/5 and 4/7 & \begin{minipage}{.6\textwidth}
\begin{itemize} \itemsep-0.4em
	\vspace{1mm}
	\item Topic: Family Impact Analysis
	\item Reading: Chapter 12
	\item Due: Family Impact Analysis 4/7 5PM
	\vspace{1mm}
\end{itemize}
\end{minipage} \\
\hline
Week 14 & 4/12 and 4/14 & \begin{minipage}{.6\textwidth}
\begin{itemize} \itemsep-0.4em
	\vspace{1mm}
	\item Topic: Current Political Issues
	\item Reading: TBA
	\item Due: Advocacy Project Part 1 \& 2 4/14 5PM
	\vspace{1mm}
\end{itemize}
\end{minipage} \\
\hline
Week 15 & 4/19 and 4/21 & \begin{minipage}{.6\textwidth}
\begin{itemize} \itemsep-0.4em
	\vspace{1mm}
	\item Topic: Careers in Family Policy
	\item Reading: Chapter 14; Fischhoff (2013)
	\item Due: Advocacy Project Part 3 4/21 5PM
	\vspace{1mm}
\end{itemize}
\end{minipage} \\
\hline
Finals Week & 4/28 & \begin{minipage}{.6\textwidth}
\begin{itemize} \itemsep-0.4em
	\vspace{1mm}
	\item \textbf{Final Exam: Thursday, 4/28, 7:30 -– 9:30AM}
	\vspace{1mm}
\end{itemize}
\end{minipage} \\
\hline
\end{tabular} 
\end{table}

\end{document}




