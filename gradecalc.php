<html>
<head>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" >
<meta name="viewport" content="width=device-width, initial-scale=1" >
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" >
<title>CHD4615 Grade Calculator</title>

	<!-- Google Tag Manager -->
		<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-KL4WL7"
		height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
		<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
		new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
		j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
		'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
		})(window,document,'script','dataLayer','GTM-KL4WL7');
		</script>
	<!-- End Google Tag Manager -->
<?php



$analysis = filter_input(INPUT_GET, 'analysis', FILTER_SANITIZE_SPECIAL_CHARS);
$views = filter_input(INPUT_GET, 'views', FILTER_SANITIZE_SPECIAL_CHARS);
$presentation = filter_input(INPUT_GET, 'presentation', FILTER_SANITIZE_SPECIAL_CHARS);
$exam1 = filter_input(INPUT_GET, 'exam1', FILTER_SANITIZE_SPECIAL_CHARS);
$exam2 = filter_input(INPUT_GET, 'exam2', FILTER_SANITIZE_SPECIAL_CHARS);
$exam3 = filter_input(INPUT_GET, 'exam3', FILTER_SANITIZE_SPECIAL_CHARS);
$extra1 = filter_input(INPUT_GET, 'extra1', FILTER_SANITIZE_SPECIAL_CHARS);
$extra2 = filter_input(INPUT_GET, 'extra2', FILTER_SANITIZE_SPECIAL_CHARS);
$extra3 = filter_input(INPUT_GET, 'extra3', FILTER_SANITIZE_SPECIAL_CHARS);
$extra4 = filter_input(INPUT_GET, 'extra4', FILTER_SANITIZE_SPECIAL_CHARS);
$missed = filter_input(INPUT_GET, 'missed', FILTER_SANITIZE_SPECIAL_CHARS);
$attendance = filter_input(INPUT_GET, 'attendance', FILTER_SANITIZE_SPECIAL_CHARS);
$disagreeable = filter_input(INPUT_GET, 'disagreeable', FILTER_SANITIZE_SPECIAL_CHARS);
$enemy = filter_input(INPUT_GET, 'enemy', FILTER_SANITIZE_SPECIAL_CHARS);
$press = filter_input(INPUT_GET, 'press', FILTER_SANITIZE_SPECIAL_CHARS);
$impact = filter_input(INPUT_GET, 'impact', FILTER_SANITIZE_SPECIAL_CHARS);
$advocacy1 = filter_input(INPUT_GET, 'advocacy1', FILTER_SANITIZE_SPECIAL_CHARS);
$advocacy2 = filter_input(INPUT_GET, 'advocacy2', FILTER_SANITIZE_SPECIAL_CHARS);
$advocacy3 = filter_input(INPUT_GET, 'advocacy3', FILTER_SANITIZE_SPECIAL_CHARS);
$total = filter_input(INPUT_GET, 'total', FILTER_SANITIZE_SPECIAL_CHARS);

if (isset($total)){
	$sum = $total;
} else {
	$sum = $analysis + $views + $presentation + $exam1 + $exam2 + $exam3 + $extra1 + $extra2 + $extra3 + $extra4 + $missed + $attendance + $disagreeable + $enemy + $press + $impact + $advocacy1 + $advocacy2 + $advocacy3;
	$total = $sum;
}

if ($sum>400){
	$grade = "<span style='color: green; text-size: 4em;'>You've done better than perfect! A! </span>";
} elseif ($sum >= 368) {
	$grade = "<span style='color: green;'> A </span>";
} elseif ($sum >= 360) {
	$grade = "A-";
} elseif ($sum >= 352) {
	$grade = "B+";
} elseif ($sum >= 328) {
	$grade = "B";
} elseif ($sum >= 320) {
	$grade = "B-";
} elseif ($sum >= 312) {
	$grade = "C+";
} elseif ($sum >= 288) {
	$grade = "C";
} elseif ($sum >= 280) {
	$grade = "C-";
} elseif ($sum >= 272) {
	$grade = "D+";
} elseif ($sum >= 248) {
	$grade = "D";
} elseif ($sum >= 240) {
	$grade = "D-";
} else {
	$grade = "F";
}

?>


<style>
body {
	padding: 1em;
}

</style>

</head>
<body>
<div class="container-fluid">
<div class="row">
<div class="page-header col-md-8 col-md-offset-2">
  <h1>Grade Calculator <small>for CHD4615 Fall 2015</small></h1>
</div>
</div>
<div class="row">
<form action="gradecalc.php" method="GET" class=" col-md-8 col-md-offset-2" >
	<div class="row">
	
	<legend>Calculate by Assignment</legend>
<!-- Old version
	<div class="form-group form-inline">
		<label class="control-label col-md-9 col-md-" for="analysis">Analysis of a Current Family Policy Issue (<?php echo $analysis?>/80pts = <?php echo round($analysis /80 * 100, 2)?>%)</label>
		<input class="form-control col-md-3" type="number" min="0" max="80" name="analysis" value="<?php echo $analysis?>">
	</div>
	<div class="form-group form-inline">
		<label class="control-label col-md-9" for="views">Views of a Controversial Family Policy (<?php echo $views?>/80pts = <?php echo round($qual /80 * 100, 2)?>%)</label>
		<input class="form-control col-md-3" type="number"  min="0" max="80" name="views" value="<?php echo $views?>">	
	</div>
	<div class="form-group form-inline">
		<label class="control-label col-md-9 col-md-9" for="presentation">Presentation of a Current Family Policy Issue (<?php echo $presentation?>/40pts = <?php echo round($presentation /40 * 100, 2)?>%)</label>
		<input class="form-control col-md-3" type="number" name="presentation" min="0" max="40" value="<?php echo $presentation?>">
	</div>
-->
	<div class="form-group form-inline">
		<label class="control-label col-md-9 col-md-9" for="disagreeable">A Disagreeable Conversation (<?php echo $disagreeable ?>/20pts = <?php echo round($disagreeable /20 * 100, 2)?>%)</label>
		<input class="form-control col-md-3" type="number" name="disagreeable" min="0" max="20" value="<?php echo $disagreeable ?>">
	</div>
	<div class="form-group form-inline">
		<label class="control-label col-md-9 col-md-9" for="enemy">My Own Worst Enemy (<?php echo $enemy?>/20pts = <?php echo round($enemy /20 * 100, 2)?>%)</label>
		<input class="form-control col-md-3" type="number" name="enemy" min="0" max="20" value="<?php echo $enemy ?>">
	</div>
	<div class="form-group form-inline">
		<label class="control-label col-md-9 col-md-9" for="press">Meet the Press (<?php echo $press?>/40pts = <?php echo round($press /40 * 100, 2)?>%)</label>
		<input class="form-control col-md-3" type="number" name="press" min="0" max="40" value="<?php echo $press ?>">
	</div>
	<div class="form-group form-inline">
		<label class="control-label col-md-9 col-md-9" for="impact">Family Impact Analysis (<?php echo $impact?>/60pts = <?php echo round($impact /60 * 100, 2)?>%)</label>
		<input class="form-control col-md-3" type="number" name="impact" min="0" max="60" value="<?php echo $impact ?>">
	</div>
	<div class="form-group form-inline">
		<label class="control-label col-md-9 col-md-9" for="advocacy1">Advocacy Project - Part I (<?php echo $advocacy1?>/20pts = <?php echo round($advocacy1 /20 * 100, 2)?>%)</label>
		<input class="form-control col-md-3" type="number" name="advocacy1" min="0" max="20" value="<?php echo $advocacy1 ?>">
	</div>
	<div class="form-group form-inline">
		<label class="control-label col-md-9 col-md-9" for="advocacy2">Advocacy Project - Part II (<?php echo $advocacy2?>/30pts = <?php echo round($advocacy2 /30 * 100, 2)?>%)</label>
		<input class="form-control col-md-3" type="number" name="advocacy2" min="0" max="30" value="<?php echo $advocacy2 ?>">
	</div>
	<div class="form-group form-inline">
		<label class="control-label col-md-9 col-md-9" for="advocacy3">Advocacy Project - Part III (<?php echo $advocacy3?>/10pts = <?php echo round($advocacy3 /10 * 100, 2)?>%)</label>
		<input class="form-control col-md-3" type="number" name="advocacy3" min="0" max="10" value="<?php echo $advocacy3 ?>">
	</div>
	<div class="form-group form-inline">
		<label class="control-label col-md-9 col-md-9" for="exam1">Exam 1 (<?php echo $exam1?>/60pts = <?php echo round($exam1 /60 * 100, 2)?>%)</label>
		<input class="form-control col-md-3" type="number" name="exam1"  min="0" max="60" value="<?php echo $exam1?>">
	</div>
	<div class="form-group form-inline">
		<label class="control-label col-md-9 col-md-9" for="exam2">Exam 2 (<?php echo $exam2?>/60pts = <?php echo round($exam2 /60 * 100, 2)?>%)</label>
		<input class="form-control col-md-3" type="number" name="exam2" min="0" max="60" value="<?php echo $exam2?>">
	</div>
	<div class="form-group form-inline">
		<label class="control-label col-md-9 col-md-9" for="exam3">Exam 3(<?php echo $exam3?>/60pts = <?php echo round($exam3 /60 * 100, 2)?>%)</label>
		<input class="form-control col-md-3" type="number" name="exam3" min="0" max="65" value="<?php echo $exam3?>">
	</div>
	<div class="form-group form-inline">
		<label class="control-label col-md-9 col-md-9" for="extra1">Extra Credit Assignment - Letter (<?php echo $extra1?>/5pts = <?php echo round($extra1 /5 * 100, 2)?>%)</label>
		<input class="form-control col-md-3" type="number" name="extra1"  min="0" max="5" value="<?php echo $extra1?>">
	</div>
	<div class="form-group form-inline">
		<label class="control-label col-md-9 col-md-9" for="extra2">Extra Credit Assignment - Wiki (<?php echo $extra2?>/5pts = <?php echo round($extra2 /5 * 100, 2)?>%)</label>
		<input class="form-control col-md-3" type="number" name="extra2"  min="0" max="5" value="<?php echo $extra2?>">
	</div>
	<div class="form-group form-inline">
		<label class="control-label col-md-9 col-md-9" for="extra3">Extra Credit Assignment - Go Foster (<?php echo $extra3?>/5pts = <?php echo round($extra3 /5 * 100, 2)?>%)</label>
		<input class="form-control col-md-3" type="number" name="extra3"  min="0" max="5" value="<?php echo $extra3?>">
	</div>
	<div class="form-group form-inline">
		<label class="control-label col-md-9 col-md-9" for="extra4">Extra Credit Assignment - Research Study (<?php echo $extra4?>/5pts = <?php echo round($extra4 /5 * 100, 2)?>%)</label>
		<input class="form-control col-md-3" type="number" name="extra4"  min="0" max="5" value="<?php echo $extra4?>">
	</div>
<!-- Old version
	<div class="form-group form-inline">
		<label class="control-label col-md-9 col-md-9" for="extra3">Extra Credit Assignment - Note-Taking (<?php echo $extra3?>/5pts = <?php echo round($extra3 /5 * 100, 2)?>%)</label>
		<input class="form-control col-md-3" type="number" name="extra3"  min="0" max="5" value="<?php echo $extra3?>">
	</div>
	<div class="form-group form-inline">
		<label class="control-label col-md-9 col-md-9" for="missed">Missed Presentation Days (<?php echo $missed?>/-7pts = <?php echo round($missed /7 * 100, 2)?>%)</label>
		<input class="form-control col-md-3" type="number" name="missed"  min="-7" max="0" value="<?php echo $missed?>">
	</div>
-->
	<div class="form-group form-inline">
		<label class="control-label col-md-9 col-md-9" for="attendance">Attendance (<?php echo $attendance?>/20pts = <?php echo round($attendance /20 * 100, 2)?>%)</label>
		<input class="form-control col-md-3" type="number" name="missed"  min="0" max="20" value="<?php echo $attendance?>">
	</div>
	</div>
	<div class="row">
	<div class="form-group form-inline">
	<button type="submit" class="btn btn-success ">Calculate!</button>
	</div>
	</div>

</form>
</div>

<div class="row">
<form action="gradecalc.php" method="GET" class=" col-md-8 col-md-offset-2" >
	<div class="row">
	<legend>Calculate by Total Points</legend>
	<div class="form-group form-inline">
		<label class="control-label col-md-9" for="total">Total Points</label>
		<input class="form-control col-md-3" type="number" step="0.01" name="total" value="<?php echo $total?>">
	</div>
	</div>
	<div class="row">
	<div class="form-group form-inline">
	<button type="submit" class="btn btn-success ">Calculate!</button>
	</div>
	</div>

</form>
</div>

<div class="row">
	<div class="col-md-8 col-md-offset-2 centered">
	<hr>
	<dl class="dl-horizontal lead">
	<dt>Current Points: </dt><dd><?php echo $sum ?> of 400 Possible</dd>
	<dt>Percentage: </dt><dd><?php echo ($sum / 400)*100 ?>%</dd>
	<dt>Grade: </dt><dd><?php echo $grade ?></dd>
	</dl>
	<h2>Questions? <a href="mailto:asb12f@my.fsu.edu">Email me!</a></h2>
	</div>
</div>
</body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
</html>