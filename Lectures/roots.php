<?php
# toggle whether page is live
$live = True;
require_once('redirect.php');

$title = 'The Roots of Family Policy';
$id = 'night';
$theme = 'css/theme/'.$id.'.css';


## Other Available Themes
## $theme = 'http://pablocubi.co/mozreveal/css/theme/one-mozilla.css';
## Predefined Themes
## beige, blood, default, moon, night, serif, simple, sky, sky-jeopardy, solarized
## $id = 'beige';
## $theme = 'css/theme/'.$id.'.css';

require_once('header.php');
?>
<!-- each slide is a section; everything else is automated in the support PHP -->
<section>
	<h1>Agenda</h1>
	<ol>
		<li>Why Study More History?</li>
		<li>Origins of Family Policy</li>
		<li>Policies that Endure</li>
	</ol>
</section>
<section>
	<section>
		<h2>Why Study More History?</h2>
	</section>
	<section>
		<blockquote >&#8220;The essence of doing science is discovery.<br /> And that means more than uncovering new facts. <br />The important thing may not be new facts.<br /> It may more likely be new ways of thinking about old facts.&#8221; <br / >&ndash; Priestley </blockquote>
	</section>
	<section>
		<h3>History as a tool for Change</h3>
		<ol>
			<li class='fragment'>By applying modern theories and understandings to past policy, we can better understand why they failed or succeeded</li>
			<li class='fragment'>By understanding what has and has not worked in the past, we can better develop future policies</li>
		</ol>
	</section>
	<section>
		<h3>Reviewing the Recent History</h3>
		<ul>
			<li><span style='color: magenta;'>1970s</span> &ndash; <span class='fragment'>Family Policy first defined, first notions of including families in policy decisions</span></li>
			<li><span style='color: magenta;'>1980s</span> &ndash; <span class='fragment'>Family Policy falls out of favor after White House Conference on Families</span></li>
			<li><span style='color: magenta;'>1990s </span> &ndash; <span class='fragment'>Resurgence in interest; exploration of the legitimacy of family policy</span></li>
			<li><span style='color: magenta;'>2000s </span>&ndash; <span class='fragment'>Continued growth; exploration of the rationale - &#8220;Why do we need family policy, and does it bring a unique value-added perspective to social policy?&#8221;</span></li>
		</ul>
	</section>
</section>
<section>
	<section>
		<h2>Origins of Family Policy</h2>
	</section>
<!---	
	<section>
	<iframe width="640" height="360" src="https://www.youtube.com/embed/6QGbsNIZ-uo" frameborder="0" allowfullscreen></iframe>
	</section>
--->
	<section>
		<h3>Women's Advocacy, 1890 &ndash; 1920</h3>
		<p>Historical Context</p>
		<ul>
			<li>Growth of Industrialization, Immigration, and Urbanization</li>
			<li>High Unemployment, Low Wages, Homelessness, and Poverty</li>
			<li>High rates of workplace injury, high levels of infant / child mortality</li>
			<li>9% of children lived with single mothers, most often widows who were unable to earn a living wage</li>
			<li>Lack of family income resulted in children being pressed into labor, and mass institutionalization of children</li>
		</ul>
	</section>
	<section>
		<h3>Women's Advocacy, 1890 &ndash; 1920 (6:55)</h3>
		<iframe width="640" height="360" src="https://www.youtube.com/embed/gdE5LaDY0Lk" frameborder="0" allowfullscreen></iframe>
	</section>
	<section>
		<h3>Women's Advocacy, 1890 &ndash; 1920</h3>
		<p>In large part due to the growth of women's literacy in the preceding 50 years, women were better educated and more influential than ever before in US history</p>
		<div class='fragment'>
		<p>Women's social clubs organized to create political action, and eventually grew to form national women's organizations</p>
		<ul>
			<li>Congress of Mothers</li>
			<li>Daughters of the American Revolution</li>
			<li>National American Woman Suffrage Assocation</li>
			<li>National Consumers' League</li>
			<li>National Women's Trade Union League</li>
		</ul>
		</div>
	</section>
	<section>
		<h3>Women's Advocacy, 1890 &ndash; 1920</h3>
		<p><a style='color:gold;' href='https://www.youtube.com/watch?v=8_FKm2-vDl0'>Woman's Christian Temperance Union (WCTU)</a></p>
		<p class='fragment'>The most influential women's group of the time</p>
		<p class='fragment'>Not limited to temperance; 25 / 39 departments focused on other issues</p>
		<p class='fragment'>Heavily invested in social services - nurseries, schools, missions, and dispensaries</p>
		<p style='color:gold;'>General Federation of Women's Clubs</p>
		<p class='fragment'>Major advocacy surrounding labor issues, especially work conditions for women and children</p>	
	</section>
	<section>
		<h3>Women's Advocacy, 1890 &ndash; 1920</h3>
		<p>Framed issues as family preservation</p>
		<p>Achievements:</p>
		<ul>
			<li class='fragment'>Compulsory Education</li>
			<li class='fragment'>Child Labor Reforms</li>
			<li class='fragment'>Pensions for Widows</li>
			<li class='fragment'>Limiting hours that Women wage earners could work</li>
			<li class='fragment'>Minimum wage for Women</li>
			<li class='fragment'>Women's Suffrage (1920)</li>
			<li class='fragment'>Prohibition (1920) <br /><iframe width="320" height="180" src="https://www.youtube.com/embed/r-N0XlqrOno" frameborder="0" allowfullscreen></iframe></li>
		</ul>
		
	</section>
	<section>
		<h3>Child and Family Saving, 1900 &ndash; 1930</h3>
		<p>Historical Context</p>
		<ul>
			<li>Tripled divorce rate 1890 &ndash; 1910</li>
			<li>30% drop in birth rate 1880 &ndash; 1920</li>
			<li>Infant mortality 2x higher than Western Europe in 1918</li>
			<li>80% of mothers lacked prenatal care, 23,000 annual deaths in childbirth</li>
		</ul>
	</section>
	<section>
		<h3>Child and Family Saving, 1900 &ndash; 1930</h3>
		<p>&#8220;If the government can have a department to take such an interest in the cotton crop, why can't it have a bureau to look after the nation's child crop?&#8221; <br /> &ndash; Florence Kelley</p>
		<p class='fragment'><a href='https://cb100.acf.hhs.gov/Cb_ebrochure'>The Children's Bureau founded in 1912</a>, was the first US agency to focus on child welfare and outcomes</p>
	</section>
	<section>
		<h3>Child and Family Saving, 1900 &ndash; 1930</h3>
		<p>Using research and advocacy, the Children's Bureau began providing education, services, and policy advocacy to save infants and children by targeting parenting and family life</p>
		<iframe width="320" height="180" src="https://www.youtube.com/embed/8fNEbCUNNUg" frameborder="0" allowfullscreen></iframe>
	</section>
	<section>
		<h3>Child and Family Saving, 1900 &ndash; 1930</h3>
		<p>The Children's Bureau disseminated knowledge through books, public events (Mother's Day &amp; National Baby Week), and motion pictures</p>
		<p class='fragment'>The Children's Bureau promoted policies to enhance living standards, including:</p>
		<ul>
		<li class='fragment'>paying half of soldier's pay directly to their families</li>
		<li class='fragment'>creating family allowances for families with 4 or more children</li>
		<li class='fragment'>extending death and disability benefits</li>
		<li class='fragment'>passing the <span style='font-weight: 900; color:gold;'>Sheppard Towner Act</span>, which provided federal funding for maternity and childcare</li>
		</ul>
		<p class='fragment'>The act was opposed by the AMA (&#8220;German paternalism&#8221;), but passed by wide margins</p>
		<p class='fragment'>Due to recurrent conflict, the program was defunded in 1929.</p>
	</section>
	<section>
		<h3>Social Security, 1930 &ndash; 1940</h3>
		<p>Historical Context</p>
		<ul>
			<li>1929 Stock Market Crash, Depression</li>
			<li>1/3 US Labor Force Unemplolyed, 1933</li>
			<li>20% Decline in birth and marriage rates, 1928 &ndash; 1932</li>
		</ul>
		<p class='fragment'>Social scientists (&#8220;<span style='color:yellow;'>Hoover's Technocrats</span>&#8221;) at the <a href='https://archive.org/stream/recentsocialtren01unitrich/recentsocialtren01unitrich_djvu.txt'>President's Research Committee on Social Trends</a></p>
		<p>Key Findings:</p>
			<ul>
				<li class='fragment'>Declines due to failings of families; women more useful as workers than mothers</li>
				<li class='fragment'>Favored privatization / state acquisition of family functions (childcare, cooking, education, healthcare, laundering, etc.)</li>
				<li class='fragment'>&#8220;Concern should no longer focus on family strength... Instead, attention should be on 'the individualization of the members of the family'.&#8221;</li>
			</ul>
	</section>
	<section>
		<h3>Social Security, 1930 &ndash; 1940</h3>
		<p><span style='color:yellow;'>American Maternalists</span></p>
		<ul>
			<li class='fragment'>Disagreed with the Technocrats; emphasized family roles and committment</li>
			<li class='fragment'>Industrialization as a threat to families</li>
			<li class='fragment'>Argued for valuing women's labor in the home; pushed for living wage for breadwinner male workers</li>
		</ul>
	</section>
	<section>
		<h3>Social Security, 1930 &ndash; 1940</h3>
		<p><span style='color:yellow;'>The New Deal</span></p>
		<ul>
			<li class='fragment'>Roosevelt was heavily influenced by the Maternalists</li>
			<li class='fragment'>Emphasized the role of men as breadwinners; very popular</li>
		</ul>
		<div class='fragment'>
		<p><a style='color:yellow;' href='http://www.ssa.gov/history/35act.html'>The Social Security Act of 1935</a></p>
		<iframe width="320" height="180" src="https://www.youtube.com/embed/Vi4KDdHltIs" frameborder="0" allowfullscreen></iframe>
		<iframe width="320" height="180" src="https://www.youtube.com/embed/iB5VzR7urkY" frameborder="0" allowfullscreen></iframe>
		</div>
	</section>
	<section>
		<h3>Social Security, 1930 &ndash; 1940</h3>
		<ul>
			<li class='fragment'>The cornerstone of the New Deal, and a foundation of modern US social programs</li>
			<li class='fragment'>One of the first and most effective antipoverty programs</li>
			<li class='fragment'>Created Old Age pensions</li>
			<li class='fragment'>Enabling States provide forblind persons, dependent and crippled children, maternal and child welfare, public health, and the administration of their unemployment compensation laws</li>
		</ul>
	</section>
	<section>
		<h3>Social Security, 1930 &ndash; 1940</h3>
		<p><span style='color:yellow;'>The Social Security Act Ammendment of 1939</span></p>
		<ul>
			<li class='fragment'>Extended pensions to survivors and dependents, moving Social Security from an Individual to Family oriented program</li>
			<li class='fragment'>Widowed mothers received 75% of husband's pension unless remarried or working</li>
			<li class='fragment'>Surviving children received 50% of husband's pension unless remarried or working</li>
			<li class='fragment'>Financial penalties for families with divorce, childlessness, illegitimacy, and maternal employment</li>
		</ul>
	</section>
</section>
<section>
	<section>
		<h2>Policies that Endure</h2>
	</section>
	<section>
		<p>The policies of the early 1900s continue to shape how we view families and social problems today.</p>
		<p>Reflecting on this, we can identify 6 policy trends that lead to enduring policy:</p>
		<ol class='fragment'>
			<li>Family Policies Move Forward When Legitimated by Relevant Research and Theory</li>
			<li>The Family Perspective is Influential When Relevant Research and Theory are Communicated to Policymakers</li>
			<li>Family Policies Move Forward When Policymakers and the Public Support Structural Rather than Individual Explanations for Social Problems</li>
			<li>Family Policy Moves forward When There is a Broad Interdisciplinary Focus on Families</li>
			<li>Family Policy Moves Forward When There are Formal Structures in Place</li>
			<li>Family Policy Moves Forward When There is Broad-Based Citizen Activism</li>
		</ol>
	</section>
	<section>
		<h3>Relevant Research and Theory</h3>
		<p class='fragment'>Research data can raise awareness of social problems (1900's infant mortality, etc) and motivate action</p>
		<p class='fragment'>Theories can change how we view practices and policy (child labor, etc)</p>
		<p class='fragment'>Data tends to be more effective when highlighting risks or crises; normative data isn't exciting &#9785;</p>
		<p class='fragment'>Theory and data can be counterproductive or distract from issues (Hoover's Technocrats, individualistic data lense, etc)</p>		
	</section>
	<section>
		<h3>Communication to Policymakers</h3>
		<p class='fragment'>Data and theory must be presented to policymakers in ways that make sense to them and accomodate how they think about problems (cotton crop vs child crop, etc)</p>
		<div class='fragment'>
		<p>3-prong approach to dissemination: </p>
		<ol>
			<li>Encourage academics to engage with policymakers</li>
			<li>Package information to meet policymaker's information needs</li>
			<li>Encourage policymakers to view policy through the Family Impact Lens</li>
		</ol>
		</div>
	</section>
	<section>
		<h3>Structural Rather than Individual Explanations</h3>
		<p class='fragment'>Family policies are more likely to be implemented when structural explanations are in favor (infant mortality, mass unemployment, etc)</p>
		<p class='fragment'>Challengers vs Incumbents</p>
		<p class='fragment'>During political campaigns and First Terms</p>
	</section>
	<section>
		<h3>Broad Interdisciplinary Focus on Families</h3>
		<p class='fragment'>Policies and policy agendas that focus on mutliple interrelated areas tend to be more successful (progressive womens' emphasis on education, labor, and rights; extension of social security to survivors and dependents; etc)</p>
		<p class='fragment'>Family perspectives align well with integrated service approaches; canalization of policy and programs emphasizes individualism</p>
	</section>
	<section>
		<h3>Formal Structures</h3>
		<p class='fragment'>Formal government structures, like the Children's Bureau, can drive funding and policy towards family goals</p>
		<p class='fragment'>Formal structures create visibility and wield political influence</p>
	</section>
	<section>
		<h3>Broad-Based Citizen Activism</h3>
		<p class='fragment'>Citizens' movements and organized social groups can advocate for major policy change (progressive era women's groups, etc)</p>
		<p class='fragment'>Partnerships between govenment and voluntary organizations are often more effective than either party working independently</p>
		<p class='fragment'>To be effective, groups need leadership and active participation from members; disorganization and passive membership are ineffective</p>
		<p class='fragment'>The most effective groups tend to be broad, encompassing people from different economic and social backgrounds</p>
		<p class='fragment'>Groups tend to be more influential when they are independent of political affiliation</p>
	</section>
</section>
<section>
	<h1>Next Up:</h1>
	<h2><a href="./?lesson=process">Policy Makers and the Policy Process</a></h2>
</section>
