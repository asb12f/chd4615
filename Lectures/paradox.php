<?php

# toggle whether page is live
$live = True;
require_once('redirect.php');

$title = 'The Theory of Paradox';
$id = 'night';
$theme = 'css/theme/'.$id.'.css';


## Other Available Themes
## $theme = 'http://pablocubi.co/mozreveal/css/theme/one-mozilla.css';
## Predefined Themes
## beige, blood, default, moon, night, serif, simple, sky, sky-jeopardy, solarized
## $id = 'beige';
## $theme = 'css/theme/'.$id.'.css';

require_once('header.php');
?>
<!-- each slide is a section; everything else is automated in the support PHP -->
<section>
	<h1>Agenda</h1>
	<ol>
		<li>The Theory of Paradox</li>
		<li>Three Worldviews of Family Change</li>
		<li>Bridging Controversy</li>
	</ol>
</section>
<section>
	<section>
		<h2>The Theory of Paradox</h2>
	</section>
	<section>
		<h3>What is your earliest political memory?</h3>
		<h3>How have your political views changed (or not changed!) over time?</h3>
	</section>
	<section>
		<h3>The Divide</h3>
		<p>US politicians are currently more polarized than ever.</p>
		<figure class='fragment'>
		<img height='450px' src='../images/political1.jpg' alt='partisan divides'>
		<figcaption><a href='https://www.reddit.com/r/dataisbeautiful/comments/1q7b3s/voting_relationships_between_senators_in_the/'>Network visualization of how often senators vote together, 113th Congress</a></figcaption>
		</figure>	
	</section>
	<section>
		<h3>The Divide</h3>
		<p>So are US citizens.</p>
		<figure class='fragment'>
		<img height='450px' src='../images/political2.gif' alt='public divide'>
		<figcaption><a href='http://www.pewresearch.org/fact-tank/2014/06/12/7-things-to-know-about-polarization-in-america/'>US Public Polarization Over Time, Pew Research Center</a></figcaption>
		</figure>
	</section>
	<section>
		<h3>Reconciling Extreme Poles</h3>
		<p>Rigid polarization leads to increasing tension, reduced compromise, and policy stagnation</p>
		<p>To help us move past polarized positions, we can apply the Theory of Paradox and the Three Worldviews of Family Change.</p>
	</section>
	<section>
		<h3>The Theory of Paradox</h3>
		<p>&#8220;The theory of paradox is a conceptual framework for moving beyond the differences of seemingly antithetical viewpoints by recognizing the validity and utility of each and, in so doing, framing policy debate in a way that has potential to foster compromise.&#8221;</p>
		<p>Every policy position contains it's opposing dialectical position; sometimes it can be hard to find</p>
		<p>Emphasizes compromise as a virtue of good policymaking</p>
	</section>
	<section>
		<h3>Rappaport</h3>
		<p><a href='http://grow.ie/wp-content/uploads/2012/03/In-Praise-of-Paradox-A-Social-Policy-of-Empowerment-Over-Prevention-.pdf'>Julian Rappaport</a> - <span class='fragment'>the father of community psychology</span></p>
		<p class='fragment'><a href='http://psycnet.apa.org/index.cfm?fa=search.displayRecord&uid=1982-01824-001'>Rappaport, J. (1981). In praise of paradox: A social policy of empowerment over prevention. <i>American Journal of Community Psychology</i>, <i>9</i>, 1-25.</a></p>
	</section>
	<section>
		<h3>Rappaport's True Paradox</h3>
		<p>Prevention <span class='fragment'>is an extension of a needs model that views people dependent actors</span></p>
		<p>Advocacy <span class='fragment'>is an extension of a rights model that views people as independent actors</span></p>
		<p>True Paradox <span class='fragment'> &ndash; Both of these are one-sided and superficially irreconcilable, yet upon closer scrutiny are valid</span></p>	
	</section>
	<section>
		<h3>Resolving The True Paradox</h3>
		<p>By recognizing the paradox - two dialectical, equally valid, and intertwined ideals - we can resolve tension</p>
		<p>Two strategies for paradox resolution:</p>
		<ol>
			<li>Pushing in the ignored direction (Balance)</li>
			<li>Pursuing both opposing, yet valid, policy goals (Synthesis)</li>
		</ol>
	</section>
	<section>
		<h3>Pushing in the Ignored Direction (Balance)</h3>
		<p>Used when an imbalance exists; <span style='color:yellow;'>this requires perspective</span></p>
		<p>Examples:</p>
		<ul>
			<li><span style='font-weight:900; color:magenta;'>TANF</span><span class='fragment'> &ndash; Caseload Reduction <span class='fragment'><br />vs Declines in Child Poverty</span></span></li>
			<li><span style='font-weight:900; color: magenta;'>Childcare</span><span class='fragment'> &ndash; Family Right to Choose Childcare Providers<span class='fragment'> <br />vs Govt Expectation of Quality Care when Funding Childcare</span></span></li>
			<li><span style='font-weight:900;color: magenta;'>Healthcare</span><span class='fragment'> &ndash; Individual Right to Self-Determination <span class='fragment'><br />vs Right of Family and Society to be Protected from Potential Harm</span></span></li>
			<li><span style='font-weight:900;color: magenta;'>Family Leave</span><span class='fragment'> &ndash; Employer Right to Establish Employee Expectations <span class='fragment'><br />vs Individual Duty to Care for Family</span></span></li>
			<li><span style='font-weight:900;color: magenta;'>Private/Charter Schools</span><span class='fragment'> &ndash; Family Right to make Decisions about Child's Education <span class='fragment'><br />vs Govt Expectation of Quality and Content in Education</span></span></li>
		</ul>
		<p class='fragment'><span style='color: red;'>Avoid the Pendulum</span>; use calibrated compensation!</p>
	</section>
	<section>
		<h3>Pursuing Both Goals Simultaneously (Synthesis)</h3>
		<p>Alternatively, we can target both ends of the policy continuum</p>
		<p>Examples:</p>
		<ul>
			<li><span style='font-weight:900;color: magenta;'>TANF</span><span class='fragment'> &ndash; Reduce Caseloads and Child Poverty</span></li>
			<li><span style='font-weight:900;color: magenta;'>Childcare</span><span class='fragment'> &ndash; Target both Family-Based and Center-Based Childcare</span></li>
			<li><span style='font-weight:900;color: magenta;'>Healthcare</span><span class='fragment'> &ndash; Create options for both voluntary and involuntary health care</span></li>
			<li><span style='font-weight:900;color: magenta;'>Family Leave</span><span class='fragment'> &ndash; Target employers and individual autonomy</span></li>
			<li><span style='font-weight:900;color: magenta;'>Private/Charter Schools</span><span class='fragment'> &ndash; Pursue education standards and education choice</span></li>
		</ul>
	</section>
	<section>
		<h3>Pursuing Both Goals Simultaneously (Synthesis)</h3>
		<p>This approach emphasizes synthesis; often a single policy can contribute to both ends of the dialectic simultaneously</p>
		<h4>Example: Working Parents' Responsibility to Support Their Children Economically</h4>
		<h5>Skocpol (1997)</h5>
		<p class='fragment'>Side A: Liberals tend to want to create social conditions for families to succeed, such as workforce training, increased wages, and expanded benefits</p>
		<p class='fragment'>Side B: Conservatives tend to want to shift the culture surrounding parents, promoting marriage, instilling values in children, and creating tougher consequences for policy violators</p>
		<p class='fragment'>Solution: Provide tax credits and benefits to families (liberal) that promote and reward changes in parenting culture (conservative)</p>
	</section>
	<section>
		<h3>Practice</h3>
		<p>What family policies are currently sources of tension in the US today? </p>
		<p>What are their dialectical positions? </p>
		<p>What solutions might we pursue?</p>
		<p style='color:yellow; text-align:center;'><br />Remember, it often takes time and careful analysis to develop real world solutions!</p>
	</section>
</section>
<section>
	<section>
		<h2>Three Worldviews of Family Change</h2>
	</section>
	<section>
		<h3>3 Camps vs 2 Parties</h3>
		<p>Families have changed over time; we often choose to think about this along party lines and embrace tension and stagnation.</p>
		<p>A more productive alternative could be to consider three worldviews, or &#8220;camps&#8221;, for considering family policy:</p>
		<ul>
			<li>The Concerned Camp<span class='fragment'> &ndash; family changes are bad</span></li>
			<li>The Satisfied / Sanguine Camp<span class='fragment'> &ndash; family changes are OK and beneficial</span></li>
			<li>The Impatient Camp<span class='fragment'> &ndash; family changes are too slow and insufficient</span></li>
		</ul>
	</section>
	<section>
		<h3>The Concerned Camp</h3>
		<p>Trends of Concern</p>
		<ol>
			<li>The Sexual Revolution (teen sex, nonmarital childbearing, etc)</li>
			<li>Increased Divorce Rates</li>
			<li>Increased Maternal Employment</li>
			<li>Declines in Fertility</li>
			<li>Serial Families</li>
		</ol>
		<p>Why might these be a cause for concern?</p>
	</section>
	<section>
		<h3>Values</h3>
		<ul>
			<li>Parental Commitment</li>
			<li>Marital Fidelity</li>
			<li>Individual Responsibility</li>
			<li>Civic Participation</li>
			<li>Anti-Individualism</li>
		</ul>
		<p class='fragment'>&#8220;Dark Side of Modernity &#8221;</p>
	</section>
	<section>
		<h3>Supporting Evidence</h3>
		<ul>
			<li class='fragment'>The Rise of Individualism </li>
			<li class='fragment'>Marriage in Decline
			<ul>
				<li>Fewer children live in married two-parent homes</li>
				<li>Only half of children in the US can expect to grow up with continuously married parents</li>
				<li>Increases in child poverty</li>
			</ul></li>
		</ul>
	</section>
	<section>
		<h3>Supporting Evidence</h3>
		<ul>
			<li class='fragment'>Threats to Optimal Married-Couple Family Functioning may Harm Children
			<ul>
				<li>Increased maternal employment</li>
				<li>Work - Family Time Conflicts</li>
				<li>Parental Guilt (Quantity vs Quality Time)</li>
			</ul></li>
			<li class='fragment'>Social &amp; Economic Value of Childrearing
			<ul>
				<li>Successful childrearing is key for raising the value of future generations</li>
				<li>ROI of Public Investments in Families
				<ul>
					<li>$\$10.50$ &ndash; $\$25$ for two-parent, middle income families</li>
					<li>$\$8$  &ndash; $\$18.50$ for single-parent, low income families</li>
				</ul></li>
			</ul></li>
		</ul>
	</section>
	<section>
		<h3>Policy Agenda</h3>
		<ul>
			<li class='fragment'>Cultural Campaign vs Legislative Campaign</li>
			<li class='fragment'>Return to the 1950's Nuclear Family?</li>
			<li class='fragment'>Support for naturally occurring family supports, not government</li>
			<li class='fragment'>Favoring Devolution <span class='fragment'>for more Local Solutions</span></li>
			<li class='fragment'>Focus is on families; assumed to help individuals indirectly</li>
		</ul>
		<p class='fragment'>Criticism: Anti-progressive?</p>
	</section>
	<section>
		<h3>The Satisfied Camp</h3>
		<p>Family changes as evidence of family adaptability to social and economic conditions</p>
		<p>Trends of interest:</p>
		<ol>
			<li class='fragment'>Increased rights and equality for oppressed groups</li>
			<li class='fragment'>Expanded roles for women - employment, social safety net</li>
			<li class='fragment'>Because divorce is available, increased marital satisfaction</li>
			<li class='fragment'>Expanded elder benefits and protections</li>
			<li class='fragment'>Child rights</li>
		</ol>
		<p class='fragment'>Social problems as a result of inadequate responses from social institutions, not families</p>
	</section>
	<section>
		<h3>Values</h3>
		<ul>
			<li>Importance of Marriage, Commitment, and Nurturance are Stable</li>
			<li>Changes in behavioral norms, not underlying values</li>
			<li>Independence and Individual Autonomy</li>
			<li>Individual Variation</li>
		</ul>
	</section>
	<section>
		<h3>Supporting Evidence</h3>
		<ul>
			<li class='fragment'>Public Opinion
			<ul>
				<li>Americans report high overall satisfaction with life</li>
				<li>Women slightly prefer working outside the home (50% vs 45%)</li>
				<li>Americans report enjoying flexibility surrounding lifestyle preferences, work choices</li>
				<li>Working mothers report less depression, sadness, and anger, and more happiness, smiling, and laughing than stay-at-home mothers</li>
			</ul></li>
		</ul>
	</section>
	<section>
		<h3>Supporting Evidence</h3>
		<ul>
			<li>Empirical Data
			<ul>
				<li>Concerns about youth and families have <a href='https://xkcd.com/1227/'>always existed</a>
					<ul>
						<li class='fragment'>&#8220;Unfortunately, the notion of marriage which prevails ... at the present time ... regards the institution as simply a convenient arrangement or formal contract ... This disregard of the sanctity of marriage and contempt for its restrictions is one of the most alarming tendencies of the present age.&#8221; <br /><span class='fragment'>&ndash;John Harvey Kellogg, Ladies&#39; guide in health and disease (1883)</span></li>
					</ul></li>
				<li class='fragment'>10-18 year olds are positive about future prospects</li>
				<li class='fragment'>Most students report having adults who care about them in their lives (95%)</li>
				<li class='fragment'>Overall HS Graduation rates, and scores on math and reading, are at an all time high</li>
				<li class='fragment'>Lowest teen birth rate since 1960 (2009); reduced child mortality</li>
			</ul></li>
		</ul>
	</section>
	<section>
		<h3>Supporting Evidence</h3>
		<ul>
			<li>Cross-Cultural Comparisons
			<ul>
				<li>Comparing US to Western Countries with similar rates of maternal employment, single parenthood, and divorce</li>
				<li class='fragment'>Larger Social Safety Nets == less decline in child well-being</li>
				<li class='fragment'>Smaller Social Safety Nets == higher child poverty, infant mortality, &amp; less access to healthcare</li>
			</ul></li>
		</ul>
	</section>
	<section>
		<h3>Policy Agenda</h3>
		<ul>
			<li class='fragment'>Need for greater institutional supports</li>
			<li class='fragment'>Broad health, housing, and income security policies</li>
			<li class='fragment'>Focus is on individuals; assumed to help family indirectly</li>
			<li class='fragment'>Women's equality</li>
		</ul>
		<p class='fragment'>Criticism: Women's Needs =/= Family Needs; May not reflect complex relationships of family life</p>
	</section>
	<section>
		<h3>The Impatient Camp</h3>
		<p>Change is good, but more changes is needed; demise of &#8220;normative&#8221; families is a good thing</p>
		<p>Areas of Focus:</p>
		<ol>
			<li class='fragment'>Society has not come to grips with diverse family forms (cohabitation, blended families, same-sex partners, single parents, multi-partner families, etc)</li>
			<li class='fragment'>Redefining Family - partner relationships, primary relationships, enduring intimate relationships, location for resource production and redistribution</li>
			<li class='fragment'>Family as an ideology, not an institution; creates power and privilege (SNAF)</li>
		</ol>
		<p class='fragment'>Social problems as a result of policymakers and academics being blind to issues of power inherent in family ideology</p>
	</section>
	<section>
		<h3>Values</h3>
		<ul>
			<li>Relativism <span class='fragment'>&ndash; No single truth, no preferred family form, no universal rule </span></li>
			<li>Pluralism <span class='fragment'>&ndash; embracing multiple belief systems simultaneously</span></li>
			<li>Diversity as a Positive; Embracing Fictive Kin and Multiple Partner Fertility</li>
			<li>Egalitarianism; Skepticism about Progress</li>
			<li>De-emphasis of the Individual; Emphasis on Social Construction</li>
		</ul>
	</section>
	<section>
		<h3>Supporting Evidence</h3>
		<ul>
			<li>Challenging Objectivity
			<ul>
				<li class='fragment'>Emphasis on Qualitative Research</li>
				<li class='fragment'>Acknowledge, but don't privilege, empirical research</li>		
			</ul></li>
			<li class='fragment'>Public Opinion Polls
			<ul>
				<li>Lack of respect and tolerance is seen as evidence of moral decline (2012)</li>
				<li>Shifts in attitudes towards diverse families (same-sex marriage, single-parents, open relationships, etc.)</li>
			</li></ul>
			<li class='fragment'>Historical Analysis &amp; Ethnographic Research
				<ul>
					<li>Greater recognition of minorities</li>
					<li>Recognition of complex relationships that expand beyond &#8220;normative&#8221; family patterns</li>
				</ul></li>
			<li class='fragment'>Empirical Evidence
			<ul>
				<li>Children are harmed by conflict-ridden marriage, not divorce</li>
			</ul></li>
		</ul>
	</section>
	<section>
		<h3>Policy Agenda</h3>
		<ul>
			<li class='fragment'>Legal, Economic, and Social Policy Reforms that promote cultural diversity</li>
			<li class='fragment'>Challenging insidious family ideology</li>
			<li class='fragment'>Critical of welfare policies (esp TANF); favor universal policies (Medicare, etc)</li>
		</ul>
		<p class='fragment'>Criticism: Devaluing &#8220;normative&#8221; families? Does conflict really prevent parents from effectively raising children?</p>
	</section>
	<section>
		<h3>Review</h3>
		<p>How would each of the three camps explain why some families are living in poverty?</p>
	</section>
	<section>
		<h3>Battle Averted?</h3>
		<img src='../images/battle_lines.jpg' height='500'/>
		<p style='text-align:center;'>Are these worldviews really mutually exclusive and irreconcilable?</p>
	</section>
</section>
<section>
	<section>
		<h2>Bridging Controversy</h2>
	</section>
	<section>
		<h3>Reframing Policy Issues</h3>
		<p>Recognizing the True Paradoxes of the 3 Camp Model:</p>
			<ul class='fragment'>
				<li>Individualism vs Familism</li>
				<li>Government as a Problem vs Government as a Solution</li>
				<li>Emphasizing Children vs Emphasizing Adults</li>
				<li>Rights vs Responsibilities</li>
				<li>Self-Sacrifice vs Self-Fulfillment</li>
				<li>Moral Absolutism vs Moral Relativism</li>
				<li>Family Change as Progress or Decline</li>
			</ul>
	</section>
	<section>
		<h3>Changing the Substance of Family Policy</h3>
		<p>Considering the three camps allows us to better characterize our understanding of policy problems. For example, regarding welfare reform:</p>
		<ul>
			<li class='fragment'>the concerned camp advocates for reducing the number of families on dependency caseloads</li>
			<li class='fragment'>the satisfied camp advocates for creating further supports for low-income adults</li>
			<li class='fragment'>the impatient camp advocates for further reducing child poverty</li>
			<li class='fragment'>the impatient camp also challenges perceptions about who receives welfare, emphasizing the diversity of families</li>
		</ul>
	</section>
	<section>
		<h3>Changing Policy Response</h3>
		<p>By considering the three camps, we can begin to determine how best to respond:</p>
		<ul>
			<li>Is this problem best solved through government action or cultural changes</li>
			<li>When disagreement exists, can the theory of paradox be used to resolve the tension and build better solutions?</li>
		</ul>
	</section>
	<section>
		<h3>Creating Politically Feasible Policy</h3>
		<p class='fragment'>By applying the theory of paradox to balance concerns from each camp, we can generate policy solutions that is more widely acceptable</p>
		<p class='fragment'>Policies formed through resolution of dialectical tension tend to be more effective than policies from a single extreme, or which seek an arbitrary middle ground</p>
	</section>
	<section>
		<h3>Mobilizing Action</h3>
		<p>&#8220;There are few, if any issues where all the truth and all the right and all the angels are on one side.&#8221; &ndash; John F. Kennedy</p>
		<p>&#8220;Those who are interested in social change must never allow themselves the privilege of being in the majority, else they run the risk of losing their grasp of the paradox... When most people agree with  you, worry.&#8221; - Julian Rappaport</p>
		<p class='fragment'>It's often hard to recognize the validity of alternative viewpoints, which makes applying the theory of paradox difficult.</p>
		<p class='fragment'>Recognizing true paradoxes can crate urgency and mobilize action, while simultaneously allowing all camps to productively participate in creating policy solutions.</p>
	</section>
	<section>
		<h3>Discussion</h3>
		<ol>
			<li>How would the three camps characterize recent controversial policy issues like abortion, same-sex marriage, and the ACA? <br/>hint: what problems are these policies trying to solve?</li>
			<li>How do these perspectives differ from the Liberal / Conservative way of thinking on these issues?</li>
			<li>What dialectics can we recognize in these positions? How might we apply the theory of paradox to resolve them?</li>
		</ol>
	</section>
</section>
<section>
	<h1>Next Up:</h1>
	<h2><a href="./?lesson=roots">The Roots of Family Policy</a></h2>
</section>

