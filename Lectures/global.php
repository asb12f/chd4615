<?php
# toggle whether page is live
$live = True;
require_once('redirect.php');

$title = 'Global Perspectives on Family Policy';
$id = 'night';
$theme = 'css/theme/'.$id.'.css';


## Other Available Themes
## $theme = 'http://pablocubi.co/mozreveal/css/theme/one-mozilla.css';
## Predefined Themes
## beige, blood, default, moon, night, serif, simple, sky, sky-jeopardy, solarized
## $id = 'beige';
## $theme = 'css/theme/'.$id.'.css';

require_once('header.php');
?>
<!-- each slide is a section; everything else is automated in the support PHP -->
<section>
	<h1>Agenda</h1>
	<ol>
		<li>Exploring International Family Policies</li>
	</ol>
</section>
<section>
	<section>
		<h2>For each video and policy area, consider:</h2>
		<ul>
			<li>How does this differ from US Policies?</li>
			<li>Is this policy driven by individualism or familism?</li>
			<li>What are the costs and benefits of this policy for individuals, families, and society at large?</li>
			<li>How do contextual factors like race, gender, religion, culture, or SES play a role?</li>
			<li>What might you change about this policy?</li>
		</ul>
	</section>
	<section>
		<h2>Maternity and Paternity Leave</h2>
		<iframe width="640" height="360" src="https://www.youtube.com/embed/7QlO7awQzDY" frameborder="0" allowfullscreen></iframe>
	</section>
	<section>
		<h2>Child Labor</h2>
		<iframe width="640" height="360" src="https://www.youtube.com/embed/9yZc2xw1xR4" frameborder="0" allowfullscreen></iframe>
	</section>
	<section>
		<h2>Family Planning</h2>
		<iframe width="640" height="360" src="https://www.youtube.com/embed/UjyM44vS0JI" frameborder="0" allowfullscreen></iframe>
	</section>
	<section>
		<h2>Recent Changes in the One Child Policy</h2>
		<audio controls preload=metadata>
			<source src="./audio/onechild.mp3"></source>
		</audio>
	</section>
	<section>
		<h2>Childcare Policy</h2>
		<iframe width="640" height="360" src="https://www.youtube.com/embed/3bdlnYlzrp0" frameborder="0" allowfullscreen></iframe>
	</section>
	<section>
		<h2>Polygamy</h2>
		<iframe width="640" height="360" src="https://www.youtube.com/embed/VLPz816ncd0" frameborder="0" allowfullscreen></iframe>
	</section>
	<section>
		<h2>Healthcare and Housing Access</h2>
		<iframe width="640" height="360" src="https://www.youtube.com/embed/l8iWZmwfYMg" frameborder="0" allowfullscreen></iframe>
	</section>
	<section>
		<h3>Reminder</h3>
		<p>Be sure to read the assigned chapters in the textbook! There is additional material on these and other policy issues that you are expected to be familiar with!</p>
	</section>
</section>
<section>
	<h1>Next Up:</h1>
	<h2><a href="./?lesson=US">US Perspectives on Family Policy</a></h2>
</section>
