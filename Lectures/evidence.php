<?php
# toggle whether page is live
$live = True;
require_once('redirect.php');

$title = 'Evidence-Based Family Policy';
$id = 'night';
$theme = 'css/theme/'.$id.'.css';


## Other Available Themes
## $theme = 'http://pablocubi.co/mozreveal/css/theme/one-mozilla.css';
## Predefined Themes
## beige, blood, default, moon, night, serif, simple, sky, sky-jeopardy, solarized
## $id = 'beige';
## $theme = 'css/theme/'.$id.'.css';

require_once('header.php');
?>
<!-- each slide is a section; everything else is automated in the support PHP -->
<section>
	<h1>Agenda</h1>
	<ol>
		<li>Intersections Between Evidence and Policy</li>
		<li>The Family Impact Seminar</li>
		<li>Strategies for Advancing Evidence-Based Family Policy</li>
	</ol>
</section>
<section>
	<section>
		<h2>Intersections Between Evidence and Policy</h2>
	</section>
	<section>
		<h3>Do We Need Policy Research?</h3>
		<ul>
			<li class='fragment'>Observation: Public policy isn't as effective as we'd like it to be</li>
			<li class='fragment'>Observation: Policymaking is often seen as an unintelligent process</li>
			<li class='fragment'>Question: Can data overcome polarization?</li>
			<li class='fragment'>Question: Should policy drive research, or research drive policy?</li>
		</ul>
	</section>
	<section>
		<h3>A Perspective on Policy and Evidence</h3>
		<iframe width="640" height="360" src="https://www.youtube.com/embed/u6mDhW0WvUE" frameborder="0" allowfullscreen></iframe>
	</section>
	<section>
		<h3>Discussion</h3>
		<ol>
			<li>Goldacre is critical of policymakers who misuse data, arguing misrepresentation is often intentional. What do you think about this claim? <span class='fragment'>Who determines what constitutes 'misuse'?</span></li>
			<li class='fragment'>Should policymakers have access to data about policies before it is released to the general public?</li>
			<li class='fragment'>Can independent statistical units really exist in the realm of policy analysis? How would this differ from institutions like the CDC and Children's Bureau, or research groups like Pew?</li>
			<li class='fragment'>How realistic are randomized trials of policies? What are the practical and ethical challenges?</li>
		</ol>
	</section>
	<section>
		<h3>Why Don't Policymakers Use Research?</h3>
		<p>Four Theories:</p>
		<ol>
			<li class='fragment'>Policy is Fast, Science is Slow </li><br />
			<li class='fragment'>Science is Circumstantial, Incomplete, and Uncommunicative</li><br />
			<li class='fragment'>Democratic Free-Market Systems vs Centralized Coporatist Systems (Germany, Japan, etc)</li><br />
			<li class='fragment'>Lack of Communication and Trust between Knowledge Producers and Knowledge Consumers (&#8220;Two Communities&#8221;)</li><br />
		</ol>
		<p class='fragment'>Which theory is most useful?</p>
	</section>
	<section>
		<h3>The Community Dissonance Theory</h3>
		<p>The breakdown between policymakers, policy administrators, and researchers is due to differences socializatoin and behavior, not structural divides.</p>
		<p>Understanding the differences between these needs can help us change behaviors and improve the link between evidence and policy.</p>
	</section>
	<section>
		<table class='reveal'>
		<h3>Research Needs</h3>
		<tr>
			<th>Characteristics</th><th>Researchers</th><th>Policymakers</th><th>Administrators</th>
		</tr>
		<tr>
			<td>Content</td><td class='fragment'>What we Don't Know</td><td class='fragment'>What we Do Know</td><td class='fragment'>What we Need to Know</td>
		</tr>
		<tr>
			<td>Detail</td><td class='fragment'>Narrow, Focused</td><td class='fragment'>Comprehensive Overview</td><td class='fragment'>Pragmatism</td>
		</tr>
		<tr>
			<td>Data Source</td><td class='fragment'>Representative Samples</td><td class='fragment'>Constituency</td><td class='fragment'>Population Served</td>
		</tr>
		</table>
	</section>
	<section>
		<table class='reveal'>
		<h3>Work Culture Needs</h3>
		<tr>
			<th>Characteristics</th><th>Researchers</th><th>Policymakers</th><th>Administrators</th>
		</tr>
		<tr>
			<td>Timing</td><td class='fragment'>Cautions, Skeptical, Tentative</td><td class='fragment'>Reactive, Fluid</td><td class='fragment'>Practical, Action-Oriented</td>
		</tr>
		<tr>
			<td>Decisionmaking Criteria</td><td class='fragment'>Rigor, Statistics, Peer-Review</td><td class='fragment'>Negotiation and Compromise, Rhetoric, Persuasion, Anecdote</td><td class='fragment'>Research, Practical Experience</td>
		</tr>
		<tr>
			<td>View of Ambiguity &amp; Complexity</td><td class='fragment'>Exciting</td><td class='fragment'>Counter-Productive</td><td class='fragment'>Needs Simplification for Application</td>
		</tr>
		</table>
	</section>
	<section>
		<table class='reveal'>
		<h3>Information Communication Needs</h3>
		<tr>
			<th>Characteristics</th><th>Researchers</th><th>Policymakers</th><th>Administrators</th>
		</tr>
		<tr>
			<td>Emphasis</td><td class='fragment'>Methodology and Analysis</td><td class='fragment'>Potential Impact</td><td class='fragment'>Transfer to Target Population</td>
		</tr>
		<tr>
			<td>Organization of Reports</td><td class='fragment'>Logical Progression</td><td class='fragment'>Focus on Conclusions</td><td class='fragment'>Focus on Conclusions for Practice</td>
		</tr>
		<tr>
			<td>Writing Style</td><td class='fragment'>In-Depth, Technical Language, Complex Illustrations</td><td class='fragment'>Concise, Easy to Read, Simple Illustrations</td><td class='fragment'>Moderate Length, Practical Technical Language, Cleary Expectations for Accountability and Practicality</td>
		</tr>
		</table>
	</section>
</section>
<section>
	<section>
		<h2><a href='https://www.purdue.edu/hhs/hdfs/fii/'>The Family Impact Seminar</a></h2>
	</section>
	<section>
		<h3>What is a Family Impact Seminar?</h3>
		<p class='fragment'>A technique for disseminating high quality research evidence to legislators</p>
		<p class='fragment'>A strategy for facilitating meaningful communication between researchers and legislators</p>
		<p class='fragment'>A forum for nonpartisan exploration of challenging family issues</p>
		<p class='fragment'>A tool for helping legislators better evaluate how policies impact the families they serve</p>
		<p class='fragment'>A TED talk for legislators, but with less fluff and more content</p>
	</section>
	<section>
		<h3>The FIS Methodology</h3> 
		<ul>
			<li>Provided to Legislators and their Staff (typically between 20 and 160 total attendees)</li>
			<li>Non-partisian presentations, reports, and discussion groups</li>
			<li>Emphasis on high-quality, objective research <span class='fragment'>(no qual?)</span></li>
			<li>No lobbyists, no press</li>
		</ul>
	</section>
	<section>
		<h3>FIS Methodology</h3>
		<ul>
			<li>Presenters are Recognized Experts</li>
			<li>Topic is Timely and Chosen by Legislators</li>
			<li>2-Hour Forum</li>
			<li>30 minute Q&amp;A for Legislators</li>
			<li>Unstructured discussion time and private meetings afterwards</li>
		</ul>
	</section>	<section>
		<h3>FIS Methodology</h3>
		<ul>
			<li>Legislators receive briefing reports</li>
			<li>May also provide audio CDs, videos of presentations, and other helpful materials</li>
		</ul>
		<p class='fragment'>Currently implemented in 21 states, but not Florida. </p>
		<p class='fragment'>Could FSU host FIS? <span class='fragment' style='color:limegreen;'>Yes, we used to.</span></p>
	</section>
	<section>
		<h3>Components of the FIS</h3>
		<ul>
			<li class='fragment'>Easy Access to Timely Research on Issues (bipartisan) Legislators Identify</li>
			<li class='fragment'>Varied Delivery Formats</li>
			<li class='fragment'>Use of the Family Impact Lens</li>
			<li class='fragment'>Objective, Nonpartisan Approach</li>
			<li class='fragment'>Opportunities for Discussion in a Neutral, Nonpartisan, Off-the-Record Setting</li>
		</ul>
	</section>
	<section>
		<h3>FIS in Action</h3>
		<iframe width="320" height="180" src="https://www.youtube.com/embed/0BiTkBWe0EM" frameborder="0" allowfullscreen></iframe>
		<iframe width="320" height="180" src="https://www.youtube.com/embed/_jZbL8669uk" frameborder="0" allowfullscreen></iframe>
	</section>
	<section>
		<h3>Video Discussion</h3>
		<p>How do the speakers recognize the challenges that face policymakers?</p>
		<p>How do the speakers attempt to move past political divides?</p>
		<p>How do the speakers orient the policymakers to the current status of policy?</p>
		<p>How do the speakers help ensure that the relevant points are communicated to policy makers?</p>
	</section>
	<section>
		<h3>Evidence Supporting the FIS</h3>
		<p class='fragment'>Legislators and their staff are willing to set aside time to attend and participate, because they value the information they receive</p>
		<p class='fragment'>Legislators report valuing the balanced information; pre-post studies show legislators learn from attending</p>
		<div class='fragment'>
		<p>Legislators report being more aware of and concerned about effects on families afterwards (Family Impact Lens; Racial Disparities)</p>
		<p>&#8220;I wasn't sure that the government had a role in parenting, but now I am sure we can no longer stick our head in the sand.&#8221;</p>
		</div>
	</section>
	<section>
		<h3>Evidence Supporting the FIS</h3>
		<p class='fragment'>Legislators value the opportunity to work on policy and network beyond party lines</p>
		<p class='fragment'>Legislators report using information gained at the seminars when making policy decisions 2 months later; in some cases newly drafted legislation has been directly attributed to attendance</p>
	</section>
	<section>
		<h3>Policies Attributed to FIS</h3>
		<ul>
			<li>Oregon's Refundable Childcare Tax Credit</li>
			<li>Wisconsin's Senior Care Prescription Drug Program</li>
			<li>Nebraska's State Children's Health Insurance Program</li>
			<li>Michigan's Juvenile Justice Reform</li>
			<li>Illinois' After School Program Funding and Curriculum</li>
		</ul>
	</section>
</section>
<section>
	<section>
		<h2>Strategies for Advancing Evidence-Based Family Policy</h2>
	</section>
	<section>
		<h3>4 Major Strategies</h3>
		<ul>
			<li>Conceptualize Policy Work as Developing Relationships, not Disseminating Information</li>
			<li>Communicate Research in Ways that Meet Policymaker's Needs</li>
			<li>Provide Timely Responses to Questions Driving Policy Debate</li>
			<li>Take Initiative in Contacting Policy Makers</li>
		</ul>
	</section>
	<section>
		<h3>Conceptualize Policy Work as Developing Relationships, not Disseminating Information</h3>
		<p class='fragment'>Better relationships make information flow easier and more effective</p>
		<p class='fragment'>Having a relationship makes you a more trusted source</p>
		<p class='fragment'>2-Way communication is most effective</p>
		<p class='fragment'>&#8220;Hanging out with policymakers helps you realize how smart they are. Everybody likes to bash them, and how the media covers them sets that dynamic up. But if you talk to them about their concerns, your view changes.&#8221;</p>
	</section>
	<section>
		<h3>Communicate Research in Ways that Meet Policymaker's Needs</h3>
		<p class='fragment'>Policymakers rely more on spoken than written word</p>
		<p class='fragment'>Policymakers retain more when they can engage in discussion</p>
		<p class='fragment'>Remember, legislators are &#8220;people-people&#8221;</p>
		<p class='fragment'>Packaging information is just as important as the information itself; avoid packaging that &#8220;delightful to academics, but dreadful for policymakers&#8221;</p>
	</section>
	<section>
		<h3>Use Clear, Careful Language when Dealing with Myths about Vulnerable Populations</h3>
		<p class='fragment'>Myths are often treated as fact in policy debate, be aware of stereotypes about vulnerable populations</p>
		<p class='fragment'>Being &#8220;willing to talk in a straightforward way about very complicated issues … that you usually wouldn’t have to explain to other researchers.&#8221; </p>
		<p class='fragment'></p>
		<p class='fragment'></p>
	</section>
	<section>
		<h3>Provide Timely Responses to Questions Driving Policy Debate</h3>
		<p><a href='https://books.google.com/books?hl=en&lr=&id=CxN1AwAAQBAJ&oi=fnd&pg=PA40&dq=kingdon+policy+window&ots=ySGo2rJn8c&sig=3VQcGxxi6DqvSrNyIHvg4KPsCHs#v=onepage&q=kingdon%20policy%20window&f=false'>Kingdon's Theory of Open Policy Windows</a>
		<p class='fragment'>Three &#8220;streams&#8221; must be aligned for a matter to be dealt with in the public policy arena:</p>
		<ol class='fragment'>
			<li>The problem stream (is the condition considered a problem?)</li>
			<li>The policy stream (are there are policy alternatives that can be implemented?)</li>
			<li>The political stream (are politicians willing and able to make a policy change?)</li>
		</ol>
		<p class='fragment'>When these three streams come together, a window of opportunity is open and action can be taken on the subject at hand.</p>
		<p class='fragment'>FIS methodology actively targets open policy windows; this is only achieved by staying current with politics</p>
	</section>
	<section>
		<h3>Take Initiative in Contacting Policy Makers</h3>
		<p class='fragment'>Policymakers are pressed for time, and rarely know what information resources exist</p>
		<p class='fragment'>Most policymakers are open to contact from experts;</p>
		<p class='fragment'>Do your homework - who is asking for information?; what do they need to know?; what do they already know?; and for what purpose do they want to know it?</p>
		<p class='fragment'>Find the &#8220;workhorses&#8221; and engage them</p>
	</section>
	<section>
		<h3>Recommended Reading</h3>
		<p><a href='http://onlinelibrary.wiley.com/doi/10.1111/j.1741-3729.2008.00549.x/full'>Friese, B., and Bogenschneider, K. (2009). The voice of experience: How social scientists communicate family research to policymakers. <i>Family Relations</i>, <i>58</i>, 229-243.</a></p>
	</section>
</section>
<section>
	<h1>Next Up:</h1>
	<h2><a href="./?lesson=impact">Family Impact Analysis</a></h2>
</section>

