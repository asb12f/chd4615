		</div>
	</div>
	<script src="lib/js/head.min.js"></script>
	<script src="js/reveal.min.js"></script>
 
	<script>
		Reveal.initialize({
		transition: 'default',
		history: true,
		dependencies:[
		 { src: 'plugin/notes/notes.js', async: true },
		 { src: 'plugin/math/math.js', async: true },
		 { src: 'plugin/highlight/highlight.js', async: true, callback: function() { hljs.initHighlightingOnLoad(); } },
		]
		});
	</script>
</body>
</html>