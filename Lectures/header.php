<!doctype html>
<html lang="en">	
<head>
	<meta charset="utf-8">
	<link rel="stylesheet" href="css/reveal.min.css">
	<link rel="stylesheet" href="<?php echo $theme ?>" id="theme">	
	<title><?php echo $title ?></title>
	<?php
	$format = filter_input(INPUT_GET, 'format', FILTER_SANITIZE_SPECIAL_CHARS);
	if ($format == 'Print') {
		echo("<link rel='stylesheet' type='text/css' href='css/print/print.css'>");
	}	
	?>
	<!--Add support for earlier versions of Internet Explorer -->
	<!--[if lt IE 9]>
	<script src="lib/js/html5shiv.js"></script>
	<![endif]-->

	<!-- Google Tag Manager -->
		<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-KL4WL7"
		height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
		<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
		new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
		j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
		'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
		})(window,document,'script','dataLayer','GTM-KL4WL7');</script>
	<!-- End Google Tag Manager -->
<style type="text/css">
p { text-align: left; }

p .left {
	text-align: left;
}

.hangingindent {
  padding-left: 50px ;
  text-indent: -50px ;
	}

h4 + ul {
	text-align: left;
	float: left;
	clear: both;
	}
	
.reveal h4 {
	clear: left;
	line-height: 1.4em;
}

.reveal td {
   border: 1px solid #eee;
}

.designTable td {
	border: none;
}

</style>

<script type="text/x-mathjax-config">
  MathJax.Hub.Config({tex2jax: {inlineMath: [['$','$'], ['\\(','\\)']]}});
</script>

</head>
 
<body>
	<div class="reveal">
		<div class="slides">
			<section id="front">
				<section>
				<h1><?php echo $title ?></h1>
				<h3>Family and Child Public Policy</h3>	
				<h6>
				<?php
				$lecture = filter_input(INPUT_GET, 'lesson', FILTER_SANITIZE_SPECIAL_CHARS);
				$role = filter_input(INPUT_GET, 'role', FILTER_SANITIZE_SPECIAL_CHARS);
				echo('<a href="./?lesson='.$lecture.'&role='.$role.'&format=Print#/">[Print]</a>')
				?>
				</h6>
				</section>	
			</section>