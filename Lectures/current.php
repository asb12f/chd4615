<?php

# toggle whether page is live
$live = True;
require_once('redirect.php');

$title = 'Current Family Policies';
$id = 'night';
$theme = 'css/theme/'.$id.'.css';


## Other Available Themes
## $theme = 'http://pablocubi.co/mozreveal/css/theme/one-mozilla.css';
## Predefined Themes
## beige, blood, default, moon, night, serif, simple, sky, sky-jeopardy, solarized
## $id = 'beige';
## $theme = 'css/theme/'.$id.'.css';

require_once('header.php');
?>
<!-- each slide is a section; everything else is automated in the support PHP -->
<section>
	<h1>Agenda</h1>
	<ol>
		<li>FL Abortion Policy</li>
		<li>ICWA</li>
	</ol>
</section>
<section>
	<h3>Disclaimer</h3>
	<p>As a reminder, the purpose of this discussion is not to endorse or challenge any policy or bill, but rather to practice understanding how policies are written and analyzing them using the family impact lens and the three worldviews.</p>
</section>
<section>
	<section>
		<h2>FL Abortion Law</h2>
	</section>
	<section>
	<h3>Recent History</h3>
	<iframe width="320" height="180" src="https://www.youtube.com/embed/NKhLKMN1Or4" frameborder="0" allowfullscreen></iframe>
	<iframe width="320" height="180" src="https://www.youtube.com/embed/hFOBVIH9zyc" frameborder="0" allowfullscreen></iframe>
	<iframe width="320" height="180" src="https://www.youtube.com/embed/LZ-ErgPsCzk" frameborder="0" allowfullscreen></iframe>
	<iframe width="320" height="180" src="https://www.youtube.com/embed/zOSDhNjxuqs" frameborder="0" allowfullscreen></iframe>
	</section>
	<section>
		<h3>Current Statutes</h3>
		<p><a href='http://www.leg.state.fl.us/statutes/index.cfm?App_mode=Display_Statute&URL=0300-0399/0390/0390.html'>Florida Statutes Title XXIX, Chapter 390, Termination of Pregnancies</a></p>
		<p>Note: This is only the Public Health provisions; there are additional sections examining issues relating to abortion elsewhere that are beyond the scope of today's discussion. If you're interested in these:</p>
		<ul>
			<li><a href='http://www.leg.state.fl.us/statutes/index.cfm?App_mode=Display_Statute&URL=0000-0099/0039/0039ContentsIndex.html'>Chapter 39 - Proceedings Relating to Children (Child Abuse Laws)</a></li>
			<li><a href='http://www.leg.state.fl.us/statutes/index.cfm?App_mode=Display_Statute&URL=0600-0699/0627/0627ContentsIndex.html'>Chapter 627 - Insurance Rates and Contracts</a></li>
			<li><a href='http://www.leg.state.fl.us/statutes/index.cfm?App_mode=Display_Statute&URL=0600-0699/0641/0641ContentsIndex.html'>Chapter 641 - Health Care Service Programs</a></li>
			<li><a href='http://www.leg.state.fl.us/statutes/index.cfm?App_mode=Display_Statute&URL=0700-0799/0765/0765ContentsIndex.html'>Chapter 665 - Health Care Advanced Directives</a></li>
			<li><a href='http://www.leg.state.fl.us/statutes/index.cfm?App_mode=Display_Statute&URL=0700-0799/0797/0797ContentsIndex.html'>Chapter 797 - Illegal Abortion</a></li>
			<li><a href='http://www.leg.state.fl.us/statutes/index.cfm?App_mode=Display_Statute&URL=0700-0799/0782/0782ContentsIndex.html'>Chapter 782 - Homicide</a></li>
			<li><a href='http://www.leg.state.fl.us/statutes/index.cfm?App_mode=Display_Statute&URL=0900-0999/0984/0984ContentsIndex.html'>Chapter 984 - Children and Families in Need of Services (DJJ)</a></li>
		</ul>
	</section>
	<section>
		<h3>Key Definitions</h3>
		<p>(1) &#8220;Abortion&#8221; <span class='fragment'>means the termination of human pregnancy with an intention <span style='color: gold;'>other than to produce a live birth or to remove a dead fetus.</span></span></p>
		<p>(4) &#8220;Born alive&#8221; <span class='fragment'>means the complete expulsion or extraction from the mother of a human infant, at any <span style='color: gold;'>stage of development</span>, who, after such expulsion or extraction, <span style='color: gold;'>breathes or has a beating heart, or definite and voluntary movement of muscles</span>, regardless of whether the umbilical cord has been cut and regardless of whether the expulsion or extraction occurs as a result of natural or induced labor, caesarean section, induced abortion, or other method.</span></p>
	</section>
	<section>
		<h3>Key Definitions</h3>	
		<p>(7) &#8220;Partial-birth abortion&#8221; <span class='fragment'>means a termination of pregnancy in which the physician performing the termination of pregnancy <span style='color: gold;'>partially vaginally delivers a living fetus before killing the fetus </span>and completing the delivery.</span></p>
		<p>(11) &#8220;Third trimester&#8221; <span class='fragment'>means the weeks of pregnancy <span style='color: gold;'>after the 24th week of pregnancy</span>.</span></p>
		<p>(12) &#8220;Viable&#8221; or &#8220;Viability&#8221;<span class='fragment'>means the stage of fetal development when the <span style='color: gold;'>life of a fetus is sustainable outside the womb</span> through <span style='color: gold;'>standard medical measures</span>.</span></p>
	</section>
	<section>
		<h3>General Provisions</h3>
		<p>TERMINATION IN THIRD TRIMESTER; WHEN ALLOWED.—No termination of pregnancy shall be performed on any human being in the third trimester of pregnancy unless one of the following conditions is met:</p>
		<ol class='fragment'>
			<li>Two physicians certify in writing that, in reasonable medical judgment, the termination of the pregnancy is necessary <span style='color: gold;'>to save the pregnant woman’s life</span> or <span style='color: gold;'>avert a serious risk of substantial and irreversible physical impairment of a major bodily function of the pregnant woman</span> other than a psychological condition.</li>
			<li>The physician certifies in writing that, in reasonable medical judgment, there is a medical necessity for legitimate emergency medical procedures for termination of the pregnancy to save the pregnant woman’s life or avert a serious risk of imminent substantial and irreversible physical impairment of a major bodily function of the pregnant woman other than a psychological condition, and another physician is not available for consultation.</li>
		</ol>
	</section>
	<section>
		<h3>Consent</h3>
		<p>CONSENTS REQUIRED.—A termination of pregnancy may not be performed or induced except with the voluntary and informed written consent of the pregnant woman or, in the case of a mental incompetent, the voluntary and informed written consent of her court-appointed guardian.</p>
		<p>Except in the case of a medical emergency, consent to a termination of pregnancy is voluntary and informed only if:</p>
		<p>(1) The physician who is to perform the procedure, or the referring physician, has, at a minimum, orally, while physically present in the same room, and at least 24 hours before the procedure, informed the woman of:
		<p class='fragment' style='padding-left:1em;'>(a) The nature and risks of undergoing or not undergoing the proposed procedure that a reasonable patient would consider material to making a knowing and willful decision of whether to terminate a pregnancy</p>
	</section>
	<section>
		<h3>Consent (continued)</h3>
		<p class='fragment' style='padding-left:1em;'>(b) The probable gestational age of the fetus, verified by an ultrasound, at the time the termination of pregnancy is to be performed.</p>
		<p class='fragment' style='padding-left:2em;'> (I) The person performing the ultrasound <span style='color: gold;'>must offer the woman the opportunity to view the live ultrasound images and hear an explanation of them</span>. If the woman accepts the opportunity to view the images and hear the explanation, a physician or a registered nurse, licensed practical nurse, advanced registered nurse practitioner, or physician assistant working in conjunction with the physician must contemporaneously review and explain the images to the woman before the woman gives informed consent to having an abortion procedure performed.</p>
	</section>
	<section>
		<h3>Consent (continued)</h3>
		<p style='padding-left:2em;'>(II) <span style='color: gold;'>The woman has a right to decline to view and hear the explanation of the live ultrasound images</span> after she is informed of her right and offered an opportunity to view the images and hear the explanation. If the woman declines, the woman shall complete a form acknowledging that she was offered an opportunity to view and hear the explanation of the images but that she declined that opportunity. The form must also indicate that the woman’s decision was not based on any undue influence from any person to discourage her from viewing the images or hearing the explanation and that she declined of her own free will.</p>
	</section>
	<section>
		<h3>Consent (continued)</h3>
		<p style='padding-left:2em;'>(III) Unless requested by the woman, <span style='color: gold;'>the person performing the ultrasound may not offer the opportunity to view the images and hear the explanation and the explanation may not be given if, at the time the woman schedules or arrives for her appointment to obtain an abortion, a copy of a restraining order, police report, medical record, or other court order</span> or documentation is presented which provides evidence that the woman is obtaining the abortion because the <span style='color: gold;'>woman is a victim of rape, incest, domestic violence, or human trafficking</span> or that the woman has been diagnosed as having a condition that, on the basis of a physician’s good faith clinical judgment, would create a serious risk of substantial and irreversible impairment of a major bodily function if the woman delayed terminating her pregnancy.</p>
		<p class='fragment style='padding-left:1em;''>(c) The medical risks to the woman and fetus of carrying the pregnancy to term.</p>	
		</section>
	<section>
		<h3>Consent (continued)</h3>
		<p>(2) Printed materials prepared and provided by the department have been provided to the pregnant woman, if she chooses to view these materials, including:</p>
		<p style='padding-left:1em;'>(a) A description of the fetus, including a description of the various stages of development.</p>
		<p style='padding-left:1em;'>(b) A list of entities that offer alternatives to terminating the pregnancy.</p>
		<p style='padding-left:1em;'>(c) Detailed information on the availability of medical assistance benefits for prenatal care, childbirth, and neonatal care.</p>
	</section>
	<section>
		<h3>(4)Third Trimester Abortions</h3>
		<p>If a termination of pregnancy is performed in the third trimester, the physician performing the termination of pregnancy must <span style='color: gold;'>exercise the same degree of professional skill, care, and diligence to preserve the life and health of the fetus which the physician would be required to exercise in order to preserve the life and health of a fetus intended to be born</span> and not aborted. However, if preserving the life and health of the fetus conflicts with preserving the life and health of the pregnant woman, the <span style='color: gold;'>physician must consider preserving the woman’s life and health the overriding and superior concern</span>.</p>
	</section>
	<section>
		<h3>(5)Partial Birth Abortions</h3>
		<p class='fragment'>(a) No physician shall knowingly perform a partial-birth abortion.</p>
		<p class='fragment'>(b) A woman upon whom a partial-birth abortion is performed may not be prosecuted under this section for a conspiracy to violate the provisions of this section.</p>
		<p class='fragment'>(c) This subsection shall not apply to a partial-birth abortion that is necessary to save the life of a mother whose life is endangered by a physical disorder, illness, or injury, provided that no other medical procedure would suffice for that purpose.</p>
	</section>
	<section>
		<h3>Additional Statues</h3>
		<p>(6) EXPERIMENTATION ON FETUS PROHIBITED; EXCEPTION. <span class='fragment'>—No person shall use any live fetus or live, premature infant for any type of scientific, research, laboratory, or other kind of experimentation either prior to or subsequent to any termination of pregnancy procedure except as necessary to protect or preserve the life and health of such fetus or premature infant.</span></p>
		<p>(7) FETAL REMAINS. <span class='fragment'>—Fetal remains shall be disposed of in a sanitary and appropriate manner and in accordance with standard health practices, as provided by rule of the Department of Health. Failure to dispose of fetal remains in accordance with department rules is a misdemeanor of the second degree, punishable as provided in s. 775.082 or s. 775.083.</span></p>
	</section>
	<section>
		<h3>More Additional Statutes</h3>
		<p>(8) REFUSAL TO PARTICIPATE IN TERMINATION PROCEDURE.<span class='fragment'>—<span style='color: gold;'>Nothing in this section shall require any hospital or any person to participate in the termination of a pregnancy, nor shall any hospital or any person be liable for such refusal</span>. No person who is a member of, or associated with, the staff of a hospital, nor any employee of a hospital or physician in which or by whom the termination of a pregnancy has been authorized or performed, who shall state an objection to such procedure on moral or religious grounds shall be required to participate in the procedure which will result in the termination of pregnancy. The <span style='color: gold;'>refusal of any such person or employee to participate shall not form the basis for any disciplinary or other recriminatory action</span> against such person.</span></p>
	</section>
	<section>
		<h3>More Additional Statutes</h3>
		<p>(12) INFANTS BORN ALIVE.—</p>
		<p class='fragment' style='padding-left:1em;'>(a) An infant born alive during or immediately after an attempted abortion is <span style='color: gold;'>entitled to the same rights, powers, and privileges as are granted by the laws of this state to any other child born alive in the course of natural birth</span>.</p>
		<p class='fragment' style='padding-left:1em;'>(b) If an infant is born alive during or immediately after an attempted abortion, any health care practitioner present at the time shall humanely exercise the same degree of professional skill, care, and diligence to preserve the life and health of the infant as a reasonably diligent and conscientious health care practitioner would render to an infant born alive at the same gestational age in the course of natural birth.</p>
		<p class='fragment' style='padding-left:1em;'>(c) An infant born alive during or immediately after an attempted abortion must be <span style='color: gold;'>immediately transported and admitted to a hospital</span> pursuant to s. 390.012(3)(c) or rules adopted thereunder.</p>
	</section>
	<section>
		<h3>More Additional Statutes</h3>
		<p>Termination of pregnancies during viability.— <span class='fragment'>(1)No termination of pregnancy shall be performed on any human being if the physician determines that, in reasonable medical judgment, the fetus has achieved viability, unless:</span></p>
		<p class='fragment' style='padding-left:1em;'>(a) <span style='color: gold;'>Two physicians</span> certify in writing that, in reasonable medical judgment, the termination of the pregnancy is necessary to save the pregnant woman’s life or avert a serious risk of substantial and irreversible physical impairment of a major bodily function of the pregnant woman other than a psychological condition; or</p>
		<p class='fragment' style='padding-left:1em;'>(b) The physician certifies in writing that, in reasonable medical judgment, there is a medical necessity for <span style='color: gold;'>legitimate emergency medical procedures</span> [shortened version]. </p>
		<p class='fragment'>(2) Before performing a termination of pregnancy, <span style='color: gold;'>a physician must determine if the fetus is viable</span> by, at a minimum, performing a medical examination of the pregnant woman and, to the maximum extent possible through reasonably available tests and the ultrasound required under s. 390.0111(3), an examination of the fetus.</p>
	</section>
	<section>
		<h3>Parental Notice of Abortion Act (390.01114)</h3>
		<p>(2) Definitions</p>
		<p class='fragment' style='padding-left:1em;'>(a) &#8220;Actual notice&#8221; means notice that is <span style='color: gold;'>given directly, in person or by telephone, to a parent or legal guardian of a minor</span>, by a physician, at least <span style='color: gold;'>48 hours</span> before the inducement or performance of a termination of pregnancy, and documented in the minor’s files.</p>
		<p class='fragment' style='padding-left:1em;'>(b) &#8220;Child abuse&#8221; means abandonment, abuse, harm, mental injury, neglect, physical injury, or sexual abuse of a child as those terms are defined in ss. 39.01, 827.04, and 984.03.</p>
		<p class='fragment' style='padding-left:1em;'>(b) &#8220;Constructive notice&#8221; means notice that is <span style='color: gold;'>given in writing, signed by the physician, and mailed at least 72 hours before</span> the inducement or performance of the termination of pregnancy, to the last known address of the parent or legal guardian of the minor, by first-class mail and by certified mail, return receipt requested, and delivery restricted to the parent or legal guardian. After the 72 hours have passed, delivery is deemed to have occurred.</p>
	</section>
	<section>
		<h3>Parental Notice of Abortion Act (390.01114)</h3>
		<p>(3) Notification Required</p>
		<p class='fragment' style='padding-left:1em;'>(a) Actual notice <span style='color: gold;'>shall be provided by the physician before the performance or inducement of the termination of the pregnancy of a minor</span>. If actual notice is not possible after a reasonable effort has been made, the physician performing or inducing the termination of pregnancy or the referring physician must give constructive notice. If actual notice is provided by telephone, the physician must actually speak with the parent or guardian, and must record in the minor’s medical file the name of the parent or guardian provided notice, the phone number dialed, and the date and time of the call. If constructive notice is given, the physician must document that notice by placing copies of any document related to the constructive notice, including, but not limited to, a copy of the letter and the return receipt, in the minor’s medical file. Actual notice given by telephone shall be confirmed in writing, signed by the physician, and mailed to the last known address of the parent or legal guardian of the minor, by first-class mail and by certified mail, return receipt requested, with delivery restricted to the parent or legal guardian. [shortened version]</p>
	</section>	
	<section>
		<h3>Parental Notice of Abortion Act (390.01114)</h3>
		<p class='fragment' style='padding-left:1em;'>(b) Notice not required if:</p>
		<p class='fragment' style='padding-left:2em;'>(1) In the physician’s good faith clinical judgment, <span style='color: gold;'>a medical emergency exists</span> and there is insufficient time for the attending physician to comply with the notification requirements. If a medical emergency exists, the physician shall make reasonable attempts, whenever possible, without endangering the minor, to contact the parent or legal guardian, and may proceed, but must document reasons for the medical necessity in the patient’s medical records. If the parent or legal guardian has not been notified within 24 hours after the termination of the pregnancy, the physician shall provide notice in writing, including details of the medical emergency and any additional risks to the minor, signed by the physician, to the last known address of the parent or legal guardian of the minor, by first-class mail and by certified mail, return receipt requested, with delivery restricted to the parent or legal guardian [shortened]</p>
	</section>
	<section>
		<h3>Parental Notice of Abortion Act (390.01114)</h3>
		<p style='padding-left:1em;'>(b) Notice not required if:</p>
		<p class='fragment' style='padding-left:2em;'>(2) Notice is waived in writing by the person who is entitled to notice and such waiver is notarized, dated not more than 30 days before the termination of pregnancy, and <span style='color: gold;'>contains a specific waiver of the right of the parent or legal guardian to notice of the minor’s termination of pregnancy</span></p>
		<p class='fragment' style='padding-left:2em;'>(3) Notice is waived by the <span style='color: gold;'>minor who is or has been married</span> or has had the disability of nonage removed under s. 743.015 or a similar statute of another state;</p>
		<p class='fragment' style='padding-left:2em;'>(4) Notice is waived by the patient because the <span style='color: gold;'>patient has a minor child dependent on her</span> </p>
	</section>	
	<section>
		<h3>PaAbortion referral or counseling agencies; penalties. (390.025)</h3>
		<p>(1) As used in this section, an &#8220;abortion referral or counseling agency&#8221; is any person, group, or organization, whether funded publicly or privately, <span style='color: gold;'>that provides advice or help to persons in obtaining abortions</span>.</p>
		<p>(2) An abortion referral or counseling agency, before making a referral or aiding a person in obtaining an abortion, <span style='color: gold;'>shall furnish such person with a full and detailed explanation of abortion, including the effects of and alternatives to abortion</span>. If the person advised is a minor, a good faith effort shall be made by the referral or counseling agency <span style='color: gold;'>to furnish such information to the parents or guardian of the minor</span>. No abortion referral or counseling agency shall <span style='color: gold;'>charge or accept any fee, kickback, or compensation of any nature</span> from a physician, hospital, clinic, or other medical facility for referring a person thereto for an abortion.</p>
	</section>
	<section>
		<h3>Applying the Three Worldviews (Discussion)</h3>
		<p>What would the concerned, impatient, and satisfied worldviews like about these policies? What would they dislike?</p>
	</section>
	<section>
		<h3>Applying the Family Impact Principles (Discussion)</h3>
		<ol>
			<li class='fragment'>Family Responsibility</li>
			<li class='fragment'>Family Relationships</li>
			<li class='fragment'>Family Stability</li>
			<li class='fragment'>Family Engagement</li>
			<li class='fragment'>Family Diversity</li>
		</ol>
	</section>
</section>
<section>
	<section>
		<h2>The Indian Child Welfare Act</h2>
	</section>
	<section>
		<h3>In the News (March 2016)</h3>
		<iframe width="640" height="360" src="https://www.youtube.com/embed/PW1Ow8620Pc" frameborder="0" allowfullscreen></iframe>
	</section>
	<section>
		<h3>What is the ICWA?</h3>
		<p>25 U.S.C. &sect; 1901 et seq.</p>
		<p>Passed in 1978</p>
		<iframe width="640" height="360" src="https://www.youtube.com/embed/VJCqeauLvY8" frameborder="0" allowfullscreen></iframe>
	</section>
	<section>
		<h3>An Informed Discussion about the "Lexi" Case</h3>
		<iframe width="640" height="360" src="https://www.youtube.com/embed/fdA0Z_JRFUs" frameborder="0" allowfullscreen></iframe>
	</section>
	<section>
		<h3>Key Sections</h3>
		<h4>25 U.S.C. &sect; 1903</h4>
		<p>(4) &#8220;Indian child&#8221; means any unmarried person who is under age eighteen and is either (a) <span style='color: gold;'>a member of an Indian tribe</span> or (b) <span style='color: gold;'>is eligible for membership in an Indian tribe and is the biological child of a member of an Indian tribe</span>;</p>
		<p>(5) &#8220;Indian child’s tribe&#8221; means (a) <span style='color: gold;'>the Indian tribe in which an Indian child is a member or eligible for membership</span> or (b), in the case of an Indian child who is a member of or eligible for membership in more than one tribe, the Indian tribe with which the Indian child has the more significant contacts;</p>
		<p>(12) &#8220;tribal court&#8221; means a court with jurisdiction over child custody proceedings and which is either <span style='color: gold;'>a Court of Indian Offenses</span>, a court established and operated under the code or custom of an Indian tribe, or any other <span style='color: gold;'>administrative body of a tribe which is vested with authority over child custody proceedings</span>.</p>
	</section>
	<section>
		<h3>Key Sections</h3>
		<h4>25 U.S.C. &sect; 1911</h4>
		<p>(a) Exclusive jurisdiction - <span style='color: gold;'>An Indian tribe shall have jurisdiction exclusive as to any State over any child custody proceeding involving an Indian child who resides or is domiciled within the reservation of such tribe</span>, except where such jurisdiction is otherwise vested in the State by existing Federal law. Where an Indian child is a ward of a tribal court, the Indian tribe shall retain exclusive jurisdiction, notwithstanding the residence or domicile of the child.</p>
		<p>(c) State court proceedings; intervention - In any State court proceeding for the foster care placement of, or termination of parental rights to, an Indian child, <span style='color: gold;'>the Indian custodian of the child and the Indian child’s tribe shall have a right to intervene at any point in the proceeding</span>. </p>
	</section>
	<section>
		<h3>Key Sections</h3>
		<h4>25 U.S.C. &sect; 1915</h4>
		<p>(a) Adoptive placements; preferences - In any adoptive placement of an Indian child under State law, a <span style='color: gold;'>preference shall be given, in the absence of good cause to the contrary, to a placement with (1) a member of the child’s extended family; (2) other members of the Indian child’s tribe; or (3) other Indian families</span>.</p>
	</section>
	<section>
		<h3>Key Sections</h3>
		<p>(b) Foster care or preadoptive placements; criteria; preferences - Any child accepted for foster care or preadoptive placement shall be placed in the least restrictive setting which most approximates a family and in which his special needs, if any, may be met. The child shall also be placed within reasonable proximity to his or her home, taking into account any special needs of the child. In any foster care or preadoptive placement, a preference shall be given, in the absence of good cause to the contrary, to a placement with:</p>
		<p style='padding-left:1em;'>(i) <span style='color: gold;'> a member of the Indian child’s extended family</span>;</p>
		<p style='padding-left:1em;'>(ii) a foster home licensed, approved, or 	<span style='color: gold;'>specified by the Indian child’s tribe</span>; </p>
		<p style='padding-left:1em;'>(iii) <span style='color: gold;'>an Indian foster home</span> licensed or approved by an authorized non-Indian licensing authority; or </p>
		<p style='padding-left:1em;'>(iv) <span style='color: gold;'>an institution for children approved by an Indian tribe</span> or operated by an Indian organization which has a program suitable to meet the Indian child’s needs.</p>
	</section>
	<section>
		<h3>Discussion</h3>
		<p>How does this fit with the three world views?</p>
		<p>How does this fit with the 5 family impact principles?<p>
	</section>
</section>


