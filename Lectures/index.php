<?php
$lecture = filter_input(INPUT_GET, 'lesson', FILTER_SANITIZE_SPECIAL_CHARS);
$live = filter_input(INPUT_GET, 'live', FILTER_SANITIZE_SPECIAL_CHARS);

if ($lecture == 'welcome') {
	require_once('welcome.php');
	} elseif ($lecture == 'why'){
	require_once('why.php');
	} elseif ($lecture == 'defining'){
	require_once('defining.php');
	} elseif ($lecture == 'vs'){
	require_once('vs.php');
	} elseif ($lecture == 'global'){
	require_once('global.php');
	} elseif ($lecture == 'US'){
	require_once('US.php');
	} elseif ($lecture == 'legit'){
	require_once('legit.php');
	} elseif ($lecture == 'lens'){
	require_once('lens.php');
	} elseif ($lecture == 'paradox'){
	require_once('paradox.php');
	} elseif ($lecture == 'roots'){
	require_once('roots.php');
	} elseif ($lecture == 'process'){
	require_once('process.php');
	} elseif ($lecture == 'evidence'){
	require_once('evidence.php');
	} elseif ($lecture == 'careers'){
	require_once('careers.php');
	} elseif ($lecture == 'advocacy'){
	require_once('advocacy.php');
	} elseif ($lecture == 'impact'){
	require_once('impact.php');
	} elseif ($lecture == 'current'){
	require_once('current.php');
	}else {
		echo '<div class="row">';
		echo '<div  style="text-align: center;" class="col-md-8 col-md-offset-2">';
		echo '<h1>Uh oh! It looks like this lecture isn&#39;t available right now!</h1>';
		echo '<h3>Perhaps you should go back to the <a href="../index.html">Home Page</a>.</h3>';
		echo '<img class="img-responsive" alt="You are not supposed to be here!" src="http://1.bp.blogspot.com/-iR4yxLeTPHI/UBq9DLFfuEI/AAAAAAAAVzY/qWe7I-0GMeo/s500/gang.gif">';
		echo '</div>';
	}
?>

<?php require_once('footer.php'); ?>