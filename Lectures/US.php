<?php
# toggle whether page is live
$live = True;
require_once('redirect.php');

$title = 'US Perspectives on Family Policy';
$id = 'night';
$theme = 'css/theme/'.$id.'.css';


## Other Available Themes
## $theme = 'http://pablocubi.co/mozreveal/css/theme/one-mozilla.css';
## Predefined Themes
## beige, blood, default, moon, night, serif, simple, sky, sky-jeopardy, solarized
## $id = 'beige';
## $theme = 'css/theme/'.$id.'.css';

require_once('header.php');
?>
<!-- each slide is a section; everything else is automated in the support PHP -->
<section>
	<h1>Agenda</h1>
	<ol>
		<li>Contributions of Families to Society</li>
		<li>Differences between US Families and Other Western Nations</li>
		<li>Societal Forces that Shape US Family Life</li>
	</ol>
</section>
<section>
	<section>
		<h2>Contributions of Families to Society</h2>
	</section>
	<section>
		<h3>The Benefits of Families</h3>
		<p class="fragment">Professionals tend to focus on the private benefits of families</p>
		<p class="fragment">Policymakers tend to focus on the public benefits of families</p>
	</section>
	<section>
		<h3>The SNAF</h3>
		<p class="fragment">In the US, policymakers often perceive families using the <span style="color: yellow;">Standard North American Family</span> model</p>
		<p class="fragment">A two-parent, heterosexual, married unit with one or two children</p>
		<p class="fragment">SNAF as a yardstick for Family Performance</p>
	</section>
	<section>
		<h3>4 Public Benefits of Families to Society</h3>
		<ol>
			<li>Generating Productive Workers</li>
			<li>Raising Caring and Committed Citizens</li>
			<li>Making Efficient Investments to Reach Societal Goals</li>
			<li >Providing an Effective Means to Promote Child and Youth Development</li>
		</ol>
	</section>
	<section>
		<h3>Generating Productive Workers</h3>
		<p class="fragment">Families foster the development of <span style="color: yellow;">human capital</span> by instilling <span style="color: yellow;">hard</span> and <span style="color: yellow;">soft</span> skills in family members.</p>
		<p class="fragment">By improving availability of childcare, policies try to make women more accessible to the workforce and increase the quality of future workers</p>
		
	</section>	
	<section>
		<h3>Raising Caring and Committed Citizens</h3>
		<p class="fragment">Families instill desirable values  like leadership, self-confidence, social competence, civic involvement, self-reliance, empathy, and reduced hostility; Secure attachment is associated with improved outcomes;</p>
		<p class="fragment">Policies like maternity or paternity leave target improved attachment</p>
		<p class="fragment">Family Medical Leave Act (FMLA) allows for 12 weeks unpaid leave for workers who have accrued 1,250 hours during the prior 12 months and work at a company that employs over 50 persons</p>
	</section>	
	<section>
		<h3>Making Efficient Investments</h3>
		<p class="fragment"><span style="color:yellow;">Cost-Benefit Analysis</span> &mdash; Which investments have the best returns for public investments? Focus is primarily economic ($)</p>
		<p class="fragment">What is the economic value of the unpaid labor of families (parents, long term caretakers, etc.)? What would the cost be for the government to assume these roles?</p>
		<div class="fragment">
			<h4>Example</h4>
			<p>Is it worthwhile to offer paid or unpaid leave to care for aging adults? </p>
			<p>The estimated value of informal caregiving in the US is estimated at $450 billion.</p>
			<p>The current Medicaid budget is $391 billion.</p>
		</div>
	</section>	
	<section>
		<h3>Child and Youth Development</h3>
		<p class="fragment">Prevention research consistently favors family interventions over &#8220;youth-only&#8221; interventions for treating problems like substance abuse and poverty; effect sizes are as much as 9x greater</p>
		<p class="fragment">Policies like minimum wage laws, tax credits,means-tested income support, universal pre-K, infant home visits, parental leave, and public education access may reduce youth poverty rates</p>
	</section>
</section>
<section>
	<section>
		<h2>Differences between US Families and Other Western Nations</h2>
	</section>
	<section>
		<h3>Twin Standards</h3>
		<p class="fragment">Marriage and family are viewed as desirable and prestigious; committment to and sharing with partners is emphasized</p>
		<p class="fragment">Individualism is sacred; personal growth and self-expression is emphasized</p>
		<div  class="fragment">
		<p>The tension between these two ideas is not unique to the US; but most other western countries focus on one or the other more:</p>
			<ul>
				<li>In Italy, marriage is emphasized, with extramarital fertility and cohabitation considered unusual</li>
				<li>In Sweden, individualism is emphasized, with cohabitation and marriage being treated as equivalent</li>
			</ul>
		</div>
		<p class="fragment">Continuum vs Dichotomy</p>
	</section>
</section>
<section>
	<section>
		<h2>Societal Forces that Shape US Family Life</h2>
	</section>
	<section>
		<h3>3 Major Forces that Shape US Families</h3>
		<ol>
			<li>Tension Between Individualism and Family</li>
			<li>Strong Market Economy</li>
			<li>Weak Social Safety Net</li>
		</ol>
	</section>
	<section>
		<h3>Individualism as an Ideal</h3>
		<p class="fragment">Individual rights are emphasized (sometimes includes rights of individual's family), while responsibilities to society are minimized</p>
		<p class="fragment">&#8220;Anything that would violate our right to think for ourselves, judge for ourselves, make or own decisions, live our life as we see fit, is not only morally wrong, it is sacrilegious&#8221; - Bellah et al., 1996</p>
		<p class="fragment">Individualism is emphasized in popular media; heroes are individuals and disconnected from society</p>
		<p class="fragment">Collective institutions are often individualized when brought to the US - ex: Religion as an individual belief rather than an organized community, hymn changes</p>
	</section>
	<section>
		<h3>Individualism as an Ideal</h3>
		<p>Divestment Planning <span class="fragment">&mdash; programs to allow older adults to move resources to younger family members in order to take advantage of government-financed programs and avoid spending on care</span></p>
		<p>Jury Duty <span class="fragment">&mdash; we emphasize the importance of juries, but nobody wants to serve</span></p>
		<p>Civic Disengagement <span class="fragment">&mdash; we &#8220;vote less, joined less...trusted lest, invested less time in public affairs and withour friends, neightbors, and even our families&#82212; (Putnam, 2002)</span></p>
		<p>Rise of the Childfree Movement</p>
			<ul>
				<li><a href="http://www.nokidding.net/">No Kidding</a> and <a href="http://www.reddit.com/r/childfree">Childfree</a></li>
				<li><a href="http://img01.deviantart.net/4bef/i/2013/008/3/a/dinkleberg__by_akari_61-d5qwvqg.jpg">DINK</a> <span class="fragment">&mdash; Double Income, No Kids</span></li>
				<li>THINKER <span class="fragment">&mdash; Two Healthy Incomes, No Kids, Early Retirement</span></li>
				<li>SITCOM <span class="fragment">&mdash;  Single Income, Two Children, Opressive Marriage</span></li>
			</ul>
	</section>
	<section>
		<h3>Marriage and Family as an Ideal</h3>
		<p class="fragment">Over 90% of Americans express a desire to be married, and emphasize valuing marriage</p>
		<p class="fragment">Higher rates of marriage; briefer cohabitations</p>
		<p class="fragment">US childrearing sees children as dependent, so socialization focuses on creating independence</p>
		<p class="fragment">US families emphasize teaching morals about cooperation and serving others</p>
	</section>
	<section>
		<h3>Marriage as an Ideal</h3>
		<p class="fragment">Higher rates divorce</p>
		<p class="fragment">Greater rates of partner instability; 10% of US women have 3+ partners by 35, compared to 2% in France or Canada</p>
		<p class="fragment">Marriage ideal is used to justify the decision to marry, and individual ideal to justify divorce/separation</p>
		<p class="fragment">Family instability is associated with reduced partner trust, increased juvenile delinquency, and lower rates of adolescent suicide</p>
	</section>
	<section>
		<h3>Strong Market Economy</h3>
		<p>Market Economy <span class="fragment"> &mdash; &#8220;An economic system in which production, investments, and prices are controlled by free market forces, by supply and demand, without interference by government control mechanisms&#8221;</span></p>
		<p>Capitalism<span class="fragment"> &mdash; &#8220;An economic and cultural system based on free market competition between private owners of good and corporations that places a high value on work and competition for profit&#8221;</span></p>
		<p>Gross Domestic Product (GDP) <span class="fragment">&mdash; &#8220;The total value of goods and services produced in a country during a given year&#8221;</span></p>
	</section>
	<section>
		<h3>Strong Market Economy</h3>
		<p class="fragment">Strong Market Economies thrive on and promote individualism</p>
		<div class="fragment">
		<p>Four Ways Market Economies Impact Family Life:</p>
		<ol>
			<li>Shaping Family Formation</li>
			<li>Shaping Family Function</li>
			<li>Replacing Family Functions</li>
			<li>Changing Thinking about Families</li>
		</ol>
		</div>
	</section>	
	<section>
		<h3>Shaping Family Formation</h3>
		<h4>&#8220;Diverging Destinies&#8221;</h4>
		<p class="fragment">Marriage is an economic act</p>
		<p class="fragment">College educated parents tend to stay marry, stay married, get well-paying jobs, and delay childbirth</p>
		<p class="fragment">Less educated parents tend to marry less, divorce more, have children out of wedlock, and secure poorer-paying jobs</p>
		<p class="fragment">This divergence is driven by economic shifts: job availability &amp; income security</p>
	</section>
	<section>
		<h3>Shaping Family Function</h3>
		<p class='fragment'>&#8220;Work is what they do. Family is why they live.&#8221;</p>
		<p class='fragment'>&#8220;Arsenic hour&#8221;; work is a place of security, home is chaos and stress</p>
		<p class="fragment">Work is increasingly a place of stability, ritual, and social</p>
	</section>
	<section>
		<h3>Replacing Family Functions</h3>
		<div  class="fragment">
		<p>Family functions are increasingly commercialized and purchased</p>
		<ul>
			<li>Birthday Parties</li>
			<li>Food, clothing, and vehicle repair</li>
			<li>Kiddy taxies</li>
			<li>Commerical Surrogacy</li>
			<li>Baby Naming Consultants</li>
			<li>Dating Coaching</li>
			<li>Elder Companionship</li>
			<li>Wedding Planning</li>
		</ul>
		</div>
	</section>
	<section>
		<h3>Changing Thinking about Families</h3>
		<p class="fragment">The value of caretaking and childrearing is hard to quantify, and has minimal short term returns; as a result these are devalued</p>
		<p class="fragment">Economic Models of Relationships &amp; Marital Calculus</p>
			<ul class="fragment">
				<li>&#8220;What do I get out of this relationship?&#8221; 
				</li>
				<li>&#8220;Am I giving more to the relationship than I'm getting out of it?&#8221;</li>
			</ul>
		<p class="fragment">Are these approaches appropriate for use with families?</p>
	</section>
	<section>
		<h3>Weak Social Safety Nets</h3>
		<p>Social Safety Net <span class="fragment"> &mdash; &#8220;A system of government policies that provides social and economic supports to individuals and families</span></p>
		<p class="fragment">Is it morally right to provide government support wthat could conceivably replace individual effort?</p>
		<p class="fragment">US policy creates a relatively weak social safety net relative to other Western countries</p>
	</section>
	<section>
		<h3>Examples of US Policies relying on families rather than a Social Safety Net:</h3>
		<ul>
			<li>Parent responsibility for school supplies</li>
			<li>Hospital discharge before full recovery</li>
			<li>Employer expectations that families will allow for long work hours, unexpected work obligations, and favor work over family events (birthdays, marriage, etc.) or needs (child or spousal illness)</li>
			<li>Care for disabled persons is primarily the task of the family, not the government</li>
		</ul>
	</section>
	<section>
		<h2>Living on the Safety Net</h2>
	</section>
	<section>
		<h3>SNAP Eligibility</h3>
		<p>In the US, SNAP Eligibility is determined by three factors:</p>
		<ol>
			<li>Gross Monthly Income at or below 130% of the Poverty Line</li>
			<li>Net Income at or below the poverty line</li>
			<li>Family assets below $ \$$2,250</li>
		</ol>
	</section>
	<section>
		<h3>SNAP Eligibility</h3>
		<p>Net income recognizes that family income pays for more than just food. The Net Income takes into account the following deductions from Gross Income:</p>
			<ul>
				<li>Standard Deduction: $ \$$155 for households of 1-3 members and $ \$$165, $ \$$193, and $ \$$221 for households with four, five, and six or more members, respectively</li>
				<li>Earning Deduction (31%): 20% of Income to account for taxes</li>
				<li>Dependent Care Deduction(4%): Out of pocket costs for dependent care</li>
				<li>Child Support Deduction(2%): Any legally required child support payments</li>
				<li>Medical Expense Deduction(4%): Any medical expenses above $ \$$35/month</li>
				<li>Shelter Deduction (71%): The amount by which housing costs exceed 50% of the household's net income after all other deductions; capped at $490</li>
			</ul>	
	</section>
	<section>
		<h3>SNAP Eligibility</h3>
		<p>People who are convicted of drug trafficking, who are running away from a felony warrant, who break Food Assistance Program rules on purpose, who are noncitizens without a qualified status, and some students in colleges or universities are not eligible for food assistance benefits.</p>
		<p>Additionally, workers on strike, undocumented immigrants, and certain documented immigrants are ineligible.</p>
		<p>Unemployed childless adults without disabilities are capped at 3 months of benefits during any 3 year period.</p>
		<p>In Florida, able bodied adults must work at least 20 hours per week when averaged monthly, or be seeking employment.</p>
	</section>
	<section>
	<h3>Income Eligibility Requirements</h3>
	<table class="reveal">
	<tr><td style="text-align: center;"><strong>Household Size</strong></td>
	<td>
	<p style="text-align: center;"><strong>Gross monthly income </strong></p>
	<p style="text-align: center;"><strong>(130 percent of poverty)</strong></p>
	</td>
	<td>
	<p style="text-align: center;"><strong>Net monthly income </strong></p>
	<p style="text-align: center;"><strong>(100 percent of poverty)</strong></p>
	</td>
	</tr><tr><td>
	<p style="text-align: center;">1</p>
	</td>
	<td style="text-align: center;">$1,265</td>
	<td style="text-align: center;">$ 973</td>
	</tr><tr><td>
	<p style="text-align: center;">2</p>
	</td>
	<td style="text-align: center;">1,705</td>
	<td style="text-align: center;">1,311</td>
	</tr><tr><td>
	<p style="text-align: center;">3</p>
	</td>
	<td style="text-align: center;">2,144</td>
	<td style="text-align: center;">1,650</td>
	</tr><tr><td>
	<p style="text-align: center;">4</p>
	</td>
	<td style="text-align: center;">2,584</td>
	<td style="text-align: center;">1,988</td>
	</tr><tr><td>
	<p style="text-align: center;">5</p>
	</td>
	<td style="text-align: center;">3,024</td>
	<td style="text-align: center;">2,326</td>
	</tr><tr><td>
	<p style="text-align: center;">6</p>
	</td>
	<td style="text-align: center;">3,464</td>
	<td style="text-align: center;">2,665</td>
	</tr><tr><td>
	<p style="text-align: center;">7</p>
	</td>
	<td style="text-align: center;">3,904</td>
	<td style="text-align: center;">3,003</td>
	</tr><tr><td>
	<p style="text-align: center;">8</p>
	</td>
	<td style="text-align: center;">4,344</td>
	<td style="text-align: center;">3,341</td>
	</tr><tr><td>
	<p style="text-align: center;">Each additional member</p>
	</td>
	<td style="text-align: center;">+440</td>
	<td style="text-align: center;">+339</td>
	</tr>
	</table>
	</section>
	<section>
		<h3>Maximum Benefits</h3>
		<p>Families are expected to spend 30% of income on food; if they have no income the maximum benefit is awarded</p>
		<table class="reveal">
		<tr>
			<td>Household <br /> Size</td><td>Max Benefit <br /> Per Month</td><td>Per Person/Week <br /> (4 wks/mo)</td><td>Per Person-Meal <br /> (3 x 30 Days)</td>
		</tr>
		<tr>
			<td>1</td><td>$194</td><td>$48.50</td><td>$2.15</td>
		</tr>
		<tr>
			<td>2</td><td>$357</td><td>$44.62</td><td>$1.98</td>
		</tr>
		<tr>
			<td>3</td><td>$511</td><td>$42.58</td><td>$1.89</td>
		</tr>
		<tr>
			<td>4</td><td>$649</td><td>$40.56</td><td>$1.80</td>
		</tr>
		<tr>
			<td>5</td><td>$771</td><td>$38.55</td><td>$1.71</td>
		</tr>
		<tr>
			<td>6</td><td>$925</td><td>$38.54</td><td>$1.71</td>
		</tr>
		<tr>
			<td>7</td><td>$1,022</td><td>$36.50</td><td>$1.62</td>
		</tr>
		<tr>
			<td>8</td><td>$1,169</td><td>36.53</td><td>$1.62</td>
		</tr>
		<tr>
			<td>Addl Persons</td><td>$146</td><td>36.50</td><td>$1.62</td>
		</tr>
		</table>
	</section>
	<section>
		<h3>Who is eligible for SNAP Benefits?</h3>
		<p>Darlene is a single mother of 2, who is employed full time (40h/wk) at minimum wage (&#36;7.25/h).</p>
		<p class="fragment">Darlene earns $ \$$1256 per month, which is below the $ \$$2144 cutoff. Darlene is <span style="color:green;">eligible</span>.</p> 
		<p>Paula and Carl are the married parents of two children, and are employed full time (40h/wk) at just above minimum wage (&#36;7.50/h).</p>
		<p class="fragment">Paula and Carl earn $ \$$2616 per month, which is above the $ \$$2584 cutoff. They are <span style="color:red;">ineligible</span>.</p>
 		<p>Michelle is a 27 year old single mother of 3, who is employed full time (40h/wk) at the proposed new minimum wage (&#36;15/h). </p>
		<p class="fragment">Michell earns $ \$$2616 per month, which is above the $ \$$2584 cutoff. Darlene is <span style="color:red;">ineligible</span>.</p> 
	</section>
	<section>
		<h3>What Benefits can Darlene Recieve?</h3>
		<p>Assuming Darlene pays $ \$120$ per month for childcare with a family friend, and $ \$900$ per month on rent for a 3-bedroom apartment</p>
		<p>Gross Income ($ \$1256$)</p>
		<p>&mdash; Standard Deduction ($ \$155$)</p>
		<p>&mdash; Earning Deduction ($ \$251$)</p>
		<p>&mdash; Childcare Deduction ($ \$90$) = $ \$760$ &#8220;Countable Income&#8221;</p>
		<p>&mdash; Shelter Deduction ($ \$490$) = $ \$270$ Net Income</p>
		<p>$ \$270$ x 30&#37; = $ \$81$ Expected Contribution to Food</p>
		<p>Max Benefit ($ \$511$) - Expected Contribution ($ \$81$) = $ \$430$ Family Benefit</p>
		<p>Assuming 90 meals per month, $ \$1.59$ per person per meal</p>
	</section>
	<section>
		<h3>What SNAP Pays for:</h3>
		<ul>
			<p>Foods for the household to eat, such as:  </p>
			<li>breads and cereals  </li>
			<li>fruits and vegetables  </li>
			<li>meats, fish and poultry   </li>
			<li>dairy products</li>
			<li>Seeds and plants which produce food for the household to eat.</li>
		</ul>
	</section>
	<section>
		<h3>What SNAP Does Not Pay for:</h3>
		<ul>
			<li>Beer, wine, liquor, cigarettes or tobacco</li>
			<li>pet foods  </li>
			<li>soaps, paper products   </li>
			<li>household supplies</li>
			<li>Vitamins and medicines</li>
			<li>Food that will be eaten in the store</li>
			<li>Hot foods</li>
		</ul>
	</section>
	<section>
		<h3>Your Challenge</h3>
		<p>Because Darlene's rent costs more than her deduction, she is forced to rely entirely on her food stamp benefit ($107.50/wk) to purchase food for herself and her two children (ages 2 and 5).</p>
		<p>Using prices listed online for grocery stores like Walmart or Publix, create a 1 week shopping list for Darlene that will feed her and her two children while remaining in her budget.</p>
		<p><small>Keep in mind that Darlene works 8 hours per day, with a 45 minute commute, so she has limited time and energy available for cooking. She also has limited storage, so buying in bulk isn't an option.</small></p>
		<p class="fragment">Could you survive on $ \$1.59$ per meal? How would this affect your productivity?</p>
	</section>
</section>
<section>
	<h1>Next Up:</h1>
	<h2><a href="./?lesson=legit">Families as a Legitimate Focus of Public Policy</a></h2>
</section>

