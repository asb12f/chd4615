<?php
# toggle whether page is live
$live = True;
require_once('redirect.php');

$title = 'Families as a Legitimate Focus of Public Policy';
$id = 'night';
$theme = 'css/theme/'.$id.'.css';


## Other Available Themes
## $theme = 'http://pablocubi.co/mozreveal/css/theme/one-mozilla.css';
## Predefined Themes
## beige, blood, default, moon, night, serif, simple, sky, sky-jeopardy, solarized
## $id = 'beige';
## $theme = 'css/theme/'.$id.'.css';

require_once('header.php');
?>
<!-- each slide is a section; everything else is automated in the support PHP -->
<section>
	<h1>Agenda</h1>
	<ol>
		<li>History of Family Policy as a Field</li>
		<li>American Values and Priorities</li>
		<li>Philanthropy and Family Policy</li>
		<li>Federal and State Law and Family Policy</li>
	</ol>
</section>
<section>
	<section>
		<h2>History of Family Policy as a Field</h2>
	</section>
	<section>
		<h3>John Adams vs Thomas Jefferson</h3>
		<p>What is the appropriate size and role for government in a democracy?</p>
		<p>Jefferson <span class='fragment'>&ndash; States rights, small government, reduced spending, people as inherently good</p>
		<p>Adams <span class='fragment'>&ndash; Strong centralized government, increased government scope and spending, people as inherently selfish</p>
		<p class='fragment'>Not &#8220;Do families need support?&#8221; but &#8220;Should support be provided by government?&#8221;</p>
		<p class='fragment'>Science can inform policy, but not decide it</p>
	</section>
	<section>
		<h3>1973</h3>
		<p><a href="http://www.jstor.org.proxy.lib.fsu.edu/stable/1084137?seq=1#page_scan_tab_contents">Senate Subcommittee on the State of American Families</a></p>
		<p>Family Impact Statements <span class='fragment'>&ndash; Formal written statements that assess the impact of government policies and programs on family well-being</span></p>
		<p>Family Impact Seminars <span class='fragment'>&ndash; Developed to study whether family impact statements were the best approach for prioritizing families in policy decisions</p>		
	</section>
	<section>
		<h3>1980</h3>
		<p>White House Conference on Families <span class='fragment'>&ndash; Intended to grant families legitimacy in policy, but became politically contentious</span></p>
		<h3>1981</h3>
		<p>Gilbert Steiner <span class='fragment'>declares family policy &#8220;futile&#8221; and a &#8220;fad&#8221;</span></p>
		<h3>1984</h3>
		<p>Theodora Ooms <span class='fragment'>&#8220;All talk of 'family' in policy and research circles became distinctly unfashionable and the pendulum swung back once again to single issue and single constituency research and avocacy&#8221;</span></p>
	</section>
	<section>
		<h3>Decade of Disregard</h3>
		<p>After the failures of the early 1980's family policy was eseentially ignored until the early 1990's.</p>
		<p>The resurgence of families as a legitimate area of policy is due primarily to changes in the values and priorities of the American people</p>
	</section>
</section>
<section>
	<section>
		<h2>American Values and Priorities</h2>
	</section>
	<section>
		<h3>Family is Important</h3>
		<p class="fragment">90% of Americans report high levels of satisfaction with family life</p>
		<p class="fragment">Over 75% report presidential candidates position on family values is important</p>
		<p class="fragment">&#8220;Family values&#8221; is typically associated with family structure or integrity, healthcare, morality, or abortion</p>
		<p class="fragment">12% think family values is a ploy for winning votes</p>
	</section>
	<section>
		<h3>Do Family Values mean Supporting Family Programs?</h3>
		<p class="fragment">Only 43% of Americans believe the government should help the poor and needy if it means going deeper into debt (2012)</p>
		<p class="fragment">Partisan Divide</p>
		<p class="fragment">Support exists for specific policies - funding for childcare, paid FMLA, and universal access to sick days</p>
		<p class="fragment">Child-free movement agrues against policies for children on the basis of discrimination; push for change on childcare and tax credits</p>
		<p class="fragment">Most Americans see parenting as a family responsibility, not a government one, but see government as providing conditions for parents to be effective</p>
		<p class="fragment">Many worry that government policies may lend support to one family form over another</p>
	</section>
</section>
<section>
	<section>
		<h2>Philanthropy and Family Policy</h2>
	</section>
	<section>
		<h3>An Alternative to Government Policy</h3>
		<p class='fragment'>Nonprofits and private individuals can contribute time, money, and resources towards issue advocacy and solving problems</p>
		<p class='fragment'>Investments tend to be smaller than what government provides</p>
		<iframe class='fragment' width="320" height="180" src="https://www.youtube.com/embed/ouZ3UfgrKe4" frameborder="0" allowfullscreen></iframe>
		<iframe class='fragment' width="320" height="180" src="https://www.youtube.com/embed/h9s_CjEafLc?list=PL6093EF7B5A25E274" frameborder="0" allowfullscreen></iframe>
	</section>
	<section>
		<h3>How Philanthropy Shapes Policy</h3>
		<ul>
			<li>Creating Momentum for Programs and Policy</li>
			<li>Focusing on Research and Data Collection</li>
			<li>Focusing on Practice in Academic and Policymaking Communities</li>
			<li>Addressing Barriers to Social, Economic, and Racial Disparities</li>
		</ul>
		<iframe width="320" height="180" src="https://www.youtube.com/embed/5PNzR-bMCyI" frameborder="0" allowfullscreen></iframe>
	</section>
</section>
<section>
	<section>
		<h2>Federal / State Law and Family Policy</h2>
	</section>
	<section>
		<h3>Growth of Famiy Policy</h3>
		<p>Starting in 1990, family policies began growing at the national level</p>
		<ul>
			<li>Increased spending on children and families</li>
			<li>Major legislation on adoption, child support, education and planning, EITC, FMLA, food stamps, marriage, SSI, and welfare</li>
			<li>Increased spending on over 80 means-tested programs</li>
			<li>In 2013, 10% of Federal Expenditures were spent on Children; 41% on the Elderly and Disabled; 20% on Defense</li>
		</ul>
	</section>
	<section>
		<h3>Effects on Families</h3>
		<ul>
			<li>More families in poverty are part of the workforce (43%)</li>
			<li>More families have access to healthcare today than at any time prior in US history</li>
			<li>Children's education is now more closely monitored and tracked</li>
		</ul>
	</section>
	<section>
		<h3>Trends in Policymaking</h3>
		<ol>
			<li>Patchwork policies and programs</li>
			<li>Variation in funding sources
				<ul>
					<li>Programs for the elderly tend to be <span class='fragment'>federally</span> funded</li>
					<li>Programs for children tend to be <span class='fragment'>locally</span> funded</li>
					<li>Programs for the elderly tend to have funding mandates and built in cost-of-living adjustments</li>
					<li>Programs for the children, education, and families tend to require annual appropriateions and lack cost-of-living adjustments</li>
				</ul></li>
			<li>Transition from Cash Support to Work Support</li>
			<li>Devolution of Federal authority on family policies</li>
		</ol>
	</section>
	<section>
		<h3>1990 Child Care and Development Block Grant</h3>
		<h4>George H. W. Bush</h4>
		<ul>
			<li class='fragment'>Expanded EITC, improving access to low-income families and increasing credit for families with infants</li>
			<li class='fragment'>Provides full-year funding for Head Start</li>
			<li class='fragment'>$22.5 Billion to improve child care</li>
			<li class='fragment'>Subsidizes child care for families through vouchers</li>
			<li class='fragment'>Creates safety and licensing requirements for child care centers</li>
			<li class='fragment'>Funds research on childcare and early child development</li>
			<li class='fragment'>Re-authorized in 1996 and <a href='http://www.acf.hhs.gov/programs/occ/ccdf-reauthorization'>2014</a></li>
		</ul>
	</section>
	<section>
		<h3>1996 Personal Responsibility and Work Opportunity Reconciliation Act (PRWOA)</h3>
		<h4>Bill Clinton</h4>
		<ul>
			<li class='fragment'>Reinvented the US Welfare System; shift from entitlement to work support</li>
			<li class='fragment'>Replaced Aid to Families with Dependent Children (AFDC) and Job Opportunities and Basic Skills Training program (JOBS) with <a href='http://www.cbpp.org/research/policy-basics-an-introduction-to-tanf'>Temporary Assistance for Needy Families(TANF)</a></li>
			<li class='fragment'>Created time-limited benefits - must begin work within 2 years, and limited to 5 years of benefits</li>
			<li class='fragment'>Single parents are required to participate in work activities for at least 30 hours per week. Two-parent families must participate in work activities 35 or 55 hours a week, depending upon circumstance.</li>
			<li class='fragment'>Encouraged marriage and two-parent families through increased subsidies for these families</li>
			<li class='fragment'>Increasing child support enforcement</li>
			<li class='fragment'>Devolved program; many states added additional requirements or exceptions</li>
			<li class='fragment'>Reauthorized in 2005, but not in 2010; currently relies on short-term extensions</li>
		</ul>
	</section>
	<section>
		<h3>2001 Revised Elementary &amp; Secondary Education Act</h3>
		<h4>George W. Bush</h4>
		<ul>
			<li class='fragment'><a href='http://www.ed.gov/esea'>AKA the No Child Left Behind Act</a></li>
			<li>Goals:
				<ul class='fragment'>
				<li>reduce testing gap</li>
				<li>create accountability for school systems</li>
				<li>increase education quality</li>
				<li>implement "scientifically based research" practices in the classroom, parent involvement programs, and professional development activities for those students that are not encouraged or expected to attend college</li>
				<li>support early literacy programs</li>
				</ul></li>
			<li class='fragment'>Emphasizes reading, language arts, mathematics and science achievement as "core academic subjects."</li>
			<li class='fragment'>Authorizes federal funding for annual standardized school testing</li>
		</ul>
	</section>
	<section>
		<h3>No Child Left Behind (continued)</h3>
		<ul>
			<li class='fragment'>Requires schools to demonstrated Adequate Yearly Progress (AYP) for students; enstates escalating corrective actions for repeated failure to achieve AYP</li>
			<li class='fragment'>Devolution of policy; states set AYP goals</li>
			<li class='fragment'>Requires schools to grant access to students to military recruiters</li>
			<li class='fragment'>Provides limited adjustments for economically disadvantaged students, students with disabilities, and students with limited English proficiency</li>
		</ul>
	</section>
	<section>
		<h3>2010 Patient Protection and Affordable Care Act</h3>
		<h4>Barack Obama</h4>
		<ul>
			<li class='fragment'><a href='http://www.hhs.gov/healthcare/rights/law/index.html'>AKA Obamacare</a></li>
			<li>Goals:
				<ul class='fragment'>
				<li>improve access to healthcare in the US</li>
				<li>extend existing health insurance coverage for students and seniors</li>
				<li>reduce lifetime healthcare costs</li>
				<li>create a <a href='https://www.whitehouse.gov/files/documents/healthcare-fact-sheets/patients-bill-rights.pdf'>patient's bill of rights</a></li>
				</ul></li>
			<li class='fragment'>expanded Medicaid to people under 65 earning 133% of the poverty level</li>
			<li class='fragment'>created federal home-visiting program for medically at-risk children</li>
			<li class='fragment'>extended depdendent coverage to 26</li>
		</ul>
	</section>
	<section>
		<h3>Obamacare (continued)</h3>
		<ul>
			<li class='fragment'>required break time for breastfeeding for 12 months after childbirth</li>
			<li class='fragment'>banned insurance exclusion of children with preexisting conditions</li>
			<li class='fragment'>banning lifetime coverage caps</li>
			<li class='fragment'>Out-of-pocket costs are capped at $\$5,950$/individual/year and $\$11,900$/family/year for new plans</li>
			<li class='fragment'>prohibing cancelation by insurers except for fraud</li>
			<li class='fragment'>created public insurance market; created tax penalties for lack of insurance</li>
			<li class='fragment'>provides subsidies for insurance for low-income families</li>
			<li class='fragment'>reviews premium increases by insurers</li>
			<li class='fragment'>free preventative services on all new plans mammograms, colonoscopies, immunizations, pre-natal and new baby care)</li>
			<li class='fragment'>creating additional accountability for healthcare professionals</li>
		</ul>
	</section>
	<!-- This section revised 9/30/15
	<section>
		<h3>Key Policies</h3>
		<table class='reveal'>
			<tr>
				<th>Year</th><th>Policy</th><th>Effects</th>
			</tr>
			<tr >
				<th>1990</th><th>Child Care and Development Block Grant</th><th class='fragment'>Expanded EITC, funded Head Start, $22.5 Billion to improve child care; re-authorized in 1996 and <a href='http://www.acf.hhs.gov/programs/occ/ccdf-reauthorization'>2014</a></th>
			</tr>
			<tr>
				<th>1993</th><th>Family and Medical Leave Act</th><th class='fragment'>12 weeks unpaid leave for serious illness or to care for ailing family member with job and health insurance security</th>
			</tr>
			<tr >
				<th>1993</th><th>Family Preservation and Support Act</th><th class='fragment'>$1 billion over 5 years for community-based services to avoid out-of-home placement</th>
			</tr>
			<tr>
				<th>1996</th><th>Personal Responsibility and Work Opportunity Reconciliation Act (PRWOA)</th><th class='fragment'>Major welfare reform; replaced Aid to Families with Dependent Children (AFDC) and Job Opportunities and Basic Skills Training program (JOBS) with TANF, creating time-limited benefits, increasing eligibility requirements, and encouraging marriage and two-parent families</th>
			</tr>
		</table>
	</section>
	<section>
		<h3>Key Policies</h3>
			<table class='reveal'>
			<tr>
				<th>Year</th><th>Policy</th><th>Effects</th>
			</tr>
			<tr>
				<th>1996</th><th>Defense of Marriage Act (DOMA)</th><th class='fragment'>Defined marriage as between one man and one woman, denied SS benefits to same-sex couples, eliminated requirement of States to recognize same-sex marriage certificates (overturned 2013)</th>
			</tr>
			<tr >
				<th>1997</th><th>Ammended Individuals with Disabilities Act (IDEA)</th><th class='fragment'>Increased protections of disabled children's rights to free, appropriate public education; required involvement of families in strategies to improve disabled children's educational outcomes; establishes principles like Individualized Education Plans, Free Appropriate Public Education, and Least Restrictive Environments</th>
			</tr>
			<tr >
				<th>2000</th><th>Children's Health Act</th><th class='fragment'>Funds research and services on child health issues; reauthorizes SAMSHA</th>
			</tr>
			</table>
	</section>
	<section>
		<h3>Key Policies</h3>
			<table class='reveal'>
			<tr>
				<th>2002</th><th>Revised Elementary & Secondary Education Act (No Child Left Behind Act)</th><th  class='fragment'>Creates new rules for scope and frequency of student testing; goal is to reduce testing gap and create accountability for school systems</th>
			</tr>
			<tr>
				<th>2003</th><th>Prosecutorial Remedies and Other Tools to End the Exploitation of Children Today Act (PROTECT)</th><th  class='fragment'>Improves funding, training, and scope of law enforcement for addressing crimes against children; codifies the Amber Alert program</th>
			</tr>
			<tr>
				<th>2003</th><th>Partial Birth Abortion Ban Act</th><th  class='fragment'>Banned partial-brith abortin, except hwen necessary to save the life of a mother</th>
			</tr>
			</table>
	</section>
	<section>
		<h3>Key Policies</h3>
			<table class='reveal'>
			<tr>
				<th>2003</th><th>Servicemembers Civil Relief Act</th><th  class='fragment'>Creates protections for members of the military and their dependents regarding termination of home and auto leases, eviction, and reposession</th>
			</tr>
			<tr>
				<th>2005</th><th>Higher Education Reconciliation Act</th><th  class='fragment'>Creates Academic Competitiveness Grants and Natioannl Science and Mathematics Acess to Retain Talent Grants for low-income students to attend college</th>
			</tr>
			<tr>
				<th>2009</th><th>Children's Health Insurance Program Reauthorization Act (CHIRPA</th><th  class='fragment'>Re-authorized SCHIP and expanded coverage to legal immigrant children, added dental insurance, adn expanded mental health coverage</th>
			</tr>
			</table>
	</section>
	<section>
		<h3>Key Policies</h3>
			<table class='reveal'>
			<tr>
				<th>2009</th><th>American Recovery and Reinvestment Act</th><th  class='fragment'>Reduced federal income tax withholding, created numerous tax credits and enhanced EITC and CHild Tax Credits; increased food stamp benefits; authorized $#2billion for cild care assistance for low-income families</th>
			</tr>
			<tr>
				<th>2010</th><th>Patient Protection and Affordable Care Act</th><th  class='fragment'>Expanded Medicaid to people under 65 earning 133% of the poverty level; created federa home-visiting program for at-risk children; extended depdendant coverage to 26; required break time for breastfeeding for 12 months after childbirth; banned insurance exclusion for preexisting conditions in children; banning lifetime coverage caps; prohibing cancelation by insurers except for fraud; </th>
			</tr>
			<tr>
				<th>Year</th><th>Policy</th><th  class='fragment'>Effects</th>
			</tr>
			
		</table>
	</section>
	-->
</section>
<section>
	<h1>Next Up:</h1>
	<h2><a href="./?lesson=lens">The Family Impact Lens</a></h2>
</section>
