<?php

# toggle whether page is live
$live = True;
require_once('redirect.php');

$title = 'Family Impact Analysis';
$id = 'night';
$theme = 'css/theme/'.$id.'.css';


## Other Available Themes
## $theme = 'http://pablocubi.co/mozreveal/css/theme/one-mozilla.css';
## Predefined Themes
## beige, blood, default, moon, night, serif, simple, sky, sky-jeopardy, solarized
## $id = 'beige';
## $theme = 'css/theme/'.$id.'.css';

require_once('header.php');
?>
<!-- each slide is a section; everything else is automated in the support PHP -->
<section>
	<h1>Agenda</h1>
	<ol>
		<li>Underlying Theory</li>
		<li>Principles, Revisited</li>
		<li>Implementations</li>
	</ol>
</section>
<section>
	<section>
		<h2>Underlying Theory</h2>
	</section>
	<section>
		<h3>Theory #1</h3>
		<ul>
			<li>Ecological Family Systems Theory</li>
		</ul>
		<p>Review: What two theories comprise the Ecological Family Systems Theory? Why?</p>
	</section>
	<section>
		<h3>Theory #2</h3>
		<ul>
			<li>Self-Efficacy Theory
			<ul class='fragment'>
				<li>Supporting family autonomy rather than supplanting family responsibility</li>
				<li>More short-term Govt intervention; lower long-term costs (early intervention)</li>
				<li>Strengths Based</li>
				<li>Meta-Analysis suggests programs that implement Self-Efficacy Theory tend to be more effective</li>
			</ul></li>
		</ul>
	</section>
	<section>
		<h3>Key Practices in Self-Efficacy Theory</h3>
		<ul>
			<li>Relational Practices
				<ul>
					<li>Dignity &amp; Respect</li>
					<li class='fragment'>Active Listening</li>
					<li class='fragment'>Compassion</li>
					<li class='fragment'>Empathy</li>
					<li class='fragment'>Respect</li>
				</ul>
			</li>	
			<li>Participatory Practices
				<ul>
					<li>Choice &amp; Collaboration</li>
					<li class='fragment'>Choices</li>
					<li class='fragment'>Input in Decision Making</li>
					<li class='fragment'>Flexible Involvement</li>
					<li class='fragment'>Individualized setting of family goals</li>
				</ul>
			</li>
		</ul>
	</section>
	<section>
		<h3>Theory #3</h3>
		<ul>
			<li>Kingdon's Open Policy Windows Theory
			<ul class='fragment'>
				<li>Policy happens when conditions are &#8220;just right&#8221;</li>
				<li>Convergence of problem recognition, policy solutions, and political climate</li>
				<li>Extends considerations beyond the family to include the policy maker</li>
			</ul></li>
		</ul>
	</section>
</section>
<section>
	<section>
		<h2>Principles, Revisited</h2>
	</section>
	<section>
		<h3>Origins and Notions</h3>
		<ul>
			<li class='fragment'>Originated by the Coalition of Family Organizations; <br>
			Revised by the Family Impact Institute in 2012</li>
			<li class='fragment'>Focus is both on the program or policy, and the actual implementation</li>
			<li class='fragment'>Not every principle applies to every policy; sometimes they may even conflict!</li>
			<li class='fragment'>Relative importance of principles will vary by policy</li>
			<li class='fragment'>Should always consider cost-effectiveness and political feasibility</li>
		</ul>
	</section>
	<section>
		<h3>The Principles (again)</h3>
		<ol>
			<li>Family Responsibility</li>
			<li>Family Stability</li>
			<li>Family Relationships</li>
			<li>Family Diversity</li>
			<li>Family Engagement</li>
		</ol>
	</section>
</section>
<section>
	<section>
		<h2>Implementations</h2>
	</section>
	<section>
		<h3></h3>
		<ol>
			<li>The Discussion Starter Method</li>
			<li>The Checklist Method</li>
			<li>The Family Impact Analysis Method</li>
			<li>The Toolkit</li>
		</ol>
	</section>
	<section>
		<h3>The Discussion Starter Method</h3>
		<ul>
			<li>Designed for panels, boards, commissions, or other open discussions with prepared questions</li>
			<li>Condenses the 5 principles into a single question</li>
		</ul>
	</section>	
	<section>
		<h3>The Discussion Starter Method</h3>
		<p>How will the policy, program, or practice:</p>
		<ul>
			<li>support rather than substitute for family members' responsibilities to one another?</li>
			<li>reinforce family members' commitment to each other adn to the stability of the family unit?</li>
			<li>recognize the power and persistence of family ties, and promote healthy couple, marital, and parental relationships?</li>
			<li>acknowledge and respect the diversity of family life (e.g., different cultural, ethnic, racial, and relgious backgrounds; family structures; geographic locales; socioeconomic statuses; stages of the life cycle; members who have special needs)?</li>
			<li>engage and work in partnership with familie?</li>
		</ul>
	</section>
	<section>
		<h3>The Checklist Method</h3>
		<ul>
			<li>Expands the 5 principles into a set of 33 detailed sub-questions</li>
			<li class='fragment'>Available for general use, and with specialized versions for some settings (schools, child care, etc.)</li>
			<li class='fragment'>Often used as a stand-alone tool to review policies or progams prior to enactment, and to evaluate subsequent effects</li>
			<li class='fragment'>Relatively quick and easy to use</li>
		</ul>
	</section>	
	<section>
		<h3>The Family Impact Analysis Method</h3>
		<ul>
			<li>A formal, in-depth method that uses the checklist to fully examine support for families</li>
			<li class='fragment'>Emphasis on whether goals are met, and whether there are unintended consequences</li>
			<li class='fragment'>4-Part Procedure
				<ol>
					<li>Select Program or Policy</li>
					<li>Determine Affected Families</li>
					<li>Applying the Checklist</li>
					<li>Disseminating Results</li>
				</ol></li>
		</ul>
	</section>
	<section>
		<h3>The Toolkit</h3>
		<p>A set of resources to help carry out Family Impact Analysis</p>
		<p><a href='https://www.purdue.edu/hhs/hdfs/fii/family-impact/'>Available Online</a></p>		
		<p>Also helpful: <a href='https://www.purdue.edu/hhs/hdfs/fii/wp-content/uploads/2015/06/fi_handbook_0712.pdf'>The Family Impact Handbook (pdf)</a></p>		
	</section>
</section>


