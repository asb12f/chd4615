<?php

# toggle whether page is live
$live = True;
require_once('redirect.php');

$title = 'Defining Family Policy';
$id = 'night';
$theme = 'css/theme/'.$id.'.css';


## Other Available Themes
## $theme = 'http://pablocubi.co/mozreveal/css/theme/one-mozilla.css';
## Predefined Themes
## beige, blood, default, moon, night, serif, simple, sky, sky-jeopardy, solarized
## $id = 'beige';
## $theme = 'css/theme/'.$id.'.css';

require_once('header.php');
?>
<!-- each slide is a section; everything else is automated in the support PHP -->
<section>
	<h1>Agenda</h1>
	<ol>
		<li>Exactly What is Family Policy?</li>
		<li>The Family Impact Lens</li>
	</ol>
</section>

<section>
	<section>
		<h1>Exactly What is Family Policy?</h1>
	</section>
	<section>
		<h2>What is a Family?</h2>
		<img src="../images/liloandstitch.jpg" alt="An Alternative Family"	/>
	</section>
	<section>
		<h3>Functional Definition</h3>
			<p>Families perform 5 explicit functions:</p>
			<ol>
				<li>Family Formation</li>
				<li>Partner Relationships</li>
				<li>Economic Support</li>
				<li>Childrearing</li>
				<li>Caregiving</li>
			</ol>
			<div class="fragment"
			<p>Do we consider anyone who fulfills these roles family? For instance, is a babysitter or bank loan officer a family member?</p>
			<img src="../images/babysitter.jpg" alt="Babysitter" />
			</div>
	</section>
	<section>
		<h3>Structural Definition</h3>
		<p>Families are people who exist in prescribed structural relationships, such as:</p>
		<ul>
			<li>Blood Relatives</li>
			<li>Legal Relationships (marriage, adoption, etc.)</li>
			<li>Residential Patterns (cohabitation)</li>
		</ul>
		<p class="fragment">What about pets? <a href="https://www.youtube.com/watch?v=qTSDL94_Y7M">Residential employees?</a> Nannies? <span style="color:#6CC417;">Extraterrestrial Refugees?</span></p>
		<p class="fragment">How broadly should we consider structural relationships? Are your roommates grandparents your family?</p>
	</section>
	<section>
		<h2>Which definition is most useful in each of these situations?</h2>
		<ul>
			<li class="fragment">Child Support Payments?</li>
			<li class="fragment">Family Tax Credits?</li>
			<li class="fragment">Grandparent's Visitation Rights?</li>
			<li class="fragment">Single Parent / Fragile Families?</li>
		</ul>
	</section>
	<section>
		<h2>Fictive Kin and Immigrant Communities</h2>
		<h3>Ebaugh &amp; Curry (2000)</h3>
		<p>Fictive Kin &mdash; <span class='fragment'>&#8220;a relationship, based not on blood or marriage but rather on religious rituals or close friendship ties, that replicates many of the rights and obligations usually associated with family ties&#8221;</span></p>
		<p>Social Capital &mdash; <span class='fragment'>&#8220;positions and relationships in groupings and social networks, including memberships, network ties, and social relations that can serve to enhance an individual's access to opportunities, information, material resources, and social status&#8221;</span></p>
	</section>	
	<section>
		<h2>3 Systems of Fictive Kin</h2>
		<ul>
			<li>Compadrazgo in Hispanic Immigrants</li>
			<li>Yoruba Houses of Ocha</li>
			<li>Respect systems among Asian Immigrants</li>
		</ul>		
		<p>For each, consider whether it is structural, functional, or both?</p>
	</section>
	<section>
		<h3>Compadrazgo - Structure</h3>
		<ul>
			<li class='fragment'>Requires one or two &#8220;sponsors&#8221; (<em>padrino</em> or <em>madrina</em>) for child at baptism, and again at first communion, confirmation, and baptism; a single child may have up to 8!</li>
			<li class='fragment'>Sponsors must commit to teaching faith and morality, and raise the child if the parents die or neglect their duties</li>
			<li class='fragment'>Extensive mutual rights and obligations exist between the sponsors, the child, and the family; relationships create and cement connections between families</li>
			<li class='fragment'>For a family with five children, there may be 40 <em>copadres</em> and <em>comadres</em>, a massive support system</li>
			<li class='fragment'>Marriage and sexual relationships are prohibited</li>
		</ul>
	</section>	
	<section>
		<h3>Houses of Ocha - Structure</h3>
		<ul>
			<li class='fragment'>Religious communities are organized into &#8220;houses&#8221;, which are groups of ~50 people who think of themselves as having a family relationship with the deities (<em>Orisha</em>) and with each other</li>
			<li class='fragment'>The priest is th leader of the house, and the members are his godchildren and godsiblings to one another; may also have aunts, uncles, nephews, and nieces, but these are ritualistic rather than blood relations</li>
			<li class='fragment'>The priest has material and spiritual responsibility to the godchildren; similar obligations exist between godsiblings</li>
			<li class='fragment'>Lineage is maintained through priesthood, similar to an extended family lineage</li>
			<li class='fragment'>Marriage and sexual relationships are prohibited within houses</li>
		</ul>
	</section>	
	<section>
		<h3>Asian Immigrants - Structure</h3>
		<ul>
			<li class='fragment'>Family members are known by age-positions (e.g., &#8220;Elder brother&#8221;, &#8220;Third Daugther&#8221;)</li>
			<li class='fragment'>Age bestows respect and obligations; elders are referred to as aunt or uncle even if unrelated</li>
			<li class='fragment'>Godparentsare fulfill parental duties</li>
			<li class='fragment'>&#8220;Covenant brothers&#8221; (co-baptism) and &#8220;adopted&#8221; siblings are expected to fulfill familial duties </li>
			<li class='fragment'>Persons identfied as a child's aunt, uncle, or parent may have no blood or legal relationship to the child</li>
			<li class='fragment'>US versions related to immigration patterns, where men immigrate first and build relationships, and then send for families</li>
			<li class='fragment'>&#8220;Paper Sons&#8221;</li>
		</ul>
	</section>	
	<section>
		<h3>Function of Fictive Kin</h3>
		<ul>
			<li class='fragment'>Spiritual Development and Cultural Continuity</li>
			<li class='fragment'>Social Control
				<ul>
					<li>Solidarity</li>
					<li>Conflict &amp; Reputation Management</li>
					<li>Sexuality</li>
				</ul></li>
			<li class='fragment'>Financial Support
				<ul>
					<li>&#8220;social insurance system&#8221; - loans and business support, sharing wealth</li>
					<li>Housing safety net</li>
					<li>Child and Elder Support</li>
					<li>Employment Support</li>
				</ul></li>
			<li class='fragment'>Social Support
				<ul>
					<li>Celebrations</li>
					<li>Mutual Care and Assistance</li>
					<li>Child and Elder Care</li>
				</ul></li>
		</ul>
	</section>
	<section>
		<h2>What is Policy?</h2>
		<p>&#8220;a plan or course of action carried out through a law, rule, code, or other mechanism in the public or private sectors. &#8221;</p>
		<img style="text-align: center;" src="../images/rules.jpg" alt="Da Rules, from Fairly Odd Parents" />
		<p class='fragment'>Remember, policies exist to try to solve problems, so to understand a policy you must understand the problem it hopes to solve!</p>

	</section>
	<section>
		<h2>What is Family Policy?</h2>
	</section>
	<section>
		<h3>Explicit vs Implicit</h3>
		<p class="fragment">Some policies explicitly target family function or structure as an end goal, or use families as their means of administering policy <span class="fragment">(for example, the EITC or Immigration)</span></p>
		<p class="fragment">Other policies implicitly affect families, even if they aren't designed specifically to impact or work through families <span class="fragment">(Healthcare policy, etc.)</span></p>
		<p class="fragment">For this course, when we refer to family policy, we're referring to policies which meet the explicit criteria.</p>
	</section>
</section>
<section>
	<section>
		<h1>The Family Impact Lens</h1>
	</section>
	<section>
		<h2>Family Policy vs the Family Impact Lens</h2>
		<p>Family Impact Lens is a framework that focuses on the implicit impacts of policy on familes.</p>
		<p>This can be applied to both explicit and implicit family policies.</p>
	</section>
	<section>
		<h3>The Role of Families in Policy</h3>
		<p>The Family Impact Lens considers three implicit roles families can play in policy:</p>
			<ol>
				<li>Families as a criterion for impact of a policy or program</li>
				<li>As a means for achieving policy goals unrelated to families</li>
				<li>As administrators of publc policy (eligibility, distribution of services, etc.)</li>
			</ol>
	</section>
	<section>
		<h3>Family Impact Lens Considerations</h3>
		<p>When policies are enacted or established (family policy):</p>
			<ul>
				<li>How families are affected by the issue</li>
				<li>If families contribute to an issue</li>
				<li>Whether involving families would improve the effectiveness or efficiency of the program or policy</li>
			</ul>
	</section>
	<section>
		<h3>Family Impact Lens Considerations (continued)</h3>
		<p>When considering how policies are implemented (family law):</p>
			<ul>
				<li>Family dignity (are families treated with respect?)</li>
				<li>Family decisionmaking and decisionmaking resources</li>
				<li>Family choice regarding services and participation</li>
				<li>Family involvement in professional collaboration</li>
			</ul>
	</section>
	<section>
		<h2>How can we use Family Policy and the Family Impact Lens?</h2>
		<p>Encourages a shift from individualistic to holistic consideration of policies</p>
		<p>Helps draw attention to overlooked groups, like non-custodial parents, fathers, or grandparents</p>
		<p>Improve the effectiveness and efficiency of programs and policies by </p>
	</section>
</section>

<section>
	<h1>Next Up:</h1>
	<h2><a href="./?lesson=vs">Individual vs Family Policy</a></h2>
</section>