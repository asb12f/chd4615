<?php
# toggle whether page is live
$live = True;
require_once('redirect.php');

$title = 'Careers in Family Policy';
$id = 'night';
$theme = 'css/theme/'.$id.'.css';


## Other Available Themes
## $theme = 'http://pablocubi.co/mozreveal/css/theme/one-mozilla.css';
## Predefined Themes
## beige, blood, default, moon, night, serif, simple, sky, sky-jeopardy, solarized
## $id = 'beige';
## $theme = 'css/theme/'.$id.'.css';

require_once('header.php');
?>
<!-- each slide is a section; everything else is automated in the support PHP -->
<section>
	<h1>Agenda</h1>
	<ol>
		<li>The 9 Roles</li>
	</ol>
</section>
<section>
	<section>
		<h2>The 9 Roles</h2>
	</section>
	<section>
		<h3>What's the most direct way to become involved in shaping policy?</h3>
		<p class='fragment'>Run for office!</p>
		<p class='fragment'>Pop Quiz: How old do you need to be to run for President? <span class='fragment'>35</span><br /> 
		For Senator? <span class='fragment'>30</span><br />
		For House Representative? <span class='fragment'>25</span><br />
		For Florida Governor? <span class='fragment'>30</span><br />
		For Tallahassee Mayor or Leon County Commissioner? <span class='fragment'>18-21</span><br />
		</p>
		<p class='fragment'>What if you don't want to run for office?</p>
	</section>
	<section>
		<h3>1. Research for Family Policy Formulation</h3>
		<p class='fragment'>Conduct research to identify or refute social problems</p>
		<p class='fragment'>Answer information needs of policymakers</p>
		<p class='fragment'>Collect data on well-being and family life</p>
		<p class='fragment'>Discover causal mechanisms and factors that contribute to social problems</p>
	</section>
	<section>
		<h3>2. Family Policy Implementation</h3>
		<p class='fragment'>Operationalizing Policy for Agencies</p>
		<p class='fragment'>Training frontline staff on how to follow new policies and procedures</p>
		<p class='fragment'>Identifying outcomes to measure implementation</p>
		<p class='fragment'>Monitoring implementation fidelity and effectiveness</p>
	</section>
	<section>
		<h3>3. Family Policy Evaluation</h3>
		<p class='fragment'>Are programs achieving their stated outcomes?</p>	
		<p class='fragment'>If so, who benefits the most? Who benefits the least?</p>	
		<p class='fragment'>If not, what problems are preventing them from being effective?</p>	
		<p class='fragment'>What factors contribute to the success or failure of a program - implementation, uptake, engagement, retention, etc?</p>	
	</section>
	<section>
		<h3>4. Family Research Integration</h3>
		<p class='fragment'>Bridging together research from diverse viewpoints</p>
		<p class='fragment'>Developing research reviews and syntheses that summarize existing knowledge</p>
		<p class='fragment'>Clearly conveying the level of confidence we have in findings without becoming overly technical</p>
		<p class='fragment'>Often in the form of policy briefs</p>
	</section>
	<section>
		<h3>5. Family Research Dissemination</h3>
		<p class='fragment'>Spreading research knowledge to both leglislators and the public</p>
		<div class='fragment' style='float:right; width:50%;'>
			<img src='http://41.media.tumblr.com/tumblr_m7vft1O6Cu1r7qpeho5_1280.jpg' alt='Bill Nye the Science Guy and Neil Tyson Degrasse'/> 
		</div>
		<p class='fragment'>Science communicators</p>
		<p class='fragment'>&#8220;In the internet age, policymakers suffer at once from too much and not enough information. What they need is the right information&#8221;</p>
		<p class='fragment'>Social Media Awareness</p>
		<p class='fragment'>&#8220;There are no good laws without good awareness&#8221; &ndash; Gloria Steinem</p>
	</section>
	<section>
		<h3>6. Family Impact Analysis</h3>
		<p class='fragment'>How do policies and families interact?</p>
		<p class='fragment'>Are there unintended or counter-productive effects?</p>
		<p class='fragment'>Family Impact Seminars!</p>
	</section>
	<section>
		<h3>7. Teaching Family Policy</h3>
		<p class='fragment'>Take my job!</p>
		<p class='fragment'>Civics education in the community</p>
		<p class='fragment'>Spreading the knowledge you've <small>(hopefully)</small> obtained</p>
	</section>
	<section>
		<h3>8. Citizen Engagement in Family Policy</h3>
		<p class='fragment'>Advocacy, Grass Roots Change</p>
		<p class='fragment'>Political organization</p>
		<p class='fragment'>Lobbying and public education about lawmaking</p>
		<p class='fragment'>Fostering public conversations and debates</p>
	</section>
	<section>
		<h3>9. University Involvement in Family Policy Scholarship</h3>
		<p class='fragment'>Research attention to policy issues</p>
		<p class='fragment'>Educating new family scholars and policy advocates</p>
		<p class='fragment'>Providing a center for public outrech and support; community engagement</p>
		<p class='fragment'>Connecting researchers, educators, and students to policymakers</p>
	</section>
	<section>
		<h3>Final Thought</h3>
		<p>Policy <span style='font-weight:900;'>WILL</span> affect your career; will your let career affect policy?</p>
	</section>
</section>
<section>
	<h1>Have a Great Summer!</h1>
</section>
