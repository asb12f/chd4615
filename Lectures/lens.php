<?php
# toggle whether page is live
$live = True;
require_once('redirect.php');

$title = 'The Family Impact Lens';
$id = 'night';
$theme = 'css/theme/'.$id.'.css';


## Other Available Themes
## $theme = 'http://pablocubi.co/mozreveal/css/theme/one-mozilla.css';
## Predefined Themes
## beige, blood, default, moon, night, serif, simple, sky, sky-jeopardy, solarized
## $id = 'beige';
## $theme = 'css/theme/'.$id.'.css';

require_once('header.php');
?>
<!-- each slide is a section; everything else is automated in the support PHP -->
<section>
	<h1>Agenda</h1>
	<ol>
		<li>Revisiting The Family Impact Lens</li>
		<li>Using the Family Impact Lens</li>
		<li>Family Impact Analysis</li>
	</ol>
</section>
<section>
	<section>
		<h2>Revisiting the Family Impact Lens</h2>
	</section>
	<section>
		<h3>What does the Family Impact Lens Consider?</h3>
		<h4>For Policies or Programs that are enacted or established:</h4>
		<ol>
			<li>How families are affected by an issue</li>
			<li>If families contribute to an issue</li>
			<li>Whether involving families in the response would result in more effective and efficient policies and programs</li>
		</ol>
	</section>
	<section>
		<h3>What does the Family Impact Lens Consider?</h3>
		<h4>When policies or programs are implemented:</h4>
		<ol>
			<li>Whether familiy dignitiy is preserved, and whether families are treated with respect</li>
			<li>If families are able to make decisions based on accessible and available information</li>
			<li>If families have choice regarding available services and the extent of their participation</li>
			<li>If families have involvement in professional collaboration, and have decisionmaking roles for family goals</li>
		</ol>
	</section>
	<section>
		<h3>Reminder</h3>
		<p style='color: yellow; text-align: center;'>The Family Impact Lens is a tool to help us critically think about policy. It does not, by itself, determine whether policies are good or bad, or whether they should be implemented.</p>
	</section>
	<section>
		<h3>How do we use this information?</h3>
		<p>We can use the family impact lens to generate useful information when consider Program and Policy:
		<ul>
			<li class='fragment'>Eligibility</li>
			<li class='fragment'>Effectiveness</li>
			<li class='fragment'>Efficiency</li>
			<li class='fragment'>Design</li>
			<li class='fragment'>Implementation</li>
			<li class='fragment'>Targets of Interest</li>
		</ul>
	</section>
</section>
<section>
	<section>
		<h2>Using the Family Impact Lens</h2>
	</section>
	<section>
		<h3>Welfare Reform</h3>
		<p>Refresher: What is TANF? How did it change welfare in the US?</p>
	</section>
	<section>
		<h3>What were the Economic and Social Contexts for Reforming Welfare Policy?</h3>
		<p class='fragment'>Increases in income inequality - GDP increases, but median income stagnated</p>
		<p class='fragment'>Reductions in male wages and labor force participation</p>
		<p class='fragment'>Increased employment of women</p>
		<p class='fragment'>Increased rates of nonmarital childbirth (33%)</p>
		<p class='fragment'>Elevated divorce rates</p>
		<p class='fragment'>Reduced rates and levels of family savings, and increased debts</p>
		<p class='fragment'>Negative stereotypes and attitudes towards the poor</p>
		<p class='fragment'>Aid to Families with Dependent Children (AFDC) seen as incentivizing avoiding marriage, working less, and nonmarital childbirth</p>
	</section>
	<section>
		<h3>What effects did Welfare Reform have on Families?</h3>
		<p class='fragment'>TANF created time-limited benefits and expectation of work</p>
		<p class='fragment'>Reduced use of program by eligible families (86% &rarr; 48%)</p>
		<p class='fragment'>Declines in child poverty in single-parent households</p>
		<p class='fragment'>Reduced poverty in female-headed households</p>
		<p class='fragment'>TANF creates incentives for marriage, work, and marital childbirth</p>
	</section>
	<section>
		<h3>How did Family Considerations Affect the Reform of Welfare Policy?</h3>
		<p class='fragment'>Shifting between focus on remediation and prevention</p>
		<p class='fragment'>Shift in focus from individual to family</p>
		<p class='fragment'>By funding to family-oriented programs (childcare), overall spending was reduced</p>
		<p class='fragment'>Greater attention to integration of services</p>
	</section>
	<section>
		<h3>Juvenile Crime</h3>
		<p>The cost of juvenile incarceration in the US is over $\$5$ Billion annually (2005)</p>
		<p>The estimated cost of juvenile delinquency in the US, considering incarceration, lost wages, lost future earnings, lost tax revenue, and other factors is around $\$21$ Billion annually (2011)</p>
		<p>For comparison, the cost of TANF is only around $\$14$ Billion annually (2008)</p>
	</section>
	<section>
		<h3>What Role Do Families Play in Creating Juvenile Delinquents?</h3>
		<p class='fragment'>The best predictor of juvenile crime is ineffective parenting, esp. parenting emphasizing coercion and poor parental monitoring</p>
	</section>
	<section>
		<h3>What Role Can Families Play in Policy to Reduce Juvenile Crime?</h3>
		<p class='fragment'>Prevention is cheaper and more effective than Remediation, as demonstrated by cost-benefit analyses</p>
		<p class='fragment'>Olds' Nurse Family Partnership - nurses visit low-income single pregnant mothers prenatally and for the first 2 years of life; reduced children's rates of nonmarital pregnancy (33%), juvenile crime (33%), and Medicaid use (60%)</p>
		<p class='fragment'>Adolescent Diversion Programs - addresses behavioral skills in home with parents, builds comunity relationships, advocates for youth rights; reduces 12 month recidivism rates and rates of police contact</p>
		<p class='fragment'>Multisystemic Therapy - intensive, ecologically focused family therapy for juvenile offenders emphasizing parenting practices, prosocial peers, and social support; reduces re-arrest rates (25-75%), out-of-home placements (47-64%), and enhances family functioning</p>
		<p class='fragment'>In general, families socialize youth and promote resiliency, so enhancing family functioning can diminish juvenile delinquency</p>
	</section>
	<section>
		<h3>How Can Policymakers Design Family-Focused Policy to Reduce Juvenile Crime?</h3>
		<p class='fragment'>Policymakers need to attend to how programs are implemented; bad implementation kills effectiveness (FFT)</p>
		<p class='fragment'>Knowing the etiology of juvenile crime, policymakers can fund programs that emphasize familya dn community components over individualistic components</p>
	</section>
	<section>
		<h3>Healthcare Policy</h3>
		<p>&#8220;Family is the hidden agent of health activity in the United States&#8221; - William Doherty</p>
	</section>
	<section>
		<h3>What Role Do Families Play in Promoting Health and Reducing Risk?</h3>
		<p class='fragment'>Families teach healthy and unhealthy habits (exercise, treatment compliance, etc.)</p>
		<p class='fragment'>Families control food access and eating behaviors of children</p>
		<p class='fragment'>Families teach values and expectations around sexual behavior and substance use</p>
	</section>
	<section>
		<h3>What Role Do Families Play in Reducing Vulnerability to Disease Onset or Relapse?</h3>
		<p class='fragment'>Social relationships and social support shape ongoing behaviors and habits</p>
		<p class='fragment'>Marriage reduces risk of many illnesses, and marital relationships can bolster treatment success</p>
		<p class='fragment'>Parent-child relationships can affect sensitivity to illness and recovery success</p>
	</section>
	<section>
		<h3>What Role Do Families Play in Appraising Illness?</h3>
		<p class='fragment'>Families are the first triage</p>
		<p class='fragment'>Families tend to have the most accurate medical history information</p>
		<p class='fragment'>Families create internal narratives and traditions about treatment, disease, and values</p>
	</section>
	<section>
		<h3>What Role Do Families Play in Acute Response to a Serious Illness?</h3>
		<p class='fragment'>Individual illness affects the family physically, emotionally, and economically</p>
		<p class='fragment'>Family members need information when loved ones are acutely ill, for self-care and for caretaking</p>
	</section>
	<section>
		<h3>What Role Do Families Play in Adapting to Illness and Aiding in Recovery?</h3>
		<p class='fragment'>Families can bolster or disrupt compliance with treatment plans and recovery</p>
		<p class='fragment'>Family psychosocial interventions have been shown to improve management and recovery of chronic illness</p>
		<p class='fragment'>Reducing parent anxiety through psychoeducation can improve child and infant outcomes</p>
	</section>
	<section>
		<h3>What Role Can Families Play in Healthcare Policy?</h3>
		<p class='fragment'>By recognizing and taping families as a source of support, the quality of healthcare can be improved, and treatment can be more effective</p>
	</section>
</section>
<section>
	<section>
		<h2>Family Impact Analysis</h2>
	</section>
	<section>
		<h3>Definition</h3>
		<p>A family impact analysis is a formal, structured report that applies the family impact lens to an identified policy problem.</p>
	</section>
	<section>
		<h3>Structure</h3>
		<ul>
			<li>Abstract</li>
			<li>Introduction &amp; Background</li>
			<li>Analysis</li>
			<li>Conclusions and Policy Implications</li>
		</ul>
	</section>
	<section>
		<h3>Carrying it Out</h3>
		<ol>
			<li>Select the Policy to be Analyzed</li>
			<li>Determine Which Family Types will be Affected
				<ul>
					<li>particular family structures?</li>
					<li>particular life cycle stages?</li>
					<li>particular incomes or education backgrounds?</li>
					<li>particular racial, ethnic, cultural, religious, or geographic backgrounds?</li>
					<li>particular special needs?</li>
					<li>families that aren't legally recognized?</li>
				</ul></li>
			<li>Use the <a href='https://www.purdue.edu/hhs/hdfs/fii/wp-content/uploads/2015/06/fi_checklist_0712.pdf'>Family Impact Checklist</a>, and conduct the analysis</li>
			<li>Disseminate the Results</li>
		</ol>
	</section>
	<section>
		<h3>The Family Impact Checklist - 5 Principles</h3>
		<p>When conducting the family impact analysis, use the Family Impact Checklist. This consists of five principles, with associated questions, that help us critically evaluate the ways inwhich family scontribute to, are affected by, and can be useful in solving policy issues.</p>
		<ol>
			<li>Family Responsibility</li>
			<li>Family Stability</li>
			<li>Family Relationships</li>
			<li>Family Diversity</li>
			<li>Family Engagement</li>
		</ol>
	</section>
	<section>
		<h3>Family Responsibility</h3>
		<p>Policies and programs should aim to support and empower the functions that families perform for society &ndash; family formation, partner relationships, economic support, childrearing, and caregiving. Substituting for the functioning of families should come only as a last resort.</p>
		<p>How well does the policy, program, or practice:</p>
		<ul>
			<li class='fragment'>help families build capacity to fulfill their functions and avoid taking over family responsibilities unless absolutely necessary?</li>
			<li class='fragment'>set realistic expectations for families to assume financial and/or caregiving responsibilities for dependent, seriously ill, or frail family members depending on their family structure, resources, and life challenges?</li>
			<li class='fragment'>address root causes of financial insecurity such as high child support debt, low iteracy, low wages, and unemployment?</li>
			<li class='fragment'>affect the ability of families to balance time committments to work, family, and community?</li>
		</ul>
	</section>
	<section>
		<h3>Family Stability</h3>
		<p>Whenever possible, policies and programs should encourage and reinforce couple, marital, parental, and family commitment and stability, especially when children are involved. Intervention in family membership and living arrangements is usually justified only to protect family members from serious harm or at the request of the family itself.</p>
		<p>How well does the policy, program, or practice:</p>
		<ul>
			<li class='fragment'>strengthen commitment to couple, marital, parental, and family obligations, and allocate resources to help keep the marriage or family together when this is the appropriate goal?</li>
			<li class='fragment'>help families avoid problems before they become serious crises or chronic situations that erode family structure and function? </li>
			<li class='fragment'>balance the safety and well-being of individuals with the rights and responsibilities of other family members and the integrity of the family as a whole? </li>
			<li class='fragment'>provide clear and reasonable guidelines for when nonfamily members are permitted to intervene and make decisions on behalf of the family (e.g., removal of a child or adult from the family)?</li>
		</ul>
	</section>
	<section>
		<h3>Family Stability (continued)</h3>
		<p>How well does the policy, program, or practice:</p>
		<ul>
			<li class='fragment'>help families maintain regular routines when undergoing stressful conditions or at times of transition? </li>
			<li class='fragment'>recognize that major changes in family relationships such as aging, divorce, or adoption are processes that extend over time and require continuing support and attention?</li>
			<li class='fragment'>provide support to all types of families involved in the issue (e.g., for adoption, consider adoptive, birth, and foster parents; for remarried families, consider birth parents, stepparents, residential and nonresidential parents, etc.)? </li>
		</ul>
	</section>
	<section>
		<h3>Family Relationships</h3>
		<p> Policies and programs must recognize the strength and persistence of family ties, whether positive or negative, and seek to create and sustain strong couple, marital, and parental relationships. </p>
		<p>How well does the policy, program, or practice:</p>
		<ul>
			<li class='fragment'>recognize that individuals’ development and well-being are profoundly affected by the quality of their relationships with close family members and family members’ relationships with each other?</li>
			<li class='fragment'>involve couples, immediate family members, and extended family when appropriate in working to resolve problems, with a focus on improving family relationships? </li>
			<li class='fragment'>assess and balance the competing needs, rights, and interests of various family members?</li>
			<li class='fragment'>take steps to prevent family abuse, violence, or neglect? </li>
		</ul>
	</section>
	<section>
		<h3>Family Relationships (Continued)</h3>
		<p>How well does the policy, program, or practice:</p>
		<ul>
			<li class='fragment'>acknowledge how interventions and life events can affect family dynamics and, when appropriate, support the need for balancing change and stability in family roles, rules, and leadership depending upon individual expectations, cultural norms, family stress, and stage of family life? </li>
			<li class='fragment'>provide the knowledge, communication skills, conflict resolution strategies, and problemsolving abilities needed for healthy couple, marital, parental, and family relationships or link families to information and education sources? </li>
		</ul>
	</section>
	<section>
		<h3>Family Diversity</h3>
		<p> Policies and programs can have varied effects on different types of families. Policies and programs must acknowledge and respect the diversity of family life and not discriminate against or penalize families solely based on their cultural, racial, or ethnic background; economic situation; family structure; geographic locale; presence of special needs; religious affiliation; or stage of life.</p>
		<p>How well does the policy, program, or practice:</p>
		<ul>
			<li class='fragment'>identify and respect the different attitudes, behaviors, and values of families from various stages of life; family structures; and cultural, economic, geographic, racial/ethnic, and religious backgrounds? </li>
			<li class='fragment'>respect cultural and religious routines and rituals observed by families within the confines of the law? </li>
			<li class='fragment'>recognize the complexity and responsibilities involved in caring for and coordinating services for family members with special needs (e.g., cognitive, emotional, physical)? </li>
		</ul>
	</section>
	<section>
		<h3>Family Diversity (continued)</h3>
		
		<p>How well does the policy, program, or practice:</p>
		<ul>
			<li class='fragment'>ensure the accessibility and quality of programs and services for culturally, economically, geographically, racially/ethnically, and religiously diverse families? </li>
			<li class='fragment'>work to ensure that operational philosophies and procedures are culturally responsive and that program staff are culturally competent? </li>
			<li class='fragment'>acknowledge and try to address root causes rather than symptoms of the issue or problem (e.g., economic, institutional, political, social/psychological causes)?  </li>
		</ul>
	</section>	
	<section>
		<h3>Family Engagement</h3>
		<p>Policies and programs must encourage partnerships between professionals and families. Organizational culture, policy, and practice should include relational and participatory practices that preserve family dignity and respect family autonomy.</p>
		<p>How well does the policy, program, or practice:</p>
		<ul>
			<li class='fragment'>provide full information and a range of choices to families, recognizing that the length and intensity of services may vary according to family needs? </li>
			<li class='fragment'>train and encourage professionals to work in collaboration with families, to allow families to make their own decisions (within the confines of the law), and to respect their choices? </li>
			<li class='fragment'>involve family members, particularly from marginalized families, in policy and  program development, implementation, and evaluation? </li>
			<li class='fragment'>affirm and build upon the existing and potential strengths of families, even when families are challenged by adversity? </li>
		</ul>
	</section>
	<section>
		<h3>Family Engagement (continued)</h3>
		<p>How well does the policy, program, or practice:</p>
		<ul>
			<li class='fragment'>make flexible program options available and easily accessible through co-location, coordinated application and reimbursement procedures, and collaboration across agencies, institutions, and disciplines? </li>
			<li class='fragment'>establish a coordinated policy and service system that allows localities and service providers to combine resources from various, diverse funding streams? </li>
			<li class='fragment'>acknowledge that the engagement of families, especially those with limited resources, may require emotional, informational, and instrumental supports (e.g., child care, financial stipends, transportation)?  </li>
			<li class='fragment'>connect families to community resources and help them be responsible consumers, coordinators, and managers of these resources? </li>
		</ul>
	</section>
	<section>
		<h3>Family Engagement (continued)</h3>
		<p>How well does the policy, program, or practice:</p>
		<ul>
			<li class='fragment'>build on social supports that are essential to families’ lives (e.g., friends; family-to-family support; community, neighborhood, volunteer, and faith-based organizations)? </li>
			<li class='fragment'>consider the whole family (even if it is outside the scope of services) and recognize how family decisions and participation may depend upon competing needs of different family members? </li>
		</ul>
	</section>	
	<section>
		<h3>Examples</h3>
		<ul>
			<li><a href='https://www.purdue.edu/hhs/hdfs/fii/wp-content/uploads/2015/06/fia_analyses_fpfmla.pdf'>FMLA</a></li>
			<li><a href='https://www.purdue.edu/hhs/hdfs/fii/wp-content/uploads/2015/06/fia_analyses_vasptfil.pdf'>After School Programs</a></li>
			<li><a href='https://www.purdue.edu/hhs/hdfs/fii/wp-content/uploads/2015/06/fia_analyses_fpmhp.pdf'>Mental Health Parity Act</a></li>
			<li><a href='https://www.purdue.edu/hhs/hdfs/fii/wp-content/uploads/2015/07/s_infis16report.pdf'>ACA (presentation format)</a></li>
		</ul>
	</section>
	<section>
		<h3>Practice</h3>
		<p>To practice, try applying the family impact lens to the following policies:</p>
		<ul>
			<li>SNAP</li>
			<li>No Child Left Behind</li>
			<li>EITC</li>
		</ul>
	</section>
</section>
<section>
	<h1>Next Up:</h1>
	<h2><a href="./?lesson=paradox">The Theory of Paradox</a></h2>
</section>
