<?php
# toggle whether page is live

$live = True;

require_once('redirect.php');

$title = 'Welcome to CHD4615';
$id = 'night';
$theme = 'css/theme/'.$id.'.css';


## Other Available Themes
## $theme = 'http://pablocubi.co/mozreveal/css/theme/one-mozilla.css';
## Predefined Themes
## beige, blood, default, moon, night, serif, simple, sky, sky-jeopardy, solarized
## $id = 'beige';
## $theme = 'css/theme/'.$id.'.css';

require_once('header.php');
?>
<!-- each slide is a section; everything else is automated in the support PHP -->
<section>
	<h1>Agenda</h1>
	<ol>
		<li>Introductions</li>
		<li>Highlights from the Syllabus</li>
		<li>Set Course Expectations and Values</li>
		<li>First Day Attendance and Administrative Issues</li>
	</ol>
</section>
<section>
	<h1>Class Info</h1>
	<ul>
		<li>Where: DHA 103 (The room you're in right now)</li>
		<li>When: Tuesday &amp; Thursday 2:00 to 3:10PM</li>
	</ul>
</section>
<section>
	<section>
	<h1>Instructor Info</h1>
	</section>
	<section>
		<h2>Instructor</h2>
		<p>Andrew Benesh, MFT </p>
		<h2>Office</h2>
		<p>Sandels 228</p>
		<h2> Email</h2>
		<p><a  href="mailto:asb12f@my.fsu.edu">asb12f@my.fsu.edu</a></p>
		<p>Please include CHD4615 in the title of your email, and use your FSU email address.</p>
	</section>
	<section>
		<h2>Teaching Assistant</h2>
		<p>Leah Pompey</p>
		<h2>Office</h2>
		<p>Sandels 211</p>
		<h2> Email</h2>
		<p><a href="mailto:lap14e@my.fsu.edu">lap14e@my.fsu.edu</a></p>
		<p>Please include CHD4615 in the title of your email, and use your FSU email address.</p>
	</section>
	<section>
	<h2>Instructor Office Hours</h2>
		<ul>
			<li> Tuesday &amp; Thursday 12:30 to 1:30PM</li>
			<li> Other times by appointment </li>
		</ul>
	</section>
</section>
<section>
	<section>
		<h1>Instructor Background</h1>
		<h2>(or, &#8220;Is this Guy Qualified?&#8221;)</h2>
	</section>

	<section>
		<h2>I'm not a:</h2>
			<h3 class="fragment">Lawyer</h3>
			<h3 class="fragment">Judge</h3>
			<h3 class="fragment">Politician</h3>
			<h3 class="fragment">Lobbyist</h3>
			<h3 class="fragment">President, CEO, Director, or Dean</h3>
	</section>

	<section>
		<h2>But I am a:</h2>
			<h3 class="fragment"><a href='https://www.tmh.org/services/mental-health-substance-abuse'>Mental Health Provider</a></h3>
			<h3 class="fragment">Educator</h3>
			<h3 class="fragment"><a href='http://www.redcross.org/fl/tallahassee'>Disaster Responder</a></h3>
			<h3 class="fragment"><a href='http://www.camptobelong-ga.org/'>Board Member</a></h3>
			<h3 class="fragment">Advocate</h3>
			<h3 class="fragment">Family Member</h3>
		<div class="fragment">
		<p>And to do these well, I have to be able to understand and interact with the world of family policy.</p>
		<p>If you still have your doubts, you can always check my <a href="http://benesh.info/?page=cv">CV</a> 
			or <a href="https://www.linkedin.com/pub/andrew-benesh/59/573/687">Linkedin</a>.</p>
		</div>
	</section>
</section>

<section>
	<section>
		<h1>What This Course is About</h1>
		<h2>(follow along in your syllabus!)</h2>
	</section>
	<section>
		<h2>Course Description</h2>
		<p style="font-size:120%;">
		This course will provide an overview of theory and implementation of public policies relating to children and families at the state and federal levels.</p>
		<p>	Students will explore ways that families contribute to and are affected by social problems, and how families can be involved in policy solutions. </p>
		<p>Students will learn about roles professionals can play in building and interacting with family policy.		
		</p>
	</section>

	<section>
		<h2>Course Objectives</h2>
		<ol style="font-size:80%;">
			<li class='fragment'>Critically examine theoretical orientations for conceptualizing family policy and for connecting research and policy making</li>
			<li class='fragment'>Demonstrate an understanding of how policy is influenced by demographic changes, values, attitudes, and perceptions of the well-being of children and families</li>
			<li class='fragment'>Apply a family perspective to policy analysis by assessing current policy issues in terms of 
				their sensitivity to and supportiveness of diverse contemporary families </li>
			<li class='fragment'>Describe the roles professionals can play in building family policies that support families across the life cycle</li>
			<li class='fragment'>Discuss how these roles can be implemented using either an educational or advocacy approach </li>
			<li class='fragment'>Apply critical thinking skills in developing and expressing logical arguments to policymakers, practitioners,and the public </li>
		</ol>
	</section>

	<section>
		<h2>What All That Means</h2>
		<p>In this course, we're going to talk about policy issues that affect families and children (shocking, I know!).</p>
		<p>More importantly, we'll learn about ways to analyze policy, advocate for differing policies, and how to be aware of policy in your professional roles.</p>
	</section>
</section>

<section>
	<section>
	<h1>Course Materials</h1>
	</section>

	<section>
	<h2>Textbooks</h2>
	<p>If you've had my classes before, you know I prefer to use free, open source textbooks.</p>
	<p>Unfortunately, there are no good open source textbooks on this topic area. </p>
	<p>So you will need to buy the textbook.</p>
	</section>

	<section>
	<h2>The Book</h2>
	
	<table class="reveal">
		<tr>
			<th>Title:</th>
			<td style="border: none;">Family Policy Matters, 3rd Edition</td>
			<td style="border: none;" rowspan="3"><img src="../images/book.jpg" style="vertical-align:top;" alt="Family Policy Matters" /></td>
		</tr>
		<tr>
			<th>Author:</th>
			<td style="border: none;">Karen Bogenschneider</td>
		</tr>
		<tr>
			<th>Publisher:</th>
			<td style="border: none;">Routledge</td>
		</tr>
		<tr>
			<th>Links:</th>
			<td style="border: none;"><a href="http://www.amazon.com/Family-Policy-Matters-Policymaking-Professionals/dp/0415844487">Amazon</a>, <a href="http://www.barnesandnoble.com/w/family-policy-matters-karen-bogenschneider/1117508913?ean=9780415844482">Barnes &amp; Noble</a></td>
		</tr>
	</table>
	<br />
	<p><small>If you do use Amazon, consider using <a href="http://smile.amazon.com">Amazon Smile</a>, so your purchase can contribute to good causes.</small></p>
	</section>

	<section>
	<h2>A Little More on the Book</h2>
	<p>You will need to read for this class; there are questions on the test that are only from the lecture.</p>
	<p>Read before coming to class, and bring your book with you. This will help you be prepared for the in-class activities.</p>
	</section>

	
	<section>
	<h2>Consider getting an APA manual...</h2>
	<p>Papers in this course use APA 6th Edition format, so you may want to get the manual.</p>
	<p>You can buy this relatively cheaply, or get a copy from the library.</p>
	</section>

</section>

<section>
	<section>
		<h1>Course Format</h1>
	</section>
	<section>
		<h2>Lectures</h2>
		<p>
		Class time is primarily devoted to lecture, but will include periodic discussions and related activities. </p>
		<p>Throughout the semester, class time may be used to view videos and documentaries, address current issues, 
		or discuss recent news or magazine articles relevant to course materials.</p>
		<p>Students are <span style='color:lime;'>strongly encouraged</span> to locate and bring in relevant articles and news.
		</p>
	</section>
	<section>
		<h2>General Attendance Policy</h2>
		<p>
		Throughout the course, students will be asked to complete in-class quizzes, surveys, and in-class activities, some of which will be collected for attendance. 
		There are a total of 20 attendance points available.</p>
		
		<p class='fragment'>Final attendance points will be determined by the proportion of points students earn on these activities (i.e., if a student earns 85% of the available points on attendance activities, their attendance score will be: 20 x 0.85 = 17). Attendance scores will be rounded down to the nearest whole point value. </p>
		
		<p class='fragment'>A the majority of test questions will come from lecture and class discussions, which will cover topics beyond those presented in the book; therefore missing class is likely to have a negative effect on student grades. 
		</p>
		<div class="fragment">
		</div>
	</section>
</section>
<section>
	<section>
		<h1>Assignments</h1>
		<h2>(where your grade comes from)</h2>
	</section>				
	<section>
		<h2>Final Grades</h2>
		<p>
			There are a total of 400 points available to you in this course. 
			Your final grade will be determined based on the total number of points you earn, using the following scale:
		</p>
		<table class="reveal">
			<tr>
				<th>Grade</th><th>Points</th><th>Grade</th><th>Points</th>
			</tr><tr>
				<th>A</th><td style="border:none;">&ge; 368 </td><th> C </th><td  style="border:none;"> 288-311 </td>
			</tr><tr>
				<th>A-</th><td style="border:none;">360-367 </td><th> C- </th><td style="border:none;"> 280-287 </td>
			</tr><tr>
				<th>B+</th><td style="border:none;">352-359</td><th> D+ </th><td  style="border:none;"> 272-279 </td>
			</tr><tr>
				<th>B</th><td style="border:none;">328-351</td><th> D </th><td  style="border:none;"> 248-271</td>
			</tr><tr>
				<th>B-</th><td style="border:none;">320-327</td><th> D- </th><td  style="border:none;"> 240-247 </td>
			</tr><tr>
				<th>C+</th><td style="border:none;">312-319</td><th> F </th><td  style="border:none;"> &lt; 240 </td>
		</table>
	</section>
	<section>
		<h2>Attendance</h2>
		<p>As discussed previously</p>
	</section>
	<section>
		<h2>Individual Papers</h2>
		<ol>
			<li class='fragment'>A Disagreeable Discussion - 20pts - 1 Page - 1/21 by 5PM</li>
			<li class='fragment'>My Own Worst Enemy - 20pts - 2 Pages - 2/11 by 5PM</li>
			<li class='fragment'>Meet the Press - 40pts - 2 Pages - 2/25 by 5PM</li>
			<li class='fragment'>Family Impact Analysis - 60pts - Max 4 Pages - 4/7 by 5PM</li>
		</ol>
		<p style="padding-top: 1em;" class='fragment'><strong>A note about feedback:</strong> I'm available to provide feedback and suggestions on early drafts of your written work, and to answer any questions you may have. </p>
		<p class='fragment'><strong>However</strong>, due to the size of the class and nature of the papers, I will not be able to provide feedback
		on drafts submitted less than one (1) week before the assignment is due. Please plan accordingly. </p>
	</section>

	<section>
		<h2>Advocacy Project</h2>

		<p>In groups of <span style='color:gold;'>no more than six</span>, students will identify a social problem, analyze it using the three worldviews, and develop a potential policy solution.</p>
		<p class='fragment'>Students will advocate for their policy solution by developing <span style='color:lime;'>a one-page policy memo</span>, and <span style='color:orange;'>a media presentation</span> in the form of a youtube video or audio podcast at least 10 minutes length.</p>
		<p class='fragment'>Finally, students will provide feedback to other groups in the form of an <span style='color:yellow;'>online review</span> in Blackboard</p>
		<ol class='fragment'>
			<li><span style='color:lime;'>Policy Memo</span> - 20pts - 1 Page - 4/14 5PM</li>
			<li><span style='color:orange;'>Advocacy or Education Media</span> - 30pts - 10 mins - 4/14 5PM</li>
			<li><span style='color:yellow;'>Consumer Response</span> - 10pts - 4/21 5PM</li>
		</ol>
	</section>
	<section>
		<h2>Tests</h2>
			<ol>
				<li>Exam 1 - 60 pts - 2/4/16</li>
				<li>Exam 2 - 60 pts - 3/3/16</li>
				<li>Exam 3 - 60pts - 4/28/16<span class='fragment'>, 7:30AM <small>(sorry!)</small></span></li>
			<ol>
	</section>
	<section>
		<h2>Extra Credit!</h2>
			<p>Extra credit must be submitted by Friday, March 4, at 5PM.</p>
			<ol>
				<li>Letter - 3-5 pts</li>
				<li>Wiki - 5 pts</li>
			<ol>
	</section>
</section>

<section>
	<section>
		<h1>Course Policies</h1>
	</section>
	<section>
		<h2>Missed Assignments and Tests</h2>
		<p>You need to provide university approved documentation as soon as possible. <span style="color:yellow;">This must state you could not attend class on the specified date(s); a note saying only that you were seen by a doctor is not sufficient.</span></p>
	</section>
	<section>
		<h2>Academic Honor Policy</h2>
		<p style="text-align:left;">The Florida State University Academic Honor Policy outlines the University's expectations for the integrity of students academic work, the procedures for resolving alleged violations of those expectations, and the rights and responsibilities of students and faculty members throughout the process. Students are responsible for reading the Academic Honor Policy and for living up to their pledge to . . . be honest and truthful and . . . [to] strive for personal and institutional integrity at Florida State University. (Florida State University Academic Honor Policy, found at <a href="http://fds.fsu.edu/Academics/Academic-Honor-Policy">http://fds.fsu.edu/Academics/Academic-Honor-Policy</a>).</p>				<br />
		<p class="fragment fade-in">Seriously, don't cheat or plagiarize.</p>
	</section>
	<section>
		<h2>Americans with Disabilites Act</h2>
		<p style="text-align:left;">Students with disabilities needing academic accommodation should: (1) register with and provide documentation to the Student Disability Resource Center; and (2) bring a letter to the instructor indicating the need for accommodation and what type. This should be done during the first week of class. This syllabus and other class materials are available in alternative format upon request. </p>
	</section>
	<section>
		<h2>Americans with Disabilities Act (continued)</h2>
		<p>For more information about services available to FSU students with disabilities, contact the:</p>
		<p>Student Disability Resource Center</p>
		<p>874 Traditions Way</p>
		<p>108 Student Services Building</p>
		<p>Florida State University</p>
		<p>Tallahassee, FL 32306-4167</p>
		<p>(850) 644-9566 (voice)</p>
		<p>(850) 644-8504 (TDD)</p>
		<p><a href="mailto:sdrc@admin.fsu.edu">sdrc@admin.fsu.edu</a></p>
		<p><a href="http://www.disabilityresourcecenter.fsu.edu">http://www.disabilityresourcecenter.fsu.edu</a></p>
	</section>
	<section>
		<h2>Deviation from the Syllabus</h2>
		<p style="text-align:left;">Except for changes that substantially affect implementation of the evaluation(grading) statement, this syllabus is a guide for the course and is subject to change with advance notice. </p>
	</section>
</section>

<section>
	<section>
		<h1>Values and Expectations</h1>
	</section>
	<section>
		<h2 style="color:red;">Warning!</h2>
			<p>
			This is a policy course; it is by definition political.
			</p><p class="fragment">
			This means we'll be discussing issues that can be contentious, and  will likely touch on 
			ideas and values that you hold dear.</p><p class="fragment">
			It is likely other people may disagree with your views or values.</p><p class="fragment">
			Despite disagreements, you are expected to remain <span style="color:green; font-weight: 900;">respectful </span> and <span style="color:green; font-weight: 900;">civil</span>. 
			Abusive or disrespectful behavior may result in <span style="color:red; font-weight: 900;">removal from the course</span>.</p>		
		<br />
			<p class="fragment">Also, please know that your personal views will not affect your grade in the course, and you are not expected to adopt
			the instructor's views; in fact, I make strong efforts to avoid sharing my own views in this course.</p>
	</section>
</section>
<section>
	<section>
		<h1>1st Day Attendance</h1>
	</section>
	<section>
	<!-- version for smaller classes
		<h2>On a notecard, please write:</h2>
		<ul>
			<li>Your Full Name </li>
			<li>What you prefer to be called </li>
			<li>Career Goal / Next Steps </li>
			<li>2-3 Issues, Policies, Programs, or Causes that Interest You</li>
			<li>If you've ever taken a policy, law, or advocacy course (if so, please specify)</li>
		</ul>
	-->
		<h3>Part I: Complete the Attendance Sheet</h3>
		<h3 class='fragment'>Part II: Complete the Student Background Survey in Blackboard</h3>
		<h3 class='fragment'>Part III: Have a Great Day!</h3>
	</section>
</section>