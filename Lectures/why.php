<?php
# toggle whether page is live
$live = True;
require_once('redirect.php');

$title = 'Why Family Policy is Important';
$id = 'night';
$theme = 'css/theme/'.$id.'.css';


## Other Available Themes
## $theme = 'http://pablocubi.co/mozreveal/css/theme/one-mozilla.css';
## Predefined Themes
## beige, blood, default, moon, night, serif, simple, sky, sky-jeopardy, solarized
## $id = 'beige';
## $theme = 'css/theme/'.$id.'.css';

require_once('header.php');
?>
<!-- each slide is a section; everything else is automated in the support PHP -->
<section>
	<h1>Agenda</h1>
	<ol>
		<li>Why am I taking Family Policy?</li>
		<li>Focusing on Family Policy Making</li>
	</ol>
</section>
<section>
	<section>
		<h1>Why am I taking Family Policy?</h2>
	</section>
	<section>
		<h2>I'm not a Politician, so how is this Relevant?</h2>
		<ul>
			<li class="fragment">People Want Change in the World</li>
			<li class="fragment">Isn't Research Enough?</li>
			<li class="fragment">If you print it, they will come... right?</li>
		</ul>
	</section>
	<section>
		<h2>Are families public or private?</h2>
		<ul>
			<li class="fragment">Public vs Private Realms</li>
			<li class="fragment">Public events, like employment benefits, can affect the private lives of families.</li>
			<li class="fragment">Private activities of the family, like instilling values in children and supporting the sick, have huge impacts on the public sphere.</li>
		</ul>
	</section>
	<section>
		<h2>Why does it matter?</h2>
		<p>Family Policy enables families to function in society by facilitating:</p>
		<ul>
			
			<li class="fragment">Childcare</li>
			<li class="fragment">Education</li>
			<li class="fragment">Job Training</li>
			<li class="fragment">Health Care</li>
			<li class="fragment">Poverty Eradication</li>
			<li class="fragment">Child Protection</li>
			<li class="fragment">Nutritional Support</li>
			<li class="fragment">Economic Support</li>
		</ul>
	</section>
	<section>
		<h3>Some Notable Family Policies</h3>
		<table class="reveal">
			<tr>
				<th style="border:1px solid white;"><a href="http://www.benefits.gov/benefits/benefit-details/607">State Children's Health Insurance Program (SCHIP)</a></th>
				<td class="fragment" style="border:1px solid white;">Provides health insurance coverage for children whose families are not eligible for Medicaid, but who cannot afford insurance.</td>
			</tr><tr>
				<th style="border:1px solid white;"><a href="http://www.cbpp.org/research/policy-basics-the-earned-income-tax-credit">Earned Income Tax Credit (EITC)</a></th>
				<td class="fragment" style="border:1px solid white;">A federal tax credit for low- and moderate-income working people.  It encourages and rewards work as well as offsets federal payroll and income taxes.</td>
			</tr><tr>
			<th style="border:1px solid white;"><a href="http://www.fns.usda.gov/snap/supplemental-nutrition-assistance-program-snap">Supplemental Nutrition Assistance Program (SNAP)</a></th>
				<td class="fragment" style="border:1px solid white;">Formerly food stamps, SNAP offers nutrition assistance to millions of eligible, low-income individuals and families and provides economic benefits to communities. SNAP is the largest program in the domestic hunger safety net. </td>
			</tr><tr>
		</table>
	</section>	
	<section>
		<h3>Some Notable Family Policies</h3>
		<table class="reveal">
			<tr>
				<th style="border:1px solid white;"><a href="http://www.acf.hhs.gov/programs/ohs">Head Start</a></th>
				<td class="fragment" style="border:1px solid white;">Head Start promotes school readiness of children under 5 from low-income families through education, health, social and other services</td>
			</tr><tr>
				<th style="border:1px solid white;"><a href="http://www.fns.usda.gov/wic/women-infants-and-children-wic">Special Supplemental Nutrition Program for Women, Infants and Children (WIC)</a></th>
				<td class="fragment" style="border:1px solid white;">WIC provides Federal grants to States for supplemental foods, health care referrals, and nutrition education for low-income pregnant, breastfeeding, and non-breastfeeding postpartum women, and to infants and children up to age five who are found to be at nutritional risk.</td>
			</tr><tr>
			<th style="border:1px solid white;"><a href="http://www.medicaid.gov/">Medicaid</a></th>
				<td class="fragment" style="border:1px solid white;">A social health care program for families and individuals with low income and limited resources. Medicaid is the largest source of funding for medical and health-related services for people with low income in the United States. </td>
			</tr><tr>
		</table>
	</section>
	<section>
		<h2>How can I make a difference?</h2>
		<p class="fragment">Students often feel politically impotent; and express feelings that they are not responsible for creating political change.</p>
		<p class="fragment">Students sometimes feel that only high powered lobbyists, interest groups, or politicians can create change.</p>
		<p style="color:gold; font-weight: 900;"  class="fragment">This simply isn't true; grassroots and student led political action have more power today than at any point in history.</p>
	</section>
	<section>
		<h3>Notable Grassroots Movements</h3>
		<table class="reveal">
			<tr>
				<th style="border-bottom:1px solid white; ">Movement</th>
				<th style="border-bottom:1px solid white; ">Effects</th>
			</tr><tr>
				<th><a href="http://occupywallst.org/">Occupy Wall Street (2010 - 2011)</a></th>
				<td class="fragment" style="border: none; border-left:1px solid white;">Re-vitalized national and international discussions about income inequality; increased news coverage of income inequality issues by 500%</td>
			</tr><tr>
			<th><a href="https://en.wikipedia.org/wiki/Egyptian_Revolution_of_2011">Arab Spring(2011)</a></th>
				<td class="fragment" style="border: none; border-left:1px solid white;">In response to human rights abuses, media censorship, and other issues, student organized and initiated protests overthrew governments in Tunisia, Egypt, Libya, and Yemen.</td>
			</tr><tr>
			<th><a href="http://www.teaparty.org/">Tea Party Movement (2009)</a></th>
				<td class="fragment" style="border: none; border-left:1px solid white;">Concerned about taxes and government spending, the Tea Party successfully reshaped the politics of the Republican Party and is considered responsible for the Republican takeover of the House of Representatives in 2012.</td>
			</tr><tr>
		</table>
	</section>
	<section>
		<h3>Notable Grassroots Movements</h3>
		<table class="reveal">
			<tr>
				<th style="border-bottom:1px solid white; ">Movement</th>
				<th style="border-bottom:1px solid white; ">Effects</th>
			</tr><tr>
				<th><a href="">Berkeley of the South (1960-1975)</a></th>
				<td class="fragment" style="border: none; border-left:1px solid white;">During the 60's and 70's, FSU was a hotspot of student activism, including regular protests about women's rights, racial integration, and the Vietnam war. Students successfully shaped policy set by the University, achieving integration in 1962, and founding <a href="http://sga.fsu.edu/cpe.shtml">The Center for Participant Education</a>. <a class="fragment" href="https://web.archive.org/web/20120527161946/http://www.fsu.edu/~fstime/FS-Times/Volume2/apr97web/4apr97.html">As part of this process, FSU students also originated the practice of streaking, when in 1974 200 students ran nude across Landis Green</a>.</td>
			</tr><tr>
			<th><a href="https://en.wikipedia.org/wiki/Barbara_Rose_Johns">Davis v. Prince Edward County (1951)</a></th>
				<td class="fragment" style="border: none; border-left:1px solid white;">16-year old Barbara Johns rallied students to protest the segregation of schools in Farmville, VA, and lead a series of strikes. These in turn lead to a lawsuit, Davis v. Prince Edward County, which was ultimately combined with other suits into the Brown v. Board of Education Supreme Court case that desegregated schools.</td>
			</tr>
		</table>
	</section>
	<section>
		<h3>Notable Grassroots Movements</h3>
		<table class="reveal">
			<tr>
				<th style="border-bottom:1px solid white; ">Movement</th>
				<th style="border-bottom:1px solid white; ">Effects</th>
			</tr><tr>
			<th><a href="https://en.wikipedia.org/wiki/Ram%C3%B3n_Grau">Cuban Revolution (1928-1933)</a></th>
				<td class="fragment" style="border: none; border-left:1px solid white;">In response to government mismanagement during the Great Depression, and other social complaints, students in Cuba launched a brief revolution that overthrew the sitting government. They installed a new government headed by a popular college professor, Dr. Ramón Grau San Martín.</td>
			</tr><tr>
		</table>
		<p style="padding-top:1em;" class="fragment">What movements today might join this list?</p>
	</section>
	<section>
		<h3>Extra Credit Opportunity (400pts)</h3>
		<ol>
			<li>After class, break into small groups based on shared interests. <br />Group size is up to you.</li>
			<li>Start a populist student movement and nonviolently overthrow the United States Government.</li>
			<li>Create a new democratic regime with international recognition, and install Mr. Benesh as President.</li>
		</ol>
		<p style="padding-top:1em;"><small>Disclaimer: If you are <a href="http://www.leoncountyso.com/leon-county-jail/jail-inmate-search">arrested</a>, then this is a joke and I will not pay your bail. If you succeed, then this is serious and you will be appointed to the <a href="http://starwars.wikia.com/wiki/Galactic_Senate">Senate</a>.</small></p>
	</section>
	<section>
		<h2>How does this affect me?</h2>
		<ul>
			<li>If you're a human service professional, policy will shape:
				<ul class="fragment">
					<li>Your scope of practice, and who you can serve</li>
					<li>Who can receive your services</li>
					<li>How you communicate about your work</li>
					<li>The serivces and opportunities your clients can receive elsewhere</li>
				</ul></li>
			<li>If you're a policy advocate, policy can:
				<ul class="fragment">
					<li>Help you create large scale change</li>
					<li>Enable you to be proactive rather than reactive when addressing social problems</li>
				</ul></li>
			<li class="fragment">Regardless of your role, policy allows you to have greater agency over your personal and professional life.</li>
		</ul>
	</section>
	<section>
		<h2>Discussion</h2>
		<ol>
			<li class="fragment">What impact do you think nonviolent political protests have had on shaping citizens views of political activism in the US and abroad?</li>
			<li  class="fragment">What assumptions, biases, or perceptions of the political process are common among college students today? What events or circumstances might have influenced these perceptions?</li>
			<li  class="fragment">What are some public policies that impact you our your family? Are the effects of these policies positive, negative, or mixed?</li>
		</ol>
	</section>
</section>

<section>
	<section>
		<h1>Focusing on Family Policy Making</h1>
		<h4>&#8220;Families are everyone's concern, but nobody's responsibility&#8221; (Ooms, 1990, p. 77)</h4>
	</section>
	<section>
		<h2>Why Families?</h2>
		<p class="fragment fade-in">Families are a fundamental part of every human society; how would society be different without them?</p>
		<iframe class="fragment" width="640" height="360" src="https://www.youtube.com/embed/2VT2apoX90o" frameborder="0" allowfullscreen></iframe>
	</section>
	<section>
		<h2>Why Policy?</h2>
		<p class="fragment fade-in">Policy dramatically changes what resources are available to families, and how families grow over time.</p>
		<div class="fragment">
			<iframe width="320" height="180" src="https://www.youtube.com/embed/9bDdaO6AINo" frameborder="0" allowfullscreen></iframe>
			<iframe width="320" height="180" src="https://www.youtube.com/embed/zhQah4PT_AI" frameborder="0" allowfullscreen></iframe>
		</div>
	</section>	
	<section>
		<h2>Are Families a Consideration in Policy Making?</h2>
		<p class="fragment">Unfortunately, families are often overlooked in policy decisions; instead, the focus is often on individuals.</p>
		<p class="fragment">Often family is acknowledged, but implementation is lacking or inadequate.</p>
	</section>	
	<section>
		<h2>Why are Families Marginalized in Policy Making?</h2>
		<h4>&#8220;...most policymakers would not think of passing a law or enacting a rule without considering its economic impact, yet family considerations are seldom taken into account...&#8221;</h4>
	</section>
	<section>
		<h3>Are politicians uninterested or inattentive to family and family issues?</h3>
		<div class="fragment">
			<iframe width="320" height="180" src="https://www.youtube.com/embed/NdKsA4q-FFA" frameborder="0" allowfullscreen></iframe>
			<iframe width="320" height="180" src="https://www.youtube.com/embed/3_PfQOA7ZqM" frameborder="0" allowfullscreen></iframe>
		</div>
		
		<p>How often to politicians talk about issues like &#8220;strengthening families&#8221;, marriage, or family structure?</p>  
	</section>
	<section>
		<h3>Is there a lack of interest, attention, or support for family policies from professionals?</h3>
		<p class="fragment">From 1988 to 2012, professional groups and organizations produced over 50 reports pushing for family-centered policies</p>
		<p class="fragment">Development of the family impact analysis framework</p>
		<p class="fragment">2009 National Human Services Assembly</p>
		<p class="fragment">NCFR makes Family Policy a requirement for CLFE credentialing</p>
	</section>
	<section>
		<h3>Is the public uninterested in families as a policy issue?</h3>
		<p class="fragment">94% of Americans report family is &#8220;very important&#8221;</p>
		<p class="fragment">93% of Americans report marriage as their &#8220;most important family goal&#8221;</p>
		<p class="fragment">Well-being and marital quality are consistently found to be related</p>
	</section>
	<section>
		<h3>Has rapid change in contemporary families marginalized family policy making?</h3>
		<ul>
			<li  class="fragment">Delays in fertility and marriage</li>
			<li  class="fragment">Increases in Cohabitation, Divorce, and Nonmarital Childbearing</li>
			<li  class="fragment">Increases in Maternal Employment</li>
			<li  class="fragment">Reduced SES Stability; Greater Divergence between SES Groups</li>
		</ul>
		<p  class="fragment">Keeping policy up to date with changing family dynamics is an ongoing challenge, which is amplified by the individualistic lens</p>
	</section>
	<section>
		<h3>Is marginalization due to skepticism or uncertainty about the appropriate role of government in family life?</h3>
		<p class="fragment">Government support for families is growing, and more policies are in place - Head Start, adoption, child health, domestic violence, family leave, immigration, etc.</p>
		<p class="fragment">Unfortunately, these programs aren't part of a unified strategy, and are often underfunded</p>
		<p class="fragment">Changing family demographics have made development of unified strategies challenging</p>
		<p class="fragment">We're still working on understanding the changes in family life, which makes addressing social problems difficult</p>
	</section>
	<section>
		<h3>Is the lack of professionals trained in evidence-based family policy responsible for the marginalization of families in policy making?</h3>
		<p class="fragment">Research suggests that evidence-based policy can be a powerful tool</p>
		<p class="fragment">Few professionals are trained in building evidence-based policies, or disseminating research findings to policy makers</p>
		<p class="fragment">Few tools exist to help professionals and policy makers consistently examine the impact of policies of families</p>
	</section>
	<section>
		<h2>Discussion</h2>
		<ol>
			<li class="fragment">What are some recent examples of campaign messages that have focused on families? How do these messages affect your selection of a candidate?</li>
			<li class="fragment">Which of the explanations for marginalization of families in policy making makes the most sense to you? Why? What can be done about it?</li>
		</ol>
	</section>
</section>
<section>
	<h1>Next Up:</h1>
	<h2><a href="./?lesson=defining">Defining Family Policy</a></h2>
</section>
