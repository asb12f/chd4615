<?php
# toggle whether page is live
$live = True;
require_once('redirect.php');

$title = 'Individual vs. Family Policy';
$id = 'night';
$theme = 'css/theme/'.$id.'.css';


## Other Available Themes
## $theme = 'http://pablocubi.co/mozreveal/css/theme/one-mozilla.css';
## Predefined Themes
## beige, blood, default, moon, night, serif, simple, sky, sky-jeopardy, solarized
## $id = 'beige';
## $theme = 'css/theme/'.$id.'.css';

require_once('header.php');
?>
<!-- each slide is a section; everything else is automated in the support PHP -->
<section>
	<h1>Agenda</h1>
	<ol>
		<li>Individualism vs Familism</li>
		<li>Theoretical Frameworks for Family Policy</li>
		<li>Marginalization of Families in Policy</li>
	</ol>
</section>
<section>
	<section>
		<h2>Individualism vs Familism</h2>
	</section>
	<section>
		<h3>Definitions</h3>
		<ul>
			<li>Individualism - <span class="fragment">emphasizes individual rights and behaviors; needs of individual are favored</li>
			<li>Familism - <span class="fragment">emphasizes responsibilities to the family and society; needs of family are favored</li>
		</ul>
	</section>
	<section>
		<h3>A Brief History</h3>
		<p class="fragment">US government has been based in individualism as a response to the heredeitary monarchy system</p>
		<p class="fragment">No mention of family in the constitution</p>
		<p class="fragment">First use of family in a Congressional subcommittee in 1981</p>
		<p class="fragment">Still no federal agency on families</p>
		<p class="fragment">Most legislation focuses on individual rights and needs rather than family needs or functions</p>
	</section>
</section>
<section>
	<section>
		<h2>Theoretical Frameworks for Family Policy</h2>
	</section>
	<section>
		<h3>Ecological Family Systems Theory</h3>
		<p class="fragment">A synthesis of Bronfenbrenner and Minuchin</p>
		<p class="fragment">Bronfenbrenner addresses the ecology that families are embedded in; external factors</p>
		<p class="fragment">Minuchin addresses the organizational processes in families, like boundaries, power, and transactions; internal factors</p>
	</section>
	<section>
		<h3>Ecological Family Systems Theory</h3>
		<p class="fragment">Ecological family systems theory allows us to recognize the intersections between internal family functions and external systems and policies.</p>
		<h4>Examples</h4>
		<table class="reveal">
		<tr>
			<th>Family Function</th><th>External Influence</th>
		</tr>
		<tr class="fragment">
			<td>Family Formation</td><td>Family Courts, Adoption Services, etc.</td>
		</tr>
		<tr class="fragment">
			<td>Economic Support</td><td>Banks, Income and Housing Programs, Social Security</td>
		</tr>
		<tr class="fragment">
			<td>Health and Mental Health</td><td>Healthcare system policies</td>
		</tr>
		<tr class="fragment">
			<td>Socialization</td><td>Media, Peers, Religious and Educational Systems, </td>
		</tr>
		<tr class="fragment">
			<td>Social Responsibility</td><td>Courts, Juvenile homes, laws, prisons</td>
		</tr>
		<tr class="fragment">
			<td>Recreation</td><td>Parks, libraries, community sports, entertainment industry</td>
		</tr>
		</table>
	</section>	
</section>
<section>
	<section>
		<h2>Marginalization of Families in Policy Making</h2>
	</section>
	<section>
		<h3>Supreme Court Trends</h3>
		<ul>
			<li class="fragment">Equal Protection Clause - Interracial Marriage &amp; Favor in Child Custody</li>
			<li class="fragment">Constitutional Autonomy - Right of individual or institution to make decisions in their sphere of influence</li>
			<li class="fragment">Privatization of Family Law; Individual contracts vs family rights</li>
			<li class="fragment">Obergefell v. Hodges - an individual right, not a right of couples</li>
			<li class="fragment">Arguments based in individual rights tend to trump those based on family, community, or collective responsibility</li>
		</ul>
	</section>
	<section>
		<h3>Federal Laws</h3>
		<ul>
			<div class="fragment">
			<li>Medicare and No Child Left Behind - create services and rights for individuals, not families</li>
			<li>Family therapy and family based interventions not covered!</li>
			<li>Respite care not reimbursed to caregivers</li>
			</div>
			<li class="fragment">Disability and social security aren't available for stay at home parents, but are for nannies who fill the same role</li>
			<li class="fragment">Marriage Penalty - for low income families, marriage can move the family above threshholds for income-based programs like SNAP, Medicaid, or Housing Programs</li>
			<li class="fragment">Tax Deductible Work Expenses - meals, travel, and entertainment are deductible, but childcare is not</li>
		<ul>
	</section>
	<section>
		<h3>State Laws</h3>
			<ul>
				<li class="fragment">Devolution Legislation of the 90's - transferred children's health insurance, family preservation, and k-12 education from federal to state authority</li>
				<li class="fragment"><a href="http://www.npr.org/sections/health-shots/2015/04/28/402642205/how-getting-married-affects-health-insurance-tax-credits">Income tested programs are more available to single parent families than two-parent families</a></li>
				<li class="fragment">Bias towards custodial rather than non-custodial parents</li>
			</ul>
	</section>
	<section>
		<h3>Local Laws and Policies</h3>
		<ul>
			<li class="fragment">Sex offender housing ordinances</li>
			<li class="fragment">Zoning decisions impact quality of neighborhoods and resource access</li>
			<li class="fragment">Local school policies shape parental involvement; Title I Parent Involvement Plan determines availability of things like parent-teacher conferences</li>
			<li class="fragment">Availability of after school and summer camp programs, <a href="http://www.npr.org/sections/ed/2015/09/08/438584249/new-york-city-mayor-goes-all-in-on-free-preschool">Pre-K</a>, and service programs are determined by local service providers</li>
		</ul>
	</section>
	<section>
		<h3>Workplace Policies</h3>
		<ul>
			<li class="fragment">Women in the workplace are often discriminated against if they are mothers; less likely to be hired or promoted in resume studies</li>
			<li class="fragment">Employers can fire for refusing to work mandatory overtime, even if this conflicts with family responsibilities</li>
			<li class="fragment">Penalties for taking leave for family issues</li>
			<li class="fragment">If you miss a test because you need to care for a sick child, is it a University Approved absence?</li>
		</ul>
	</section>
	<section>
		<h3>Service Organization Policies and Procedures</h3>
		<ul>
			<li class="fragment">Service provider clustering and coordination</li>
			<li class="fragment">Homeless shelter housing rules</li>
			<li class="fragment">DV arrest rules; availability of DV shelters to men</li>
			<li class="fragment">Jobs training programs are more effective if they promote and utilize stable couple relationships</li>
		</ul>
	</section>
	<section>
		<h3>Professional Training</h3>
		<ul>
			<li class="fragment">Few courses on family policy exist, and little research on family policy is published</li>
			<li class="fragment">Teachers and other service professionals often have little training on working with families</li>
			<li class="fragment">Service providers often treat families as just a group of individuals</li>
			<li class="fragment">Treatment modalities often emphasize individual over family, despite evidence supporting family interventions (CBT, DBT, etc)</li>
		</ul>
	</section>
	<section>
		<h3>Research</h3>
		<ul>
			<li class="fragment">Data collection often overlooks key issues relating to families (cohabitation, etc).</li>
			<li class="fragment">Measures tend to be inherently individualistic, and rarely capture the complexity of family life and relationships</li>
			<li class="fragment">Analysis favors individual rather than family; dyadic and social network analyses are underutilized and often misunderstood</li>
			<li class="fragment">Research often overlooks key informants, like fathers or noncustodial parents</li>
			<li class="fragment">Theories used to interpret and understand family studies are often individualistic, leading to individualistic policy responses</li>
		</ul>
	</section>
</section>
<section>
	<h2>Discussion</h2>
	<ol>
		<li>Why is &#8220;Rugged individualism&#8221; so idealized in the US today?</li>
		<li>Can policymakers focus too much on familism? What are potential consequences of overfocus on familism?</li>
		<li>Why do professionals treat individuals and not family units?</li>
	</ol>
</section>
<section>
	<h1>Next Up:</h1>
	<h2><a href="./?lesson=global">Global Perspectives on Family Policy</a></h2>
</section>
