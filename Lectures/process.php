<?php

# toggle whether page is live
$live = True;
require_once('redirect.php');

$title = 'Policy Makers and the Family Policy Process';
$id = 'night';
$theme = 'css/theme/'.$id.'.css';


## Other Available Themes
## $theme = 'http://pablocubi.co/mozreveal/css/theme/one-mozilla.css';
## Predefined Themes
## beige, blood, default, moon, night, serif, simple, sky, sky-jeopardy, solarized
## $id = 'beige';
## $theme = 'css/theme/'.$id.'.css';

require_once('header.php');
?>
<!-- each slide is a section; everything else is automated in the support PHP -->
<section>
	<h1>Agenda</h1>
	<ol>
		<li>The Structure and Function of Government</li>
		<li>What Policymakers are Like</li>
		<li>What Policymaking Institution is Like</li>
		<li>The Culture of Policymaking</li>
		<li>Communicating with Policymakers</li>
	</ol>
</section>
<section>
<h4>The Middle School Version</h4>
	<iframe width="640" height="360" src="https://www.youtube.com/embed/2nKyihoV9z8" frameborder="0" allowfullscreen></iframe>
</section>
<section>
	<section>
		<h2>The Structure and Function of Government</h2>
	</section>
	<section>
		<h3>Who is in Charge in a Democracy??</h3>
		<p>&#8220;The only title superior to that of president in a democracy is the title of <span class='fragment'>citizen&#8221; &ndash; Justice Louis Brandeis</span></p>
		<p class='fragment'>Three Branches of Government:</p>
		<ul class='fragment'>
			<li>Executive</li>
			<li>Judicial</li>
			<li>Legislative</li>
		</ul>
	</section>
	<section>
		<h3>The Executive Branch</h3>
		<ul>
			<li class='fragment'>Enforcement and Enactment of Laws</li>
			<li class='fragment'>President, Vice President, Governor, Vice Governor, Cabinet Members, and Agency Heads</li>
		</ul>
	</section>
	<section>
		<h3>The Judicial Branch</h3>
		<ul>
			<li class='fragment'>Resolving Disputes over Law Interpretation</li>
			<li class='fragment'>Resolving Disputes about How Laws are Applied</li>
			<li class='fragment'>Determining if Laws are Constitutional</li>
			<li class='fragment'>U.S. and State Supreme Courts</li>
		</ul>
	</section>
	<section>
		<h3>The Legislative Branch</h3>
		<ul>
			<li class='fragment'>Making New Laws</li>
			<li class='fragment'>Two-Chamber (Bicameral) Congress &ndash; Senate &amp; House of Representatives</li>
			<li class='fragment'>Most Common Target for Policy Change</li>
		</ul>
	</section>
	<section>
		<h3>Why 3 Branches?</h3>
		<p class='fragment'>Opposition to Monarchy</p>
		<p class='fragment'>Checks and Balances</p>
	</section>
	<section>
		<h3>U.S. Constitution Article 1, Section 8</h3>
		<p class='fragment' style='color:LawnGreen ;'>The Congress shall have Power To lay and collect Taxes, Duties, Imposts and Excises, to pay the Debts and provide for the common Defense and general Welfare of the United States; but all Duties, Imposts and Excises shall be uniform throughout the United States;</p>
		<p class='fragment'>To borrow Money on the credit of the United States;<br />
		To regulate Commerce with foreign Nations, and among the several states, and with the Indian Tribes;</p>
		<p class='fragment' style='color:LawnGreen ;'>To establish an uniform Rule of Naturalization, and uniform Laws on the subject of Bankruptcies throughout the United States;</p>
		<p class='fragment'>To coin Money, regulate the Value thereof, and of foreign Coin, and fix the Standard of Weights and Measures; <br />To provide for the Punishment of counterfeiting the Securities and current Coin of the United States;</p>
		<p class='fragment' style='color:LawnGreen ;'> To establish Post Offices and post Roads;</p>
	</section>
	<section>
		<h3>U.S. Constitution Article 1, Section 8</h3>
		<p class='fragment' style='color:LawnGreen ;'>To promote the Progress of Science and useful Arts, by securing for limited Times to Authors and Inventors the exclusive Right to their respective Writings and Discoveries;</p>
		<p class='fragment'>To constitute Tribunals inferior to the supreme Court; <br />
		To define and punish Piracies and Felonies committed on the high Seas, and Offences against the Law of Nations;</p>
		<p class='fragment' style='color:LawnGreen ;'>To declare War, grant Letters of Marque and Reprisal, and make Rules concerning Captures on Land and Water;</p>
		<p class='fragment'>To raise and support Armies, but no Appropriation of Money to that Use shall be for a longer Term than two Years; <br />
		To provide and maintain a Navy; <br />
		To make Rules for the Government and Regulation of the land and naval Forces; <br />
		To provide for calling forth the Militia to execute the Laws of the Union, suppress Insurrections and repel Invasions;</p>
	</section>
	<section>
		<h3>U.S. Constitution Article 1, Section 8</h3>
		<p class='fragment'>To provide for organizing, arming, and disciplining, the Militia, and for governing such Part of them as may be employed in the Service of the United States, reserving to the States respectively, the Appointment of the Officers, and the Authority of training the Militia according to the discipline prescribed by Congress;</p>
		<p class='fragment'>To exercise exclusive Legislation in all Cases whatsoever, over such District (not exceeding ten Miles square) as may, by Cession of particular States, and the Acceptance of Congress, become the Seat of the Government of the United States, and to exercise like Authority over all Places purchased by the Consent of the Legislature of the State in which the Same shall be, for the Erection of Forts, Magazines, Arsenals, dock-Yards, and other needful Buildings;</p>
		<p class='fragment' style='color:LawnGreen ;'>&ndash;And To make all Laws which shall be necessary and proper for carrying into Execution the foregoing Powers, and all other Powers vested by this Constitution in the Government of the United States, or in any Department or Officer thereof.</p>
	</section>
	<section>
		<h3>Pop Quiz</h3>
		<ol>
			<li class='fragment'>What are the first names of the Kardashians? <span class='fragment'><a href=https://en.wikipedia.org/wiki/Keeping_Up_with_the_Kardashians'>Answer Here</a></span></li>
			<li class='fragment'>What are the names of the Spice Girls? <span class='fragment'><a href=https://en.wikipedia.org/wiki/Spice_Girls'>Answer Here</a></span></li>
			<li class='fragment'>What are the names of two US Senators for FL? <span class='fragment'>Bill Nelson (D) &amp; Marco Rubio</span></li>
			<li class='fragment'>How many representatives does Florida have in the US House? <span class='fragment'>27</span></li>
			<li class='fragment'>How can you name? <span class='fragment'><a href=https://www.govtrack.us/congress/members/FL'>Answer Here</a></span></li>
			<li class='fragment'>Who are the current Governor and Lt. Governor for FL? <span class='fragment'>Rick Scott &amp; Carlos Lopez-Cantera</span></li>
		</ol>
	</section>
	<section>
		<h3>Subdivisions</h3>
		<p>The Senate and House both contain subcommittees and members with special ranks.</p>
	</section>
	<section>
		<h3>The House of Representatives</h3>
		<ul>
			<li>Constitutional Leadership 
				<ul>
					<li>Speaker of the House <span class='fragment'>&ndash; Paul Ryan</span></li>
				</ul></li>
			<li>Party Leadership
				<ul>
					<li>Majority Caucus
						<ul>
							<li>Majority Leader <span class='fragment'>&ndash; Kevin McCarthy</span></li>
							<li>Majority Whip <span class='fragment'>&ndash; Steve Scalise</span></li>
						</ul></li>
					<li>Minority Caucus
						<ul>
							<li>Minority Leader <span class='fragment'>&ndash; Nancy Pelosi</span></li>
							<li>Minority Whip <span class='fragment'>&ndash; Steny Hoyer</span></li>
						</ul></li>
				<ul></li>
		</ul>
	</section>
	<section>
		<h3>Standing Senate Committees</h3>
		<div style='width: 50%; float: left;'>
		<ul>
			<li>Agriculture</li>
			<li>Appropriations</li>
			<li>Armed Services</li>
			<li>Budget</li>
			<li>Education and the Workforce</li>
			<li>Energy and Commerce</li>
			<li>Ethics</li>
			<li>Financial Services</li>
			<li>Foreign Affairs</li>
			<li>Homeland Security</li>
			<li>House Administration</li>
		</ul>
		</div>
		<div style='width: 50%; float: left;'>
		<ul>
			<li>Judiciary</li>
			<li>Natural Resources</li>
			<li>Oversight and Government Reform</li>
			<li>Rules</li>
			<li>Science, Space, and Technology</li>
			<li>Small Business</li>
			<li>Transportation and Infrastructure</li>
			<li>Veterans’ Affairs</li>
			<li>Ways and Means</li>
			<li>Intelligence</li>
		</ul>
		</div>
	</section>
	<section>
		<h3>The Senate</h3>
		<ul>
			<li>Constitutional Leadership 
				<ul>
					<li>The President of the Senate<span class='fragment'>&ndash; Joe Biden</span></li>
					<li>President Pro Tempore<span class='fragment'>&ndash; Patrick Leahy</span></li>
				</ul></li>
			<li>Party Leadership
				<ul>
					<li>Majority Caucus
						<ul>
							<li>Majority Leader <span class='fragment'>&ndash;  Mitch McConnell</span></li>
							<li>Majority Whip (Assistant Majority Leader)<span class='fragment'>&ndash; John Cornyn</span></li>
						</ul></li>
					<li>Minority Caucus
						<ul>
							<li>Minority Leader <span class='fragment'>&ndash;  Harry Reid</span></li>
							<li>Minority Whip (Assistant Majority Leader)<span class='fragment'>&ndash; Richard Durbin</span></li>
						</ul></li>
				<ul></li>
		</ul>
	</section>
	<section>
		<h3>Standing Senate Committees</h3>
		<div style='width: 50%; float: left;'>
		<ul>
			<li>Agriculture, Nutrition, and Forestry</li>
			<li>Appropriations</li>
			<li>Armed Services</li>
			<li>Banking, Housing, and Urban Affairs</li>
			<li>Budget</li>
			<li>Commerce, Science, and Transportation</li>
			<li>Energy and Natural Resources</li>
			<li>Environment and Public Works</li>
		</div>
		<div style='width: 50%; float: left;'>
		<ul>
			<li>Finance</li>
			<li>Foreign Relations</li>
			<li>Health, Education, Labor, and Pensions</li>
			<li>Homeland Security and Governmental Affairs</li>
			<li>Judiciary</li>
			<li>Rules and Administration</li>
			<li>Small Business and Entrepreneurship</li>
			<li>Veterans' Affairs</li>
		</ul>
		</div>
	</section>
	<section>
		<h3>A Policymaker's Quandry</h3>
		<p>Is it better for a policymaker to always follow the wishes of public opinion, or to adhere exclusively to their personal convictions?</p>
	</section>
	<section>
		<h3>Policymakers vs Policy Administrators</h3>
		<p>Policymakers &ndash; <span class='fragment'>Persons who write and pass policies or laws</span></p>
		<ul class='fragment'>
			<li>Congressional Representatives</li>
			<li>City Council</li>
			<li>Chairpersons</li>
			<li>School Board Members</li>
		</ul>
		<p>Policy Administrators &ndash; <span class='fragment'>Lower Level Personel who develop practices and procedures to implement programs and policies</span></p>
		<ul class='fragment'>
			<li>Directors</li>
			<li>Administrators</li>
			<li>Managers</li>
		</ul>
		<p class='fragment' style='text-align:center; padding-top: 0.5em; color:LawnGreen;'>Do Policymakers or Policy Administrators have a greater role in determining the effectiveness of a new policy or law?</p>
	</section>
</section>
<section>
	<section>
		<h2>What Policymakers are Like</h2>
	</section>
	<section>
		<h3>On Congress</h3>
		<p class='fragment'>&#8220;Reader, suppose you were an idiot. And suppose you were a member of Congress. But I repeat myself.&#8221; &ndash; Mark Twain</p>
		<p class='fragment'>&#8220;You can lead a man to congress but you can't make him think.&#8221; &ndash; Milton Berle</p>
		<p class='fragment'>&#8220;It could probably be shown by facts and figures that there is no distinctly native criminal class except Congress.&#8221; &ndash; Twain</p>
		<p class='fragment'>&#8220;I have come to the conclusion that one useless man is called a disgrace, that two are called a law firm, and that three or more become a congress.&#8221; &ndash; Peter Stone</p>
		<p class='fragment'>&#8220;Chances are that a man cannot get into Congress now without resorting to arts and means that should render him unfit to go there.&#8221; &ndash; Twain</p>
		<p class='fragment'>&#8220;The taxpayers are sending congressmen on expensive trips abroad. It might be worth it except they keep coming back.&#8221; &ndash;Will Rogers</p>
		<p class='fragment'>&#8220;Politicians and diapers must be changed often, and for the same reason.&#8221; &ndash; Twain</p>
	</section>
	<section>
		<h3>Reality</h3>
		<p>While often maligned and criticized, most politicians are intelligent, capable, honest, and dedicated to pursuing the common good.</p>
		<p class='fragment'>Machiavellian schemes may be effective for short-term goals, but these rarely lead to lasting policy changes (or lasting political careers!)</p>
		<p class='fragment'>Most policymakers are open to listening to new information, even if it opposes their personal ideology</p>
		<p class='fragment'>Policymakers are pragmatic; they must rely on building relationships and alliances to accomplish policy goals</p>
		<p class='fragment'>Policymakers must be decisive, which means accepting imperfect solutions, dealing with collateral damage, and facing intense public scrutiny</p>
		<p class='fragment'>Policymakers are <a href='https://www.youtube.com/watch?v=20Kp8Kma9eM'>human</a>, and sometimes make mistakes and poor decisions</p>
	</section>
	<section>
		<h3>Challenging Our Bias</h3>
		<p>Two Ways to Challenge our Demonization of Legislators:</p>
		<ul>
			<li class='fragment'>Develop an Understanding of the Complex Challenges of Making Policy</li>
			<li class='fragment'>Recognize the Skills Necessary to Succeed in Policymaking</li>
		</ul>
	</section>
</section>
<section>
	<section>
		<h2>What Policymaking Institution is Like</h2>
	</section>
	<section>
		<h3>Where Do Policy Ideas Come From?</h3>
		<p class='fragment'>Very few policy ideas come from legislators themselves.</p>
		<p class='fragment'>Most come from Lobbyists, Analysts, Constitutents, and Speical Interest Groups</p>
		<p class='fragment'>Legislators must sort through these competing ideas, and choose those that are most likely to both succeed and be beneficial</p>
	</section>
	<section>
		<h3>The Process of Lawmaking</h3>
		<img src='https://www.congress.gov/content/legprocess/legislative-process-poster.jpg' alt='Congress.gov Overview of the Legislative Process'/>
	</section>
	<section>
		<h3>Congress.Gov Guide to the Legislative Process (~30mins)</h3>
		<iframe width="640" height="360" src="https://www.youtube.com/embed/E1CIWwu6KdQ?list=PLpAGnumt6iV6j07liSyksxaQe_KmPbeow" frameborder="0" allowfullscreen></iframe>
	</section>
	<section>
		<h3>Let's Pass a Bill!</h3>
		<p>First, we need to generate a policy idea, and a little momentum. What's a good problem to work on that affects families, warrants national attention, and isn't currently widely addressed?</p>
		<div class='fragment'>
		<img width=45% src='http://cdn.orkin.com/images/fleas/flea-exterior-interior_1162x1248.jpg' style='float:left;' />
		<img width=45% src='https://www.phoenixcsd.org/portals/0/Head%20lice%20pic.jpg' style='float:left;' />
		</div>
	</section>
	<section>
		<h3>National Organization for Flea and Louse Eradication in America (NO-FLEA)</h3>
		<ul>
			<li>A a socially and economically diverse, multidisciplinary group of public health analysts, health professionals, educators, economists, families, and community developers who want to eradicate fleas and head lice in the US</li>
			<li>Fleas present a major economic and health threat - 12 million medical cases of lice annnually; 22 &ndash; 54% of households at risk for fleas</li>
			<li>Fleas and lice have been associated with the spread of disease, increased risk of mental health problems and severity of symptoms, reduced educational achievement in children, and decreased worker productivity</li>
			<li><a href='http://www.sciencedirect.com/science/article/pii/S1201971210000299'>Bitam, Dittmar, Parola, Whiting, &amp; Raoult (2010)</a>; <a href='http://europepmc.org/abstract/med/15515633'>West (2004)</a></li>
			<li>Goal: Multidisciplinary program to exterminate fleas and lice in the US within 10 years</li>
		</ul>
	</section>
	<section>
		<h3>From Idea to Bill</h3>
		<p>NO-FLEA writes a draft of a bill featuring: universal screenings at schools, health providers, and worksites; free treatment services; tax incentives for being 'bug free'; national pesticide applications; universal veterinary coverage for treatment; appropriated grants to fund parasite-resistant fabrics; paid leave to treat infestations; and national education outreach programs.</p> 
		<p class='fragment'>We need someone to sponsor the bill. Who is our best bet?</p>
		<ul>
			<li class='fragment'>Senate or House?</li>
			<li class='fragment'>Challenger or Incumbent?</li>
		</ul>
		<p class='fragment'>Success! Senator Jane Newrep has agreed to sponsor our bill, and Senator John Oldhat has agreed to endorse it! The bill is submitted to the Senate clerks, and introduced for consideration.</p>
		<p class='fragment'>Our bill is now SB 1726, the Flea and Lice Extermination Act (FLEA).</p>
	</section>
	<section>
		<h3>To Committee!</h3>
		<p>Before our bill can be heard by the full Senate, it must be approved by one or more committees. What committees will we be most likely to need to be approved by?</p>
		<ul class='fragment'>
			<li><a href='http://www.agriculture.senate.gov/'>Agriculture, Nutrition and Forestry</a></li>
			<li><a href='http://www.help.senate.gov/'>Health, Education, Labor and Pensions</a></li>
		</ul>
		<p class='fragment'>At the committee hearings, testimony is provided by experts, agencies, companies, and concerned citizens. Committee hearings also generate some media attention. Ultimately, the committe reports out favorably on the bill with only moderate revisions.</p>
	</section>
	<section>
		<h3>To the Floor!</h3>
		<p>Before our bill can actually go to the floor, it must get put on the Schedule. To do this, the Majority leader motions that the Senate proceeds to a bill, which if met with a majority vote, results in the bill being considered.</p>
		<p class='fragment'>Once the bill is considered, Senators may propose ammendments ot the bill. There are no limits on Senate debate; ammendments do not need to be related to the content of the bill here.</p>
		<p class='fragment'>After some time, SB 1726 is passed, but it now looks fairly different; our grants for parasite-resistant fabrics are gone, and it now includes funding for a rural doctor recuitment program and provisions for TSA screenings. Now we need to go to the House.</p>
	</section>
	<section>
		<h3>Back to Step One</h3>
		<p>Now we need to get our bill into the House. To do this, we need a new sponsor. Thankfully, the media attention we gathered in committee has made this easier, and our bill is deposited in the House hopper almost immediately. It is now HR 2119.</p>
	</section>
	<section>
		<h3>Committees, Round 2</h3>
		<p>In the House, we'll probably only need to go to one committee, since the Senate committees have already met. We'll probably be seen in either:</p>
		<ul>
			<li><a href='http://edworkforce.house.gov/'>Education and the Workforce</a></li>
			<li><a href='http://agriculture.house.gov/'>Agriculture</a></li>
		</ul>
		<p>Thankfully we get through committee without ammendments.</p>
	</section>
	<section>
		<h3>To the Floor (Again)!</h3>
		<p>After scheduling, most bills are passed under &#8220;Suspension of the Rules&#8221; procedures:</p>
		<ul class='fragment'>
			<li>Max. 40 Minutes Debate</li>
			<li>No Ammendments Allowed</li>
			<li>Must pass with 2/3 majority</li>
		</ul>
		<p class='fragment'>Otherwise, bills are considered under &#8220;Special Rules&#8221;, which are custom drafted for each bill by the House Rules Committee.</p>
		<p class='fragment'>Thanfully, our bill is considered under Suspension of the Rules and is passed successfully.</p>
	</section>
	<section>
		<h3>Resolution</h3>
		<p class='fragment'>Once a bill is passed by one of the two congressional bodies it is considered &#8220;engrossed&#8221;. In most cases, the second chamber will then agree to the same version of the bill, and the bill is ready for executive action.</p>
		<p class='fragment'>However, if the second chamber wishes to make ammendments, such as in our case, ammednment exchange or &#8220;ping-pong&#8221; occurs. In this process, the bill is sent back to the first chamber for approval of the new ammendments; the first chamber can accept or send a set of counter-ammendments. This continues until both chambers agree on a single version of the bill.</p>
		<p class='fragment'>Alternatively, the bill may go to &#8220;Conference Committee&#8221;. This is a committee that negotiates a proposal that is agreeable to both chambers, and is composed of conferees from both chambers. If approved by a 51% majority of conferees from each chamber, the proposal is then reported and considered by each chamber.</p>
		<p class='fragment'>Our bill is accepted by both chambers, and is now considered &#8220;enrolled&#8221;, which means it can be presented to the president.</p>
	</section>
	<section>
		<h3>Executive Action</h3>
		<p class='fragment'>After a bill is enrolled, the president has 10 days to either sign or veto the bill.</p>
		<p class='fragment'>If the bill is signed in 10 days, it becomes law.</p>
		<p class='fragment'>If the bill is not signed in 10 days, it becomes law.</p>
		<p class='fragment'>If the bill is vetoed, it returns to the chamber of origination.</p>
		<p class='fragment'>If 2/3 of the voting members in both chambers vote to override, the veto is overridden.</p>
		<p class='fragment'>The president is amicable towards our bill, and signs it when it after just one day. It is assigned a public bill number, and is published in the next edition of the US Codes!</p>
	</section>
	<section>
		<h3>Implementation Matters</h3>
		<p class='fragment'>Now that our bill is law, it must be implemented by various agencies under the executive branch.</p>
		<p class='fragment'>Additionally, our bill and it's programs will need to be funded through appropriations in order to remain in force.</p>
	</section>
</section>
<section>
	<section>
		<h2>The Culture of Policymaking</h2>
	</section>
	<section>
		<h3>Policymaking is Rational</h3>
		<p class='fragment'>The classical problems solving process - defining a problem, clarifying goals, evaluating costs and benefits of each option, and choosing the best option - isn't really typical of modern politics</p>
		<p class='fragment'>Instead, policy is shaped by trying to meet competing needs through compromise;</p>
		<p class='fragment'>By having each &#8220;side&#8221; advocate in their own interests, we move towards a solution that maximizes benefits and minimizes harms</p>
	</section>
	<section>
		<h3>Policymaking is Political</h3>
		<p class='fragment'>Politics is a good thing; by creating tension and opposition we create rational decision making</p>
		<p class='fragment'>Compromise means that often a politician's staunches critics are those people who agree with them most!</p>
		<p class='fragment'>Political tension causes change to be slow, which allows for fuller exploration and consideration of ideas</p>
		<p class='fragment'>Cynicism about political tension can challenge </p>
	</section>
	<section>
	<iframe width="640" height="360" src="https://www.youtube.com/embed/VCI7QW11XV0" frameborder="0" allowfullscreen></iframe>
	</section>
</section>
<section>
	<section>
		<h2>Communicating with Policymakers</h2>
	</section>
	<section>
		<h3>Policy Briefs</h3>
		<h4>Written</h4>
		<iframe width="320" height="180" src="https://www.youtube.com/embed/557WZg5sDIs" frameborder="0" allowfullscreen></iframe>
		<iframe width="320" height="180" src="https://www.youtube.com/embed/nfpvCZuOVqg?list=PL1-r9I7wULg8OGChcMEnU2dShhx8IpOJs" frameborder="0" allowfullscreen></iframe>
		<h4>Oral</h4>
		<iframe width="320" height="180" src="https://www.youtube.com/embed/KDZn8NSFFBk" frameborder="0" allowfullscreen></iframe>
	</section>
	<section>
		<h3>Video College Briefs</h3>
		<iframe width="320" height="180" src="https://www.youtube.com/embed/D2mcBJDjb5o" frameborder="0" allowfullscreen></iframe>
		<iframe width="320" height="180" src="https://www.youtube.com/embed/Q2CX0oChuF4" frameborder="0" allowfullscreen></iframe>
	</section>
	<section>
		<h3>Additional Policy Brief Resources</h3>
		<iframe width="320" height="180" src="https://www.youtube.com/embed/_Y1Kuq5NK_8" frameborder="0" allowfullscreen></iframe>
		<iframe width="320" height="180" src="https://www.youtube.com/embed/R1GpcAoBvnc" frameborder="0" allowfullscreen></iframe>
		<p><a href='http://www.jhsph.edu/research/centers-and-institutes/womens-and-childrens-health-policy-center/de/policy_brief/index.html'>Source and Exercises</a></p>
	</section>
	<section>
		<h3>Letters</h3>
		<iframe width="640" height="360" src="https://www.youtube.com/embed/rWhLSORCwW0" frameborder="0" allowfullscreen></iframe>
		<p><a href='https://www.flsenate.gov/About/EffectiveCommunication'>Florida Tips</a></p>
		<p><a href='http://usgovinfo.about.com/od/uscongress/a/letterscongress.htm'>General Tips 1</a></p>
		<p><a href='https://www.avma.org/Advocacy/Tools/Pages/letter-writing-tips.aspx'>General Tips 2</a></p>
	</section>
	<section>
		<h3>Review</h3>
		<ol>
			<li>What are some common stereotypes about legislators and the legislative process? What role do these have on how the public interacts with policy? How can we challenge these stereotypes?</li>
			<li>What are the skills necessary to be an effective policymaker? How do they differ from those needed to be an effective researcher or clinician? How can we bridge this divide?</li>
		</ol>
	</section>
</section>
<section>
	<h1>Next Up:</h1>
	<h2><a href="./?lesson=evidence">Evidence-Based Family Policy</a></h2>
</section>

