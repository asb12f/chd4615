<?php

# toggle whether page is live
$live = True;
require_once('redirect.php');

$title = 'Advocacy or Education';
$id = 'night';
$theme = 'css/theme/'.$id.'.css';


## Other Available Themes
## $theme = 'http://pablocubi.co/mozreveal/css/theme/one-mozilla.css';
## Predefined Themes
## beige, blood, default, moon, night, serif, simple, sky, sky-jeopardy, solarized
## $id = 'beige';
## $theme = 'css/theme/'.$id.'.css';

require_once('header.php');
?>
<!-- each slide is a section; everything else is automated in the support PHP -->
<section>
	<h1>Agenda</h1>
	<ol>
		<li>Getting Involved</li>
		<li>Family Policy Advocacy</li>
		<li>Family Policy Education</li>
		<li>Choosing an Approach</li>
	</ol>
</section>
<section>
	<section>
		<h2>Family Policy Advocacy</h2>
	</section>
	<section>
		<h3>What is Advocacy?</h3>
		<iframe width="640" height="360" src="https://www.youtube.com/embed/SvvurHIl8LA" frameborder="0" allowfullscreen></iframe>
	</section>
	<section>
		<h3>Definitions</h3>
		<h4>What is Advocacy?</h4>
		<p class='fragment'>To campaign for an underrepresented group, policy, or ideology.</p>
		<h4>What is Family Policy Advocacy?</h4>
		<p class='fragment'>To campaign for an underrepresented group, policy, or ideology that may enhance family wellbeing.</p>
	</section>
	<section>
		<h3>Distinctions</h3>
		<p class='fragment'>Small 'a' advocacy &ndash; focused on specific groups or problems, but with nonspecific policies</p>
		<p class='fragment'>Big 'A' Advocacy &ndash; focused on promoting a specific policy</p>
	</section>
	<section>
		<h3>Relationship to Evidence</h3>
		<p class='fragment'>Explicit statement of values and interpretation of scientific evidence.</p>
		<p class='fragment'>Works with incomplete evidence and beliefs about social problems and their solutions</p>
		<p class='fragment'>At the same time, evidence is a powerful tool for advocacy</p>
	</section>
	<section>
		<h3>The Power of Advocacy</h3>
		<iframe width="640" height="360" src="https://www.youtube.com/embed/_dzaM0fCqsg" frameborder="0" allowfullscreen></iframe>
	</section>
	<section>
		<h3>Tactics of Advocacy</h3>
			<p>Lobbying</p>
			<p>Protest</p>
			<p>Awareness and Rallying</p>
			<p>Collaboration</p>
			<p>Emotional Connection</p>
			<br />
			<p>Which of these tactics did you see discussed in the video? Are there tactics in the video that aren't mentioned here?</p>
			<p>How do you see these tactics used by modern movements?</p>
	</section>
	<section>
		<h3>Advocacy in Action</h3>
		<iframe width="640" height="360" src="https://www.youtube.com/embed/F_1rLCf6Gnw" frameborder="0" allowfullscreen></iframe>
	</section>
	<section>
		<h3>Consequences of Advocacy</h3>
		<p>Reminder: These can be pros or cons, depending on the context</p>
		<ul>
			<li class='fragment'>Widely used and well understood</li>
			<li class='fragment'>A &#8220;Feel-Good&#8221; Activity</li>
			<li class='fragment'>Easy to track effectiveness</li>
			<li class='fragment'>Focus is one-sided; not an even-handed consideration</li>
			<li class='fragment'>Not an objective process</li>
			<li class='fragment'>May alienate or reduce credibility with opposing parties</li>
			<li class='fragment'>May change the nature of organizations which choose to take up advocacy, causing internal conflicts</li>
			<li class='fragment'>Requires realistic assessment of political will and influence</li>
		</ul>
	</section>
	<section>
		<h3>Discussion</h3>
		<ul>
			<li class='fragment'>Why does the distinction exist between &#8220;Big A&#8221; and &#8220;little a&#8221; adovcacy? Is this distinction useful? Are there particular professions or careers that might be suited to one or the other?</li>
			<li class='fragment'>Some employers, such as State and Federal governments, prohibit their employees from becoming involved in advocacy for State or Federal policies. Why might they do this, and is this OK?</li>
		</ul>
	</section>
</section>
<section>
	<section>
		<h2>Family Policy Education</h2>
	</section>
<section>
		<h3>Definition</h3>
		<h4>What is Family Policy Education?</h4>
		<p class='fragment'>Rather than promoting a single policy, this approach attempts to clarify potential consequences of policy alternatives</p>
	</section>
	<section>
		<h3>Distinctions</h3>
		<p class='fragment'>Education, not persuasion, is the goal</p>
		<p class='fragment'>Objective data</p>
		<p class='fragment'>Leaves decisions and value judgements to policymakers</p>
	</section>
	<section>
		<h3>Relationship to Values</h3>
		<p class='fragment'>Values are Pluralistic</p>
		<p class='fragment'>Conflict between diverse interests (democracy) leads to better policy than unilateral action</p>
		<p class='fragment'>Enlightened Self-Interest</p>
	</section>
	<section>
		<h3>Challenges of Education</h3>
		<p class='fragment'>Social Problem vs Social Conflict</p>
		<p class='fragment'>Data Interpretation</p>
		<p class='fragment'>Must recognize incompleteness and ambiguity</p>
		<p class='fragment'>Is Neutrality really possible?</p>
	</section>
	<section>
		<h3>Tactics of Education</h3>
			<p>Consultation</p>
			<p>Nonpartisan Government Agency Reports (Congressional Budget Office, etc)</p>
			<p>White Papers</p>
			<p>Family Impact Seminars</p>
	</section>
	<section>
		<h3>Education in Action</h3>
		<iframe width="640" height="360" src="https://www.youtube.com/embed/HGSl_6Rhlls" frameborder="0" allowfullscreen></iframe>
	</section>
	<section>
		<h3>Consequences of Education</h3>
		<p>Reminder: These can be pros or cons, depending on the context</p>
		<ul>
			<li class='fragment'>Less commonly used; often seen as unique</li>
			<li class='fragment'>Educators are seen as having a certain &#8220;agenda-free&#8221; mystique</li>
			<li class='fragment'>Less confrontational than advocacy</li>
			<li class='fragment'>De-polarization; opportunities for dialogue</li>
			<li class='fragment'>Evaluating effects is difficult; what outcomes need to be tracked?</li>
			<li class='fragment'>More difficult to explain to newcomers</li>
			<li class='fragment'>May be less effective for powerless groups; power often dictates research</li>
			<li class='fragment'>More difficult to execute - educators must be familiar with wide range of data, policy alternatives, and evidence</li>
		</ul>
	</section>
	<section>
		<h3>Discussion</h3>
		<li class='fragment'>In what ways are the education and advocacy approaches similar? Could they ever be combined?</li>
		<li class='fragment'>Thinking about the social and policy movements we've discussed in class, which were driven by education? Which were driven by advocacy? Which were driven by both?</li>
	</section>
</section>
<section>
	<section>
		<h2>Choosing an Approach</h2>
	</section>
	<section>
		<h3>Which stance to take?</h3>
		<p>Is it better to be an advocate or an educator?</p>
		<p>Is it possible to be both?</p>
		<p class='fragment'>Nye and McDonald argue advocacy is never appropriate for researchers or professionals who claim to be objective; Bogenschneider disagrees.</p>
	</section>
	<section>
		<h3>Considerations</h3>
		<ul>
			<li>Job Context</li>
			<li>Communication Style</li>
			<li>The Issue</li>
			<li>Potential Beneficiaries</li>
		</ul>
	</section>
	<section>
		<h3>Appropriate Advocacy Efforts by Professionals</h3>
		<ul>
			<li class='fragment'>Representing marginalized groups in decisionmaking</li>
			<li class='fragment'>Hired to do research or professional work for advocacy groups</li>
		</ul>
	</section>
	<section>
		<h3>Appropriate Education Efforts by Professionals</h3>
		<ul>
			<li class='fragment'>When data is needed to clarify policy alternatives</li>
			<li class='fragment'>When policies prevent advocacy</li>
			<li class='fragment'>When the educator is able to communicate even-handedly despite personal opinions</li>
		</ul>
	</section>
	<section>
		<h3>Discussion</h3>
		<ol>
			<li class='fragment'>As a student and an individual, which of these approaches is the best fit for you and the causes you support?</li>
			<li class='fragment'>Is it possible to move between education and advocacy roles? If so, how?</li>
		</ol>
	</section>
</section>
<section>
	<section>
		<h2>Slacktivism</h2>
		<h3>Lee &amp; Hsieh (2013)</h3>
	</section>
	<section>
		<h3>What is Slacktivism?</h3>
		<p class='fragment'>&#8220;low-risk, low-cost activity via social media whose purpose is to raise awareness, produce change, or grant satisfaction to the person engaged in the activity &#8221;</p>
		<h3>Examples</h3>
		<ul>
			<li class='fragment'>Liking and Sharing on Facebook</li>
			<li class='fragment'>Retweeting and Hashtags</li>
			<li class='fragment'>Online Petitions</li>
			<li class='fragment'>Changing your Profile Picture</li>
			<li class='fragment'>Sharing Memes and Videos</li>
			<li class='fragment'>Cause Marketing</li>
		</ul>
	</section>
	<section>
	<h3>Examples</h3>
	<h6>Disclaimer: These are live twitter feeds; I have no control over their content.</h6>
<a class="twitter-timeline" 
href="https://twitter.com/hashtag/Kony2012" 
data-widget-id="712324947725697025"
width="200"
height="600">
#Kony2012 Tweets</a>
<a class="twitter-timeline" 
href="https://twitter.com/hashtag/BringBackOurGirls" 
data-widget-id="712322397563658240"
width="200"
height="600">
#BringBackOurGirls Tweets</a>
<a class="twitter-timeline" 
href="https://twitter.com/hashtag/ALSIceBucketChallenge" 
data-widget-id="712324348103761920"
width="200"
height="600"
>#ALSIceBucketChallenge Tweets</a>
<a class="twitter-timeline" 
href="https://twitter.com/hashtag/Slacktivism" 
data-widget-id="712325583993233414"
width="200"
height="600">#Slacktivism Tweets</a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
	</section>
	<section>
		<h3>Criticism</h3>
		<iframe width="640" height="360" src="https://www.youtube.com/embed/efVFiLigmbc" frameborder="0" allowfullscreen></iframe>
		<p class='fragment'>Does engaging in slacktivism really reduce the likelihood of actual contribution?</p>
	</section>
	<section>
		<h3>Evaluating Criticism</h3>
		<ul>
			<li>Moral Balancing
				<ul>
					<li class='fragment'>Peforming a good deed reduces the likelihood of subsequent good deeds</li>
					<li class='fragment'>&#8220;cleansing&#8221; and &#8220;licensing&#8221;; Moral Accounting</li>
					<li class='fragment'>People who engage in slacktivism should be less likely to participate in additional activism</li>
					<li class='fragment'>Current research demonstrates that people who made a prosocial choice are less likely to perform a different, subsequent prosocial action</li>
				</ul>
			</li>
			<li>Cognitive Dissonance Theory
				<ul>
					<li class='fragment'>People want their actions and thinking to be consistent</li>
					<li class='fragment'>People who participate in slacktivism should be more likely to engage in additional activism</li>
					<li class='fragment'>If the subsequent action is congruent to the initial action, can the initial participation actually raise subsequent participation?</li>
				</ul>
			</li>
		</ul>
	</section>
	<section>
		<h3>Current Research Findings</h3>
		<ul>
			<li class='fragment'>Participants who signed petition were more likely to donate to a charity when the charity was related to the petition’s cause</li>
			<li class='fragment'>When people are invited to sign a petition and decline to do so, they actually subsequently donated more to an incongruent charity</li>
			<li class='fragment'>Simply being asked to sign the petition increased subsequent donation from the participants</li>
			<li class='fragment'>Signing the petition did not increase or decrease participants’ intentions to participate in subsequent high cost actions such as attending protests, it only increased intentions to sign future petitions and write letters.</li>
		</ul>
	</section>
	<section>
		<h3>Practical Application</h3>
		<ul>
			<li class='fragment'>&#8220;Campaign designers could make a large, excessive online request for issue A that is likely to be turned down, which would make people feel guilty for their inaction. Then, the campaign designers would follow-up with a request for an unrelated issue B. That would then result in higher support for issue B than if people were approached to support issue B right away. &#8221;</li>
			<li class='fragment'>Slacktivism in general may help subsequent activism, regardless of how many people actually choose to participate in the slacktivism.</li>
		</ul>
	</section>
	<section>
		<h3>Final Thoughts</h3>
		<iframe width="640" height="360" src="https://www.youtube.com/embed/klDNC_-YUGA" frameborder="0" allowfullscreen></iframe>
	</section>	
</section>

<section>
	<h1>Next Up:</h1>
	<h2>Evidence Based Family Policy</h2>
</section>

